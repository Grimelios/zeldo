﻿using System;
using System.Collections.Generic;
using Engine.Core._2D;
using Engine.Editing;
using Engine.Shaders;
using static Engine.GL;

namespace Engine.Accessibility
{
	// TODO: Apply colorblind filters to the canvas as well.
	public class ColorblindCommand : TerminalCommand
	{
		private const float Gamma = 2.2f;
		private const float GammaInverse = 1 / Gamma;
		
		private Sprite mainSprite;
		private Shader shader;

		private ColorblindTypes currentType;

		public ColorblindCommand(Sprite mainSprite) : base("colorblind", "filter")
		{
			this.mainSprite = mainSprite;
			
			currentType = ColorblindTypes.None;
			
			// See https://github.com/nvkelso/color-oracle-java/blob/master/src/ika/colororacle/Simulator.java.
			var gammaToLinear = new int[256];
			var linearToGamma = new int[256];

			for (int i = 0; i < gammaToLinear.Length; i++)
			{
				float l = 0.992052f * (float)Math.Pow(i / 255f, Gamma) + 0.003974f;

				gammaToLinear[i] = (int)(l * 32767);
				linearToGamma[i] = (int)(255 * Math.Pow(i / 255f, GammaInverse));
			}

			// TODO: If 2D component shaders are simplified, this should be simplified in response.
			shader = new Shader();
			shader.Attach(ShaderTypes.Vertex, "Sprite.vert");
			shader.Attach(ShaderTypes.Fragment, "Accessibility/Colorblind.frag");
			shader.AddAttribute<float>(2, GL_FLOAT);
			shader.AddAttribute<float>(2, GL_FLOAT);
			shader.AddAttribute<byte>(4, GL_UNSIGNED_BYTE, ShaderAttributeFlags.IsNormalized);
			shader.Initialize();
			shader.Use();
			shader.SetUniform("gammaToLinear", gammaToLinear);
			shader.SetUniform("linearToGamma", linearToGamma);
		}

		public override TerminalArgument[] Usage => new []
		{
			new TerminalArgument("type", ArgumentTypes.Required)
		};

		public override string[] GetOptions(string[] args)
		{
			// Conveniently, all colorblind types already sort alphabetically after "Clear".
			var list = new List<string>();
			list.Add("Clear");
			list.AddRange(Enum.GetNames(typeof(ColorblindTypes)));

			return list.ToArray();
		}

		public override bool Process(string[] args, out string result)
		{
			var raw = args[0];
			var isValid = Enum.TryParse(raw, true, out ColorblindTypes type);
			var shouldClear = isValid && type == ColorblindTypes.None ||
				raw.Equals("Clear", StringComparison.CurrentCultureIgnoreCase);

			// Colorblind mode should be reset.
			if (shouldClear)
			{
				if (currentType == ColorblindTypes.None)
				{
					result = "Colorblind mode is already disabled.";
				}
				else
				{
					result = "Colorblind mode disabled.";
					mainSprite.Shader = null;
					currentType = ColorblindTypes.None;
				}

				return true;
			}

			// A non-valid type was entered.
			if (!isValid)
			{
				result = $"'{raw}' is not a valid form of colorblindness.";

				return false;
			}
			
			// A valid type was entered.
			var s = type.ToString();

			result = type == currentType
				? $"{s} is already enabled."
				: $"{s} filter applied.";

			Refresh(type);

			return true;
		}

		public void Refresh(ColorblindTypes type)
		{
			if (type != currentType)
			{
				shader.Use();
				shader.SetUniform("colorblindType", (int)type);
				
				mainSprite.Shader = shader;
				currentType = type;
			}
		}
	}
}
