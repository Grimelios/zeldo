﻿using GlmSharp;

namespace Engine.Core._2D
{
	public class RenderScale2DField : RenderField<vec2>
	{
		public RenderScale2DField(Flags<TransformFlags> flags) : base(flags, TransformFlags.IsScaleSet,
			TransformFlags.IsScaleInterpolationNeeded, vec2.Ones)
		{
		}

		protected override vec2 Interpolate(float t)
		{
			return vec2.Lerp(OldValue, Value, t);
		}
	}
}
