﻿using System.Linq;
using Engine.Graphics;
using Engine.Graphics._2D;
using Engine.Utility;
using GlmSharp;

namespace Engine.Core._2D
{
	public class Sprite : Component2D
	{
		private QuadSource source;
		private Bounds2D sourceRect;

		public Sprite(string filename, Bounds2D sourceRect = null, Alignments alignment = Alignments.Center,
			SpriteModifiers mods = SpriteModifiers.None) :
			this (ContentCache.GetTexture(filename), sourceRect, alignment, mods)
		{
		}

		public Sprite(QuadSource source, Bounds2D sourceRect = null, Alignments alignment = Alignments.Center,
			SpriteModifiers mods = SpriteModifiers.None) : base(alignment, mods)
		{
			this.source = source;
			this.sourceRect = sourceRect;

			data = new float[QuadSize];
		}

		public Bounds2D SourceRect
		{
			get => sourceRect;
			set
			{
				sourceRect = value;
				flags.Value |= TransformFlags.IsSourceChanged;
			}
		}

		public void ScaleTo(int width, int height, bool shouldInterpolate)
		{
			float w;
			float h;

			if (sourceRect != null)
			{
				w = sourceRect.Width;
				h = sourceRect.Height;
			}
			else
			{
				w = source.Width;
				h = source.Height;
			}

			Scale.SetValue(new vec2(width / w, height / h), shouldInterpolate);
		}

		protected override void RecomputeTransformData(vec2 p, vec2 scale, float rotation)
		{
			float width;
			float height;

			if (sourceRect != null)
			{
				width = sourceRect.Width;
				height = sourceRect.Height;
			}
			else
			{
				width = source.Width;
				height = source.Height;
			}

			width *= scale.x;
			height *= scale.y;

			var points = new []
			{
				new vec2(0, 0),
				new vec2(0, height),
				new vec2(width, 0),
				new vec2(width, height)
			};

			RecomputeOrigin(scale);

			for (int i = 0; i < 4; i++)
			{
				var point = points[i];
				point -= origin;
				points[i] = point;
			}

			if (rotation != 0)
			{
				var matrix = Utilities.RotationMatrix2D(rotation);

				for (int i = 0; i < 4; i++)
				{
					var point = points[i];
					point = matrix * point;
					points[i] = point;
				}
			}

			var order = Enumerable.Range(0, 4).ToArray();

			bool flipHorizontal = (Mods & SpriteModifiers.FlipHorizontal) > 0;
			bool flipVertical = (Mods & SpriteModifiers.FlipVertical) > 0;

			if (flipHorizontal)
			{
				order[0] = 2;
				order[1] = 3;
				order[2] = 0;
				order[3] = 1;
			}

			if (flipVertical)
			{
				int temp1 = order[0];
				int temp2 = order[2];

				order[0] = order[1];
				order[1] = temp1;
				order[2] = order[3];
				order[3] = temp2;
			}

			for (int i = 0; i < 4; i++)
			{
				var start = i * VertexSize;
				var point = points[order[i]] + p;

				data[start] = point.x;
				data[start + 1] = point.y;
			}
		}

		private void RecomputeOrigin(vec2 scale)
		{
			int width;
			int height;

			if (sourceRect == null)
			{
				width = source.Width;
				height = source.Height;
			}
			else
			{
				width = sourceRect.Width;
				height = sourceRect.Height;
			}

			// TODO: Should origin be cast to an integer? Might lose accuracy and smoothness.
			origin = Utilities.ComputeOrigin((int)(width * scale.x), (int)(height * scale.y), Alignment);
		}

		protected override void RecomputeSourceData()
		{
			var coords = new vec2[4];

			if (sourceRect != null)
			{
				coords[0] = new vec2(sourceRect.Left, sourceRect.Top);
				coords[1] = new vec2(sourceRect.Left, sourceRect.Bottom);
				coords[2] = new vec2(sourceRect.Right, sourceRect.Top);
				coords[3] = new vec2(sourceRect.Right, sourceRect.Bottom);

				var dimensions = new vec2(source.Width, source.Height);

				for (int i = 0; i < 4; i++)
				{
					coords[i] /= dimensions;
				}
			}
			else
			{
				coords[0] = vec2.Zero;
				coords[1] = vec2.UnitY;
				coords[2] = vec2.UnitX;
				coords[3] = vec2.Ones;
			}

			for (int i = 0; i < 4; i++)
			{
				var value = coords[i];
				var start = i * VertexSize + 2;

				data[start] = value.x;
				data[start + 1] = value.y;
			}
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			Draw(sb, t, source.Id, data);

			Statistics.Increment(RenderKeys.Triangles, 2);
		}	
	}
}
