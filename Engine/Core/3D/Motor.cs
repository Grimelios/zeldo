﻿using System.Diagnostics;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.Utility;
using GlmSharp;

namespace Engine.Core._3D
{
	public class Motor : IComponent
	{
		private IOrientationContainer parent;

		private vec3 axis;
		private quat baseOrientation;

		private float rotation;

		public Motor(IOrientationContainer parent, bool isRunning = true)
		{
			this.parent = parent;

			// This assumes the parent object is oriented correctly when the motor is created.
			baseOrientation = parent.Orientation;
			IsRunning = isRunning;
			axis = vec3.UnitY;
		}

		public bool IsComplete => false;
		public bool IsRunning { get; set; }

		public vec3 Axis
		{
			get => axis;
			set
			{
				Debug.Assert(Utilities.LengthSquared(axis) > 0.00001f, "Motor axis must have non-zero length.");

				axis = value;
			}
		}

		public float AngularVelocity { get; set; }

		public void Update()
		{
			Debug.Assert(axis != vec3.Zero, "Motor axis can't be vec3.zero (this likely means the axis was never " +
				"set.");

			if (IsRunning)
			{
				rotation = Utilities.RestrictAngle(rotation + AngularVelocity * Game.DeltaTime);
				parent.SetOrientation(baseOrientation * quat.FromAxisAngle(rotation, axis), true);
			}
		}
	}
}
