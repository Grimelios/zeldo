﻿using Engine.Core._2D;
using Engine.Interfaces;
using Engine.Interfaces._2D;
using Engine.Interfaces._3D;
using Engine.Shaders;
using Engine.Utility;
using GlmSharp;

namespace Engine.Core._3D
{
	public class Sprite3D : IRenderable3D, IRenderScalable2D, IRenderColorable
	{
		// This value scales pixels to meters (or in-game units). When unscaled, every X pixels spans one meter.
		public const int PixelDivisor = 100;

		private RenderPosition3DField positionField;
		private RenderScale2DField scaleField;
		private RenderOrientationField orientationField;
		private RenderColorField colorField;
		private ivec2 origin;

		public Sprite3D(string filename, Alignments alignment = Alignments.Center) :
			this(ContentCache.GetTexture(filename), alignment)
		{
		}

		// For the time being, source rects for 3D sprites aren't supported (in order to simplify the rendering
		// process). This might be fine, even if source rects are never added.
		public Sprite3D(QuadSource source, Alignments alignment = Alignments.Center)
		{
			Source = source;
			origin = Utilities.ComputeOrigin(source.Width, source.Height, alignment) -
				new ivec2(source.Width, source.Height) / 2;

			var flags = new Flags<TransformFlags>(TransformFlags.None);

			IsDrawEnabled = true;
			IsShadowCaster = true;
			positionField = new RenderPosition3DField(flags);
			scaleField = new RenderScale2DField(flags);
			orientationField = new RenderOrientationField(flags);
			colorField = new RenderColorField(flags);
		}

		public RenderPosition3DField Position => positionField;
		public RenderScale2DField Scale => scaleField;
		public RenderOrientationField Orientation => orientationField;
		public RenderColorField Color => colorField;
		public mat4 WorldMatrix { get; private set; }

		public QuadSource Source { get; }

		public bool IsBillboarded { get; set; }
		public bool IsDrawEnabled { get; set; }
		public bool IsShadowCaster { get; set; }

		// Custom shaders are optional. If not set, the default 3D sprite shader is used.
		public Shader Shader { get; set; }
		public Shader ShadowShader { get; set; }

		public void Recompute(float t)
		{
			colorField.Evaluate(t);

			var p = positionField.Evaluate(t);
			var s = scaleField.Evaluate(t);
			var q = orientationField.Evaluate(t);

			// By shifting the world matrix using the origin, all sprites (regardless of transform or alignment) can be
			// rendered using the same unit square in GPU memory.
			var correction = new vec3((vec2)origin / PixelDivisor, 0) * q;
			
			WorldMatrix = mat4.Translate(p + correction) * q.ToMat4 * mat4.Scale(new vec3(s, 0));
		}

		// TODO: Do textures need to be unloaded?
		public void Dispose()
		{
			Shader?.Dispose();
			ShadowShader?.Dispose();
		}
	}
}
