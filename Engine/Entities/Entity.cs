﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Engine.Animation;
using Engine.Core;
using Engine.Core._3D;
using Engine.Graphics._3D;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.Physics;
using Engine.Props;
using Engine.Sensors;
using Engine.Shapes._3D;
using GlmSharp;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;

namespace Engine.Entities
{
	// TODO: Consider adding an origin for controller bodies (e.g. doors, which are positioned by their bottom-center).
	public abstract class Entity : ITransformContainer3D, IDynamic, IReloadable
	{
		private List<(EntityAttachmentTypes Type, ITransformable3D Target, vec3 Position,
			quat Orientation)> attachments;
		private List<(EntityAttachmentTypes Type, IRenderTransformable3D Target, vec3 Position,
			quat Orientation)> renderAttachments;
		private List<(Entity Target, vec3 Position, quat Orientation)> entities;
		private List<(RigidBody Target, vec3 Position, quat Orientation)> bodies;
		private List<EntityHandle> handles;

		protected EntityFlags entityFlags;

		protected vec3 position;
		protected quat orientation;
		protected RigidBody controllingBody;

		// The entity's transform properties (Position and Orientation) also update attachments. Since the controlling
        // physics body is itself an attachment, this would cause bodies to set their own transforms again (which is
        // wasteful). Using this variable avoids that.
	    protected bool selfUpdate;

		protected Entity(int group)
		{
			Debug.Assert(group >= 0, "Entity group can't be negative.");

			Group = group;
			entityFlags = EntityFlags.IsUpdateEnabled | EntityFlags.IsDrawEnabled;
			orientation = quat.Identity;
			attachments = new List<(EntityAttachmentTypes Type, ITransformable3D Target, vec3 Position,
				quat Orientation)>();
			renderAttachments = new List<(EntityAttachmentTypes Type, IRenderTransformable3D Target, vec3 Position,
				quat Orientation)>();
			Components = new ComponentCollection();

			// -1 means that the entity had no explicit ID given when spawned from a fragment file.
			Id = -1;
		}
		
		protected ComponentCollection Components { get; }

		public int Group { get; }

		// ID isn't set on all entities (only those meant to be retrieved via handles).
		public int Id { get; private set; }

		public bool IsUpdateEnabled => (entityFlags & EntityFlags.IsUpdateEnabled) > 0;
		public bool IsInitialized => (entityFlags & EntityFlags.IsInitialized) > 0;

		public bool IsDrawEnabled
		{
			get => (entityFlags & EntityFlags.IsDrawEnabled) > 0;
			set
			{
				if (value == IsDrawEnabled)
				{
					return;
				}

				renderAttachments.ForEach(a => ((IRenderable3D)a.Target).IsDrawEnabled = value);

				if (value)
				{
					entityFlags |= EntityFlags.IsDrawEnabled;
				}
				else
				{
					entityFlags &= ~EntityFlags.IsDrawEnabled;
				}
			}
		}

		public Scene Scene { get; set; }
		public RigidBody ControllingBody => controllingBody;
		public vec3 Position => position;
		public quat Orientation => orientation;

		public virtual void Initialize(Scene scene, JToken data)
		{
			Debug.Assert((entityFlags & EntityFlags.IsInitialized) == 0, "Can't initialize an entity more than once.");

			Scene = scene;
			entityFlags |= EntityFlags.IsInitialized;

			if (data == null)
			{
				return;
			}

			// Parse ID.
			if (data.TryParse("Id", out int id))
			{
				Id = id;
			}

			// Parse handles.
			var hToken = data["Handle"];
			var hListToken = data["Handles"];

			Debug.Assert(hToken == null || hListToken == null, "Duplicate entity handle blocks. Use either Handle " +
				"(for singular handles) or Handles (for multiple handles).");

			if (hToken != null || hListToken != null)
			{
				handles = new List<EntityHandle>();

				if (hToken != null)
				{
					handles.Add(new EntityHandle(hToken));
				}
				else
				{
					foreach (var token in hListToken.Children())
					{
						handles.Add(new EntityHandle(token));
					}
				}
			}
			
			Properties.Access(this);
		}

		public virtual bool Reload(PropertyAccessor accessor, out string message)
		{
			message = null;

			return true;
		}

		// This function is useful primarily when swapping out an entity for a ragdolled version.
		public void TransferEntities(Entity to)
		{
			if (to.entities == null)
			{
				to.entities = new List<(Entity Target, vec3 Position, quat Orientation)>();
			}

			entities?.ForEach(e => to.entities.Add(e));
		}

		private void ComputeOffsets(vec3 position, quat orientation, out vec3 p, out quat q)
		{
			var inverse = this.orientation.Inverse;
			p = inverse * (position - this.position);
			q = inverse * orientation;
		}

		private void Attach(EntityAttachmentTypes type, ITransformable3D target, vec3? nullablePosition,
			quat? nullableOrientation)
		{
			var p = nullablePosition ?? vec3.Zero;
			var q = nullableOrientation ?? quat.Identity;

			attachments.Add((type, target, p, q));
			target.Position = position + orientation * p;
			target.Orientation = orientation * q;
		}

		private void Attach(EntityAttachmentTypes type, IRenderTransformable3D target, vec3 p, quat q)
		{
			renderAttachments.Add((type, target, p, q));

			target.Position.SetValue(position + orientation * p, false);
			target.Orientation.SetValue(orientation * q, false);

			((IRenderable3D)target).IsDrawEnabled = IsDrawEnabled;
		}

		// Position and orientation are given in absolute units (rather than relative).
		public void Attach(Entity entity, vec3? nullablePosition = null, quat? nullableOrientation = null)
		{
			// The entity attachment list is lazily created. In contrast, the other two attachment lists are created in
			// the constructor (under the assumption that virtually all entities will use at least one regular and
			// rendered attachment).
			ComputeOffsets(nullablePosition ?? entity.position, nullableOrientation ?? entity.orientation, out var p,
				out var q);

			entities = entities ?? new List<(Entity Target, vec3 Position, quat Orientation)>();
			entities.Add((entity, p, q));
		}

		private void ComputeFan(int count, vec3 position, quat? nullableOrientation, vec3 axis, out vec3[] positions,
			out quat[] orientations)
		{
			Debug.Assert(count > 0, "Fan count must be positive.");

			// Note that passing an orientation allows each fanned object to be rotated. It does NOT affect computed
			// positions around the axis.
			var increment = Constants.TwoPi / count;
			var q = nullableOrientation ?? quat.Identity;

			positions = new vec3[count];
			orientations = new quat[count];

			for (int i = 0; i < count; i++)
			{
				var o = quat.FromAxisAngle(increment * i, axis);
				orientations[i] = o * q * orientation;
				positions[i] = this.position + o * position;
			}
		}

		// TODO: Are these getters needed? Is an additional function needed to get attached entities too?
		protected T GetAttachment<T>() where T : class
		{
			foreach (var a in attachments)
			{
				if (a.Target is T result)
				{
					return result;
				}
			}

			return null;
		}

		protected T GetRenderAttachment<T>() where T : class
		{
			foreach (var a in renderAttachments)
			{
				if (a.Target is T result)
				{
					return result;
				}
			}

			return null;
		}

		protected Model CreateModel(Scene scene, string filename, bool shouldAttach = true,
			vec3? nullablePosition = null, quat? nullableOrientation = null)
		{
			return CreateModel(scene, ContentCache.GetMesh(filename), shouldAttach, nullablePosition,
				nullableOrientation);
		}

		// This version of the function is commonly used for simple objects that create a physics body based on mesh
		// bounds.
		protected Model CreateModel(Scene scene, string filename, out vec3 bounds)
		{
			var model = CreateModel(scene, ContentCache.GetMesh(filename));
			bounds = model.Mesh.Bounds;

			return model;
		}

		protected Model CreateModel(Scene scene, Mesh mesh, bool shouldAttach = true, vec3? nullablePosition = null,
			quat? nullableOrientation = null)
		{
			Debug.Assert(mesh != null, "Can't create a model with a null mesh.");

			var model = new Model(mesh);
			scene.Renderer.Add(model);

			if (shouldAttach)
			{
				ComputeOffsets(nullablePosition ?? position, nullableOrientation ?? orientation, out var p, out var q);
				Attach(EntityAttachmentTypes.Model, model, p, q);
			}

			return model;
		}

		protected Model[] FanModels(Scene scene, string filename, int count, vec3 position, vec3 axis,
			quat? nullableOrientation = null)
		{
			return FanModels(scene, ContentCache.GetMesh(filename), count, position, axis, nullableOrientation);
		}

		protected Model[] FanModels(Scene scene, Mesh mesh, int count, vec3 axis, vec3 position,
			quat? nullableOrientation = null)
		{
			ComputeFan(count, position, nullableOrientation, axis, out var positions, out var orientations);

			var models = new Model[count];

			for (int i = 0; i < count; i++)
			{
				models[i] = CreateModel(scene, mesh, true, positions[i], orientations[i]);
			}

			return models;
		}

		protected void RemoveModel(Model model = null)
		{
			var renderer = Scene.Renderer;

			// If a model is given, only that model is removed.
			if (model != null)
			{
				for (int i = renderAttachments.Count - 1; i >= 0; i--)
				{
					if (renderAttachments[i].Target == model)
					{
						renderer.Remove(model);
						renderAttachments.RemoveAt(i);

						return;
					}
				}

				Debug.Fail("Given model isn't attached.");

				return;
			}

			// If no model is given, the first attached model is removed.
			for (int i = renderAttachments.Count - 1; i >= 0; i--)
			{
				var attachment = renderAttachments[i];

				if (attachment.Type == EntityAttachmentTypes.Model)
				{
					renderer.Remove((Model)attachment.Target);
					renderAttachments.RemoveAt(i);

					return;
				}
			}

			Debug.Fail("No models attached.");
		}

		protected Sensor CreateSensor(Scene scene, Shape3D shape = null, int group = 0, int affects = 0,
			SensorTypes type = SensorTypes.Entity, bool isEnabled = true, bool shouldAttach = true,
			vec3? nullablePosition = null, quat? nullableOrientation = null)
		{
			var sensor = new Sensor(type, this, group, affects, shape);
			sensor.IsEnabled = isEnabled;
			scene.Space.Add(sensor);

			if (shouldAttach)
			{
				ComputeOffsets(nullablePosition ?? position, nullableOrientation ?? orientation, out var p, out var q);
				Attach(EntityAttachmentTypes.Sensor, sensor, p, q);
			}

			return sensor;
		}

		protected Sensor[] FanSensors(Scene scene, Func<Shape3D> shapeFunction, int count, vec3 axis, vec3 position,
			int group = 0, int affects = 0, SensorTypes type = SensorTypes.Entity, quat? nullableOrientation = null)
		{
			ComputeFan(count, position, nullableOrientation, axis, out var positions, out var orientations);

			var sensors = new Sensor[count];

			for (int i = 0; i < count; i++)
			{
				sensors[i] = CreateSensor(scene, shapeFunction.Invoke(), group, affects, type, true, true,
					positions[i], orientations[i]);
			}

			return sensors;
		}

		protected void RemoveSensor(Sensor sensor = null)
		{
			var space = Scene.Space;

			// If a sensor is given, only that sensor is removed.
			if (sensor != null)
			{
				Debug.Assert(attachments.Exists(a => a.Target == sensor), "Given sensor isn't attached.");

				space.Remove(sensor);

				for (int i = attachments.Count - 1; i >= 0; i--)
				{
					if (attachments[i].Target == sensor)
					{
						attachments.RemoveAt(i);

						return;
					}
				}

				return;
			}

			Debug.Assert(attachments.Exists(a => a.Type == EntityAttachmentTypes.Sensor), "No sensors attached.");

			// If no sensor is given, the first attached sensor is removed.
			for (int i = attachments.Count - 1; i >= 0; i--)
			{
				var target = attachments[i].Target;

				if (target is Sensor result)
				{
					space.Remove(result);
					attachments.RemoveAt(i);

					return;
				}
			}
		}

		// TODO: After consideration, it's impossible to attach a non-controlling, non pseudo-static body (since by setting the body's transform as an attachment, it's pseudo-static by definition). Class should be updated accordingly.
		protected RigidBody CreateBody(Scene scene, Shape shape, uint group,
			RigidBodyTypes bodyType = RigidBodyTypes.Dynamic, RigidBodyFlags flags = RigidBodyFlags.None,
			bool isControlling = true, vec3? nullablePosition = null, quat? nullableOrientation = null,
			bool shouldAttach = true)
		{
			Debug.Assert(shape != null, "Can't create a body with a null shape.");
			Debug.Assert(!isControlling || controllingBody == null, "Controlling body is already set.");
			Debug.Assert(!isControlling || (nullablePosition == null && nullableOrientation == null), "Can't apply " +
				"attachment offsets to a controlling body.");
			Debug.Assert(isControlling || bodyType == RigidBodyTypes.PseudoStatic, "Non-controlling body " +
				"attachments must be pseudo-static.");

			var body = new RigidBody(shape, group, bodyType, flags);
			body.Tag = this;

            // Note that the controlling body is intentionally not attached as a regular attachment. Doing so would
            // complicate transform updates from that body.
			scene.World.AddBody(body);

		    if (isControlling)
		    {
				if ((entityFlags & EntityFlags.IsPositionSet) > 0)
				{
					body.Position = position.ToJVector();
				}

				if ((entityFlags & EntityFlags.IsOrientationSet) > 0)
				{
					body.Orientation = orientation.ToJMatrix();
				}

			    controllingBody = body;
		    }
		    else if (shouldAttach)
		    {
				ComputeOffsets(nullablePosition ?? vec3.Zero, nullableOrientation ?? quat.Identity, out var p,
					out var q);

			    bodies = bodies ?? new List<(RigidBody Target, vec3 Position, quat Orientation)>();
				bodies.Add((body, p, q));
		    }

			return body;
		}

		// This function is built for only pseudo-static bodies. Seems unlikely a fan would be needed for other types.
		protected RigidBody[] FanBodies(Scene scene, Shape shape, uint group, int count, vec3 axis, vec3 position,
			quat? nullableOrientation = null)
		{
			ComputeFan(count, position, nullableOrientation, axis, out var positions, out var orientations);

			var results = new RigidBody[count];
			
			for (int i = 0; i < count; i++)
			{
				// TODO: This shares the shape. Does it need to be cloned?
				var body = CreateBody(scene, shape, group, RigidBodyTypes.PseudoStatic, RigidBodyFlags.None, false,
					positions[i], orientations[i]);

				body.Position = positions[i].ToJVector();
				body.Orientation = orientations[i].ToJMatrix();
				body.Tag = this;

				results[i] = body;
			}

			return results;
		}

		public void ResolveHandles(Scene scene)
		{
			// Managing handles in this way simplifies extending classes (since they don't need to manually track
			// handles).
			if (handles != null && handles.Count > 0)
			{
				ResolveHandles(scene, handles);

				// Once handles have been resolved to actual entity references, handles aren't needed anymore.
				handles = null;
			}
		}

		protected virtual void ResolveHandles(Scene scene, List<EntityHandle> handles)
		{
		}

		public virtual void SetPosition(vec3 position, bool shouldInterpolate)
		{
			this.position = position;

			entityFlags |= EntityFlags.IsPositionSet;

			if (!selfUpdate && controllingBody != null)
			{
				var jP = position.ToJVector();

				if (controllingBody.BodyType == RigidBodyTypes.PseudoStatic)
				{
					controllingBody.SetPosition(jP, Game.DeltaTime);
				}
				else
				{
					controllingBody.Position = jP;
				}

				// If a controlling body is set, attachments are updated accordingly (with interpolation) from Update.
				// As such, it'd be wasteful to also update those attachments here.
				if (shouldInterpolate)
				{
					return;
				}
			}


			attachments.ForEach(a => a.Target.Position = position + orientation * a.Position);
			renderAttachments.ForEach(a => a.Target.Position.SetValue(position + orientation * a.Position,
				shouldInterpolate));
			entities?.ForEach(e => e.Target.SetPosition(position + orientation * e.Position, shouldInterpolate));
			bodies?.ForEach(b => b.Target.SetPosition((position + orientation * b.Position).ToJVector(),
				Game.DeltaTime));
		}

		public void SetOrientation(quat orientation, bool shouldInterpolate)
		{
			this.orientation = orientation;

			entityFlags |= EntityFlags.IsOrientationSet;

			if (!selfUpdate && controllingBody != null)
			{
				var matrix = orientation.ToJMatrix();

				if (controllingBody.BodyType == RigidBodyTypes.PseudoStatic)
				{
					controllingBody.SetOrientation(matrix, Game.DeltaTime);
				}
				else
				{
					controllingBody.Orientation = matrix;
				}

				// See the similar comment in SetPosition above.
				if (shouldInterpolate)
				{
					return;
				}
			}

			attachments.ForEach(a =>
			{
				var target = a.Target;
				target.Position = position + orientation * a.Position;
				target.Orientation = orientation * a.Orientation;
			});

			renderAttachments.ForEach(a =>
			{
				var target = a.Target;
				target.Position.SetValue(position + orientation * a.Position, shouldInterpolate);
				target.Orientation.SetValue(orientation * a.Orientation, shouldInterpolate);
			});

			entities?.ForEach(e =>
			{
				var target = e.Target;
				target.SetPosition(position + orientation * e.Position, shouldInterpolate);
				target.SetOrientation(orientation * e.Orientation, shouldInterpolate);
			});

			bodies?.ForEach(b =>
			{
				var body = b.Target;
				body.SetPosition((position + orientation * b.Position).ToJVector(), Game.DeltaTime);
				body.SetOrientation((orientation * b.Orientation).ToJMatrix(), Game.DeltaTime);
			});
		}

		public void SetTransform(vec3 position, quat orientation, bool shouldInterpolate)
		{
			this.position = position;
			this.orientation = orientation;

			entityFlags |= EntityFlags.IsPositionSet | EntityFlags.IsOrientationSet;

			if (!selfUpdate && controllingBody != null)
			{
				var jP = position.ToJVector();
				var matrix = orientation.ToJMatrix();

				if (controllingBody.BodyType == RigidBodyTypes.PseudoStatic)
				{
					controllingBody.SetPosition(jP, Game.DeltaTime);
					controllingBody.SetOrientation(matrix, Game.DeltaTime);
				}
				else
				{
					controllingBody.Position = jP;
					controllingBody.Orientation = matrix;
				}
			}

			attachments.ForEach(a =>
			{
				var target = a.Target;
				target.Position = position + orientation * a.Position;
				target.Orientation = orientation * a.Orientation;
			});

			renderAttachments.ForEach(a =>
			{
				var target = a.Target;
				target.Position.SetValue(position + orientation * a.Position, shouldInterpolate);
				target.Orientation.SetValue(orientation * a.Orientation, shouldInterpolate);
			});

			entities?.ForEach(e =>
			{
				e.Target.SetTransform(position + orientation * e.Position, orientation * e.Orientation,
					shouldInterpolate);
			});

			bodies?.ForEach(b =>
			{
				var body = b.Target;
				body.SetPosition((position + orientation * b.Position).ToJVector(), Game.DeltaTime);
				body.SetOrientation((orientation * b.Orientation).ToJMatrix(), Game.DeltaTime);
			});
		}
		
		public virtual bool OnContact(Entity entity, RigidBody body, vec3 p, vec3 normal)
		{
			return true;
		}

		// Note that for this callback, the point is the position on the triangle, not the entity.
		public virtual bool OnContact(vec3 p, vec3 normal, vec3[] triangle)
		{
			return true;
		}

		public virtual void Dispose()
		{
			foreach (var (type, target, _, _) in attachments)
			{
				switch (type)
				{
					case EntityAttachmentTypes.Body:
						Scene.World.Remove(((TransformableBody)target).Body);

						break;

					case EntityAttachmentTypes.Sensor:
						Scene.Space.Remove((Sensor)target);

						break;
				}
			}

			foreach (var (type, target, _, _) in renderAttachments)
			{
				switch (type)
				{
					case EntityAttachmentTypes.Model:
						Scene.Renderer.Remove((Model)target);

						break;


					case EntityAttachmentTypes.Skeleton:
						Scene.Renderer.Remove((Skeleton)target);

						break;
				}

			}

			if (controllingBody != null && (entityFlags & EntityFlags.PreserveBody) == 0)
			{
				Scene.World.Remove(controllingBody);
			}
		}

		public virtual void Update()
		{
			Components.Update();

			// TODO: Can controlling bodies be static?
			// Static bodies can't move, so there'd be no point updating transform.
		    if (controllingBody != null && controllingBody.BodyType != RigidBodyTypes.Static)
		    {
			    var p = controllingBody.Position.ToVec3();
			    var q = controllingBody.Orientation.ToQuat();

		        selfUpdate = true;			
				SetTransform(p, q, true);
		        selfUpdate = false;
		    }
		}

		public virtual void Draw()
		{
		}
	}
}
