﻿using System.Collections.Generic;
using System.Diagnostics;
using Engine.Graphics._3D;
using Engine.Graphics._3D.Rendering;
using Engine.Interfaces;
using Engine.Props;
using Engine.Sensors;
using Engine.UI;
using Engine.View;
using Jitter;

namespace Engine.Entities
{
	public class Scene : IDynamic
	{
		private Camera3D camera;
		private MasterRenderer3D renderer;
		private SceneFragment lastFragment;
		private PrimitiveRenderer3D primitives;

		private Dictionary<int, List<Entity>> entities;
		private List<Entity> addList;
		private List<Entity> removeList;

		private bool isUpdateActive;

		public Scene()
		{
			entities = new Dictionary<int, List<Entity>>();
			addList = new List<Entity>();
			removeList = new List<Entity>();
			Tags = new Dictionary<string, object>();
		}

		public Camera3D Camera
		{
			get => camera;
			set
			{
				camera = value;

				// TODO: Consider making this reloadable.
				var accessor = Properties.Access();

				renderer = new MasterRenderer3D();
				renderer.ShadowNearPlane = accessor.GetFloat("shadow.near.plane");
				renderer.ShadowFarPlane = accessor.GetFloat("shadow.far.plane");

				primitives = new PrimitiveRenderer3D(value, 400000, 40000);
			}
		}

		public Canvas Canvas { get; set; }
		public Space Space { get; set; }
		public World World { get; set; }
		public MasterRenderer3D Renderer => renderer;
		public PrimitiveRenderer3D Primitives => primitives;

		// TODO: Are tags needed?
		// In this context, a "tag" means custom data optionally loaded with each fragment. Used as needed in order to
		// implement custom features for different kinds of locations.
		public Dictionary<string, object> Tags { get; }

		// TODO: This is temporary to respawn the player.
		public SceneFragment LastFragment => lastFragment;

		// TODO: Consider generating a fragment ID (in order to ensure that entity IDs are unique among fragments).
		public SceneFragment LoadFragment(string filename)
		{
			// TODO: Track fragments (and assert for duplicate fragments).
			var fragment = SceneFragment.Load(filename, this);
			var eList = fragment.Entities;
			var body = fragment.MapBody;

			renderer.Add(fragment.MapModel);

			if (body != null)
			{
				World.AddBody(body);
			}

			// It's valid (though unlikely) for a fragment to contain no entities.
			if (eList != null)
			{
				foreach (var e in eList)
				{
					// Entities are initialized by the fragment (so they don't need to be initialized again when added
					// to the appropriate group).
					AddInternal(e, false);
				}

				foreach (var e in eList)
				{
					// Handles must be resolved after all entities are initialized *and* added to their corresponding
					// groups lists.
					e.ResolveHandles(this);
				}
			}

			lastFragment = fragment;

			return fragment;
		}

		// TODO: Should this be moved to the fragment class?
		public void UnloadFragment(SceneFragment fragment)
		{
			Renderer.Remove(fragment.MapModel);

			var body = fragment.MapBody;

			if (body != null)
			{
				World.Remove(body);
			}

			foreach (var e in fragment.Entities)
			{
				e.Dispose();
				entities[e.Group].Remove(e);
			}
		}

		// TODO: Reload was previously called from the gameplay loop. Should probably be called instead as a terminal command.
		/*
		public void Reload()
		{
			var filename = lastFragment.Filename;

			UnloadFragment(lastFragment);
			LoadFragment(filename);

			var player = (PlayerCharacter)entities[(int)EntityGroups.Player][0];
			var p = lastFragment.Origin + lastFragment.Spawn;

			player.Reset(p);
			primitives.Clear();
		}
		*/

		public void Add(Entity entity)
		{
			if (!isUpdateActive)
			{
				AddInternal(entity, true);
			}
			else
			{
				addList.Add(entity);
			}
		}

		private void AddInternal(Entity entity, bool shouldInitialize)
		{
			var group = entity.Group;

			if (!entities.TryGetValue(group, out var list))
			{
				list = new List<Entity>();
				entities.Add(group, list);
			}

			list.Add(entity);

			if (shouldInitialize)
			{
				entity.Initialize(this, null);
			}
		}

		public void Remove(Entity entity)
		{
			if (!isUpdateActive)
			{
				RemoveInternal(entity);
			}
			else
			{
				removeList.Add(entity);
			}
		}

		private void RemoveInternal(Entity entity)
		{
			var group = entity.Group;
			
			Debug.Assert(entities[group].Contains(entity), "Attempting to remove entity that's not currently part " +
				"of the scene (this likely means that it was already removed).");

			entity.Dispose();
			entities[group].Remove(entity);
		}

		public List<Entity> GetEntities(int group)
		{
			return entities[group];
		}

		public List<T> GetEntities<T>(int group) where T : Entity
		{
			var list = new List<T>();

			foreach (var e in entities[group])
			{
				if (e is T item)
				{
					list.Add(item);
				}
			}

			return list;
		}

		public void Dispose()
		{
		}

		public void Update()
		{
			isUpdateActive = true;

			foreach (var list in entities.Values)
			{
				list.ForEach(e =>
				{
					if (e.IsUpdateEnabled)
					{
						e.Update();
					}
				});
			}

			isUpdateActive = false;

			removeList.ForEach(RemoveInternal);
			removeList.Clear();

			addList.ForEach(e => AddInternal(e, true));
			addList.Clear();
		}

		// TODO: t might not be needed here (since matrices were already recomputed in the render target step).
		public void Draw(Camera3D camera, float t)
		{
			foreach (var list in entities.Values)
			{
				list.ForEach(e =>
				{
					if (e.IsDrawEnabled)
					{
						e.Draw();
					}
				});
			}

			renderer.VpMatrix = camera.ViewProjection;
			renderer.Draw(t);

			primitives.Flush();
		}
	}
}
