﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Engine.Input;
using Engine.Interfaces;
using Engine.Messaging;
using Engine.Props;
using static Engine.GLFW;

namespace Engine
{
	// TODO: Pull monitor data on launch (and react accordingly).
	// TODO: Consider responding to monitors being disconnected (might not be worth the trouble).
	public abstract class Game : IReceiver
	{
		private const int UpdateFramerate = 75;

		public const float DeltaTime = 1f / UpdateFramerate;

		// The input processor (and associated callbacks) are static to avoid the garbage collector moving things
		// around at runtime (which would cause an error in GLFW).
		private static InputProcessor inputProcessor;
		private static GLFWkeyfun keyCallback;
		private static GLFWcursorposfun cursorPositionCallback;
		private static GLFWmousebuttonfun mouseButtonCallback;
		private static GLFWwindowfocusfun focusCallback;

		static Game()
		{
			keyCallback = KeyCallback;
			cursorPositionCallback = CursorPositionCallback;
			mouseButtonCallback = MouseButtonCallback;
			focusCallback = FocusCallbackInternal;
		}

		public static Action<bool> FocusCallback;

		// This is used to spawn entities from the terminal (among other usages probably).
		public static string Assembly { get; private set; }

		private static void KeyCallback(IntPtr window, int key, int scancode, int action, int mods)
		{
			// Key can be negative in some cases (like alt + printscreen).
			if (key == -1)
			{
				return;
			}

			switch (action)
			{
				case GLFW_PRESS: inputProcessor.OnKeyPress(key, mods); break;
				case GLFW_RELEASE: inputProcessor.OnKeyRelease(key); break;
			}
		}

		private static void CursorPositionCallback(IntPtr window, double x, double y)
		{
			// TODO: Do these need to be cast to integers?
			// TODO: If focus is lost and regained, might need to add code to prevent an artifically huge delta.
			int mX = (int)Math.Round(x);
			int mY = (int)Math.Round(y);

			inputProcessor.OnMouseMove(mX, mY);
		}

		private static void MouseButtonCallback(IntPtr window, int button, int action, int mods)
		{
			switch (action)
			{
				case GLFW_PRESS: inputProcessor.OnMouseButtonPress(button); break;
				case GLFW_RELEASE: inputProcessor.OnMouseButtonRelease(button); break;
			}
		}

		private static void FocusCallbackInternal(IntPtr window, int isFocused)
		{
			FocusCallback?.Invoke(isFocused != 0);
		}

		private float previousTime;
		private float updateAccumulator;
		private float renderAccumulator;

		protected Window window;

		protected Game(string title, string assembly)
		{
			Debug.Assert(!string.IsNullOrEmpty(title), "Game title can't be null or empty.");
			Debug.Assert(!string.IsNullOrEmpty(assembly), "Game assembly can't be null or empty.");

			Assembly = assembly;

			glfwInit();
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			IntPtr address = glfwCreateWindow(Resolution.WindowWidth, Resolution.WindowHeight, title, IntPtr.Zero,
				IntPtr.Zero);

			if (address == IntPtr.Zero)
			{
				glfwTerminate();

				return;
			}

			window = new Window(title, 800, 600, address);
			inputProcessor = InputProcessor.Instance;

			// TODO: Is glfwSetInputMode(window, GLFW_LOCK_KEY_MODS, GLFW_TRUE); needed? (see the GLFW input guide)
			// TODO: Consider using raw mouse motion for in-game (i.e. non-cursor) control
			glfwMakeContextCurrent(address);
			glfwSetKeyCallback(address, keyCallback);
			glfwSetCursorPosCallback(address, cursorPositionCallback);
			glfwSetMouseButtonCallback(address, mouseButtonCallback);
			glfwSetWindowFocusCallback(address, focusCallback);
			//glfwSetWindowPos(address, 2100, 100);

			MessageSystem.Subscribe(this, CoreMessageTypes.Exit, data =>
			{
				glfwSetWindowShouldClose(window.Address, 1);
			});
		}

		public List<MessageHandle> MessageHandles { get; set; }

		public int RenderFramerate { get; set; } = 60;

		// TODO: Consider using multithreading in the future (probably splitting update and draw).
		public void Run()
		{
			float renderTick = 1.0f / RenderFramerate;

			while (glfwWindowShouldClose(window.Address) == 0)
			{
				var time = (float)glfwGetTime();
				var delta = time - previousTime;

				updateAccumulator += delta;
				renderAccumulator += delta;
				previousTime = time;
				
				// TODO: Consider using a while loop (with maximum iterations).
				if (updateAccumulator >= DeltaTime)
				{
					glfwPollEvents();
					inputProcessor.Refresh();
					Update();

					// TODO: If a while loop is used, this should revert back to -= (rather than modulus).
					updateAccumulator %= DeltaTime;
				}
				
				if (renderAccumulator >= renderTick)
				{
					// TODO: If a while loop is used above, the t value will need to use modulus.
					Draw(updateAccumulator / DeltaTime);
					glfwSwapBuffers(window.Address);
					renderAccumulator %= renderTick;
				}
			}

			glfwTerminate();
		}

		public virtual void Dispose()
		{
		}

		public void ToggleCursor(bool isVisible)
		{
			glfwSetInputMode(window.Address, GLFW_CURSOR, isVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
		}

		protected abstract void Update();
		protected abstract void Draw(float t);
	}
}
