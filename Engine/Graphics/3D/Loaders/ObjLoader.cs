﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using GlmSharp;

namespace Engine.Graphics._3D.Loaders
{
	public static class ObjLoader
	{
		public static Dictionary<string, Mesh> Load(string filename)
		{
			var lines = File.ReadAllLines(Paths.Meshes + filename);

			// The default Blender export uses individual objects for each mesh (lines staring with "o"). Object groups
			// start with "g" instead, but are functionally the same (for now, anyway).
			var usesObjectGroups = lines.Any(l => l.StartsWith("g ") || l.StartsWith("o "));

			Dictionary<string, Mesh> results;

			if (usesObjectGroups)
			{
				results = ParseGroupedMeshes(lines);
			}
			else
			{
				// If object groups aren't used, all meshes in the file are combined into a single larger mesh.
				results = new Dictionary<string, Mesh>();
				results.Add("", ParseUngroupedMesh(filename, lines));
			}

			return results;
		}

		private static Dictionary<string, Mesh> ParseGroupedMeshes(string[] lines)
		{
			var map = new Dictionary<string, Mesh>();
			var index = 0;

			Mesh mesh;

			while ((mesh = ParseMesh(lines, ref index, out var name)) != null)
			{
				map.Add(name, mesh);
			}

			return map;
		}

		private static Mesh ParseMesh(string[] lines, ref int index, out string name)
		{
			// This means that the end of the file was reached.
			if (index >= lines.Length - 1 || lines[index].Length == 0)
			{
				name = null;

				return null;
			}

			string line = lines[index];

			while (!(line.StartsWith("o ") || line.StartsWith("g ")))
			{
				line = lines[++index];
			}

			name = line.Split(' ')[1];

			// Custom object names often end with "_[Shape].XYZ", such as "Box_Cube.001". In those cases, the user-set
			// name is just the first portion. The last underscore index is used in case the object name itself
			// contains underscores.
			var underscoreIndex = name.LastIndexOf("_", StringComparison.CurrentCulture);

			if (underscoreIndex > 0)
			{
				name = name.Substring(0, underscoreIndex);
			}

			var points = new List<vec3>();
			var normals = new List<vec3>();

			// Parse points and normals.
			do
			{
				line = lines[++index];

				if (line.StartsWith("v "))
				{
					points.Add(ParseVec3(line));
				}
				else if (line.StartsWith("vn "))
				{
					normals.Add(ParseVec3(line));
				}
			}
			while (line.StartsWith("v ") || line.StartsWith("vn "));

			// TODO: Support textures.
			var source = new List<vec2>();
			source.Add(vec2.Zero);

			// Advance to face lines.
			while (!lines[++index].StartsWith("f "))
			{
			}

			var vertices = new List<ivec3>();
			var indices = new List<ushort>();

			while (index < lines.Length - 1 && (line = lines[index]).StartsWith("f "))
			{
				var tokens = line.Split(' ');

				for (int i = 1; i <= 3; i++)
				{
					var subTokens = tokens[i].Split('/');

					// TODO: Support textures (using the second sub-token).
					// .obj files use 1-indexing (rather than 0-indexing).
					var pointIndex = int.Parse(subTokens[0]) - 1;
					var normalIndex = int.Parse(subTokens[2]) - 1;
					var vertex = new ivec3(pointIndex, 0, normalIndex);

					int vIndex;

					if ((vIndex = vertices.IndexOf(vertex)) != -1)
					{
						indices.Add((ushort)vIndex);
					}
					else
					{
						indices.Add((ushort)vertices.Count);
						vertices.Add(vertex);
					}
				}

				index++;
			}

			// When multiple meshes are present in a Blender scene, vertices are based on absolute position. In order
			// to make positioning the mesh easier in-game, points are normalized back to the origin.
			var minX = points.Min(p => p.x);
			var minY = points.Min(p => p.y);
			var minZ = points.Min(p => p.z);

			for (int i = 0; i < points.Count; i++)
			{
				points[i] -= new vec3(minX, minY, minZ);
			}

			// Same applies to indexes.
			var minPointIndex = vertices.Min(v => v.x);
			var minNormalIndex = vertices.Min(v => v.z);

			for (int i = 0; i < vertices.Count; i++)
			{
				var v = vertices[i];
				v.x -= minPointIndex;
				v.z -= minNormalIndex;
				vertices[i] = v;
			}

			// TODO: Load texture (if present in the file).
			return new Mesh(points.ToArray(), source.ToArray(), normals.ToArray(), vertices.ToArray(),
				indices.ToArray(), null);
		}

		private static Mesh ParseUngroupedMesh(string filename, string[] lines)
		{
			var points = lines.Where(l => l.StartsWith("v ")).Select(ParseVec3);
			var normals = lines.Where(l => l.StartsWith("vn")).Select(ParseVec3);
			var sources = lines.Where(l => l.StartsWith("vt")).Select(ParseVec2).ToList();

			bool isUvMapped;
			string texture;

			if (sources.Count > 0)
			{
				texture = ParseTexture(filename);
				isUvMapped = true;
			}
			// If no material is specified, a default grey texture is used instead.
			else
			{
				texture = "Grey.png";
				sources.Add(vec2.Zero);
				isUvMapped = false;
			}

			var vertices = new List<ivec3>();
			var indices = new List<ushort>();

			foreach (var line in lines.Where(l => l.StartsWith("f ")))
			{
				var tokens = line.Split(' ');

				for (int i = 1; i <= 3; i++)
				{
					var subTokens = tokens[i].Split('/');

					// .obj files use 1-indexing (rather than 0-indexing).
					var pointIndex = int.Parse(subTokens[0]) - 1;
					var sourceIndex = isUvMapped ? int.Parse(subTokens[1]) - 1 : 0;
					var normalIndex = int.Parse(subTokens[2]) - 1;
					var vertex = new ivec3(pointIndex, sourceIndex, normalIndex);

					int index;

					if ((index = vertices.IndexOf(vertex)) != -1)
					{
						indices.Add((ushort)index);
					}
					else
					{
						indices.Add((ushort)vertices.Count);
						vertices.Add(vertex);
					}
				}
			}

			return new Mesh(points.ToArray(), sources.ToArray(), normals.ToArray(), vertices.ToArray(),
				indices.ToArray(), texture);
		}

		private static vec2 ParseVec2(string line)
		{
			var tokens = line.Split(' ');
			var x = float.Parse(tokens[1]);
			var y = float.Parse(tokens[2]);

			return new vec2(x, y);
		}

		private static vec3 ParseVec3(string line)
		{
			var tokens = line.Split(' ');
			var x = float.Parse(tokens[1]);
			var y = float.Parse(tokens[2]);
			var z = float.Parse(tokens[3]);

			return new vec3(x, y, z);
		}

		private static string ParseTexture(string meshFilename)
		{
			// This assumes that every material file will use the same as the corresponding mesh (with the ".mtl"
			// extension). It also assumes the mesh + material will be in the same directory.
			var lines = File.ReadAllLines(Paths.Meshes + meshFilename.StripExtension() + ".mtl");
			var line = lines.First(l => l.StartsWith("map_Kd"));

			return line.Split('\\').Last();
		}
	}
}
