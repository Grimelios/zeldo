﻿using System;
using System.Collections.Generic;
using Engine.Core;
using Engine.Shaders;
using Engine.Shapes._2D;
using Engine.Shapes._3D;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using static Engine.GL;

namespace Engine.Graphics._3D
{
	// TODO: At time of writing, primitives are broken when rendering multiple modes. Should be fixed.
	public class PrimitiveRenderer3D : IDisposable
	{
		private Camera3D camera;
		private Shader defaultShader;
		private Shader customShader;
		private PrimitiveBuffer buffer;

		private uint bufferId;
		private uint indexId;
		private uint mode;

		public PrimitiveRenderer3D(Camera3D camera, int bufferSize, int indexSize)
		{
			this.camera = camera;

			buffer = new PrimitiveBuffer(bufferSize, indexSize);

			GLUtilities.AllocateBuffers(bufferSize, indexSize, out bufferId, out indexId, GL_DYNAMIC_DRAW);

			defaultShader = new Shader(bufferId, indexId);
			defaultShader.Attach(ShaderTypes.Vertex, "Primitives3D.vert");
			defaultShader.Attach(ShaderTypes.Fragment, "Primitives.frag");
			defaultShader.AddAttribute<float>(3, GL_FLOAT);
			defaultShader.AddAttribute<byte>(4, GL_UNSIGNED_BYTE, ShaderAttributeFlags.IsNormalized);
			defaultShader.Initialize();
		}

		public void Apply(Shader shader)
		{
			if (shader == customShader)
			{
				return;
			}

			Flush();
			customShader = shader;

			if (customShader != null && !customShader.IsBindingComplete)
			{
				customShader.Bind(bufferId, indexId);
			}
		}

		public void Dispose()
		{
			defaultShader.Dispose();

			GLUtilities.DeleteBuffers(bufferId, indexId);
		}

		public void Draw(Arc arc, float y, Color color, int segments)
		{
			var center = arc.Position;
			var points = new vec3[segments + 2];
			var start = arc.Angle - arc.Spread / 2;
			var increment = arc.Spread / segments;

			for (int i = 0; i <= segments; i++)
			{
				vec2 p = center + Utilities.Direction(start + increment * i) * arc.Radius;

				points[i] = new vec3(p.x, y, p.y);
			}

			points[points.Length - 1] = new vec3(center.x, y, center.y);

			Buffer(points, color, GL_LINE_LOOP);
		}

		// TODO: Add a function to draw 3D shapes in general.
		public void Draw(Box box, Color color)
		{
			var halfSize = new vec3(box.Width, box.Height, box.Depth) / 2;
			var min = -halfSize;
			var max = halfSize;
			var points = new []
			{
				min,
				new vec3(min.x, min.y, max.z),
				new vec3(min.x, max.y, max.z),
				new vec3(min.x, max.y, min.z),
				max,
				new vec3(max.x, max.y, min.z),
				new vec3(max.x, min.y, min.z),
				new vec3(max.x, min.y, max.z) 
			};

			if (box.IsOrientable)
			{
				for (int i = 0; i < points.Length; i++)
				{
					points[i] = box.Orientation * points[i];
				}
			}

			for (int i = 0; i < points.Length; i++)
			{
				points[i] += box.Position;
			}

			ushort[] indices =
			{
				0, 1, 1, 2, 2, 3, 3, 0,
				4, 5, 5, 6, 6, 7, 7, 4,
				0, 6, 1, 7, 2, 4, 3, 5
			};

			Buffer(points, color, GL_LINES, indices);
		}

		public void Draw(Capsule capsule, Color color)
		{
			const int Segments = 16;
			const int Rings = 5;

			float h = capsule.Height / 2;
			float r = capsule.Radius;

			var orientation = capsule.Orientation;
			var p = capsule.Position;
			var v = orientation * vec3.UnitY;

			for (int i = 0; i < Rings; i++)
			{
				float radius = (float)Math.Cos(Constants.PiOverTwo / Rings * i) * r;
				float offset = r / Rings * i;

				DrawCircle(radius, p + v * (h + offset), orientation, color, Segments);
				DrawCircle(radius, p - v * (h + offset), orientation, color, Segments);
			}

			for (int i = 0; i < Segments; i++)
			{
				var d = Utilities.Direction(Constants.TwoPi / Segments * i) * r;
				var point = orientation * new vec3(d.x, 0, d.y) + p;

				DrawLine(point + v * h, point - v * h, color);
			}
		}

		public void Draw(Cylinder cylinder, Color color)
		{
			const int Segments = 16;
			const float Increment = Constants.TwoPi / Segments;

			var orientation = cylinder.Orientation;
			var center = cylinder.Position;
			var v = orientation * new vec3(0, cylinder.Height, 0);
			var c = center - v / 2;
			var radius = cylinder.Radius;

			DrawCircle(radius, c, orientation, color, Segments);
			DrawCircle(radius, center + v / 2, orientation, color, Segments);

			vec3[] points = new vec3[Segments];

			for (int i = 0; i < points.Length; i++)
			{
				vec2 p = Utilities.Direction(Increment * i) * radius;

				points[i] = orientation * new vec3(p.x, 0, p.y) + c;
			}

			for (int i = 0; i < Segments; i++)
			{
				vec3 p = points[i];

				DrawLine(p, p + v, color);
			}
		}

		public void DrawCircle(float radius, vec3 center, quat orientation, Color color, int segments)
		{
			var points = new vec3[segments];
			var increment = Constants.TwoPi / segments;

			for (int i = 0; i < points.Length; i++)
			{
				var p = Utilities.Direction(increment * i) * radius;

				points[i] = orientation * new vec3(p.x, 0, p.y) + center;
			}

			Buffer(points, color, GL_LINE_LOOP);
		}

		public void Draw(vec3 p, Color color)
		{
			Buffer(new [] { p }, color, GL_POINTS);
		}

		public void Draw(Line3D line, Color color)
		{
			DrawLine(line.P1, line.P2, color);
		}

		public void DrawLine(vec3 p1, vec3 p2, Color color)
		{
			vec3[] points = { p1, p2 };

			Buffer(points, color, GL_LINES);
		}

		public void DrawTriangle(vec3 p0, vec3 p1, vec3 p2, Color color)
		{
			DrawTriangle(new [] { p0, p1, p2 }, color);
		}

		public void DrawTriangle(vec3[] points, Color color)
		{
			Buffer(points, color, GL_LINE_LOOP, new ushort[] { 0, 1, 2 });
		}

		public void Fill(vec3[] strip, Color color)
		{
			Buffer(strip, color, GL_TRIANGLE_STRIP);
		}
		
		// TODO: Consider removing this function (it was added to render water using filled triangles, but likely won't be needed later).
		public void Fill(vec3[] strips, int height, Color color)
		{
			VerifyMode(GL_TRIANGLE_STRIP);

			var width = strips.Length / height;
			var indices = new ushort[height - 1][];

			for (int i = 0; i < height - 1; i++)
			{
				var array = new ushort[width * 2];
				indices[i] = array;

				for (int j = 0; j < width; j++)
				{
					array[j * 2] = (ushort)(i * width + j);
					array[j * 2 + 1] = (ushort)((i + 1) * width + j);
				}
			}

			buffer.BufferData(GetData(strips, color));

			for (int i = 0; i < indices.Length; i++)
			{
				buffer.BufferIndexes(indices[i], i == indices.Length - 1);
			}
		}

		private float[] GetData(vec3[] points, Color color)
		{
			var f = color.ToFloat();
			var data = new float[points.Length * 4];

			for (int i = 0; i < points.Length; i++)
			{
				var start = i * 4;
				var p = points[i];

				data[start] = p.x;
				data[start + 1] = p.y;
				data[start + 2] = p.z;
				data[start + 3] = f;
			}

			return data;
		}

		/*
		// This is useful for things like lily pads, which use a custom triangle mesh for rendering.
		public void BufferStrips(vec3[] points, ushort[] indices, Color color)
		{
			VerifyMode(GL_TRIANGLE_STRIP);

			buffer.BufferData(GetData(points, color));
			buffer.BufferIndexes(indices, true);
		}
		*/

		private void Buffer(vec3[] points, Color color, uint mode, ushort[] indices = null)
		{
			VerifyMode(mode);

			var data = GetData(points, color);

			// If the index array is null, it's assumed that indices can be added sequentially.
			if (indices == null)
			{
				indices = new ushort[points.Length];

				for (int i = 0; i < indices.Length; i++)
				{
					indices[i] = (ushort)i;
				}
			}

			buffer.Buffer(data, indices);
		}

		private void VerifyMode(uint mode)
		{
			if (this.mode != mode)
			{
				Flush();

				this.mode = mode;

				buffer.Mode = mode;
			}
		}

		public void Clear()
		{
			buffer.Clear();
		}

		public unsafe void Flush()
		{
			if (buffer.Size == 0)
			{
				return;
			}

			Shader shader;

			// If a custom shader is used, it's assumed to only apply to the current batch of primitives.
			if (customShader != null)
			{
				shader = customShader;
				customShader = null;
			}
			else
			{
				shader = defaultShader;
			}

			shader.Apply();
			shader.SetUniform("mvp", camera.ViewProjection);

			glDrawElements(mode, buffer.Flush(), GL_UNSIGNED_SHORT, null);

			Statistics.Increment(RenderKeys.DrawCalls);
		}
	}
}
