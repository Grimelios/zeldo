﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using static Engine.GL;

namespace Engine.Graphics
{
	public class PrimitiveBuffer
	{
		private static readonly uint[] restartModes =
		{
			GL_LINE_LOOP,
			GL_LINE_STRIP,
			GL_TRIANGLE_FAN,
			GL_TRIANGLE_STRIP
		};

		private int bufferSize;
		private int maxIndex;

		private byte[] buffer;
		private ushort[] indexBuffer;
		private bool isPrimitiveRestartEnabled;

		public PrimitiveBuffer(int bufferCapacity, int indexCapacity)
		{
			buffer = new byte[bufferCapacity];
			indexBuffer = new ushort[indexCapacity];
			maxIndex = -1;
		}

		public int Size => bufferSize;
		public int IndexCount { get; private set; }

		public uint Mode
		{
			set => isPrimitiveRestartEnabled = restartModes.Contains(value);
		}

		public void Buffer<T>(T[] data, ushort[] indices, int start = 0, int length = -1) where T : struct
		{
			BufferData(data, start, length);
			BufferIndexes(indices, true);
		}

		// In some cases, data and indices can be buffered separately (e.g. water, where a single, large array of
		// points are buffered, followed by many individual strips of indices).
		public void BufferData<T>(T[] data, int start = 0, int length = -1)
		{
			int size = Marshal.SizeOf(typeof(T));
			int sizeInBytes = size * (length != -1 ? length : data.Length);

			// See https://stackoverflow.com/a/4636735/7281613.
			System.Buffer.BlockCopy(data, start * size, buffer, bufferSize, sizeInBytes);

			bufferSize += sizeInBytes;
		}

		public void BufferIndexes(ushort[] indices, bool shouldRecomputeMax)
		{
			if (shouldRecomputeMax)
			{
				var max = -1;

				for (int i = 0; i < indices.Length; i++)
				{
					var index = indices[i];

					indexBuffer[IndexCount + i] = (ushort)(maxIndex + index + 1);
					max = Math.Max(max, index);
				}

				maxIndex += max + 1;
			}
			else
			{
				for (int i = 0; i < indices.Length; i++)
				{
					indexBuffer[IndexCount + i] = (ushort)(maxIndex + indices[i] + 1);
				}
			}

			IndexCount += indices.Length;

			if (isPrimitiveRestartEnabled)
			{
				indexBuffer[IndexCount] = Constants.RestartIndex;
				IndexCount++;
			}
		}

		// This clears the buffer *without* issuing a draw call.
		public void Clear()
		{
			bufferSize = 0;
			IndexCount = 0;
			maxIndex = -1;
		}

		public unsafe uint Flush()
		{
			if (isPrimitiveRestartEnabled)
			{
				glEnable(GL_PRIMITIVE_RESTART);
			}
			else
			{
				glDisable(GL_PRIMITIVE_RESTART);
			}

			// It's assumed that a relevant shader will be applied before calling this function (such that the correct
			// buffers are already bound).
			fixed (byte* address = &buffer[0])
			{
				glBufferSubData(GL_ARRAY_BUFFER, 0, (uint)bufferSize, address);
			}

			fixed (ushort* address = &indexBuffer[0])
			{
				glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(ushort) * (uint)IndexCount, address);
			}

			uint count = (uint)IndexCount;

			Clear();

			return count;
		}
	}
}
