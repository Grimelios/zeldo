﻿using Engine.Input.Data;
using Engine.Input.Data.Controllers;
using Engine.Utility;
using Engine.XInput;
using GlmSharp;

namespace Engine.Input
{
	internal class GamepadHelper
	{
		private GamePadState oldState;

		public GamepadData CreateData()
		{
			var newState = GamePad.GetState(PlayerIndex.One);
			var newButtons = newState.Buttons;
			var newDPad = newState.DPad;
			var newTriggers = newState.Triggers;
			var newJoysticks = newState.ThumbSticks;

			var oldButtons = oldState.Buttons;
			var oldDPad = oldState.DPad;
			var oldTriggers = oldState.Triggers;
			var oldJoysticks = oldState.ThumbSticks;

			var buttons = new InputStates[Utilities.EnumCount<XboxButtons>()];
			buttons[(int)XboxButtons.A] = GetState(oldButtons.A, newButtons.A);
			buttons[(int)XboxButtons.B] = GetState(oldButtons.B, newButtons.B);
			buttons[(int)XboxButtons.X] = GetState(oldButtons.X, newButtons.X);
			buttons[(int)XboxButtons.Y] = GetState(oldButtons.Y, newButtons.Y);
			buttons[(int)XboxButtons.Start] = GetState(oldButtons.Start, newButtons.Start);
			buttons[(int)XboxButtons.Back] = GetState(oldButtons.Back, newButtons.Back);
			buttons[(int)XboxButtons.L3] = GetState(oldButtons.LeftStick, newButtons.LeftStick);
			buttons[(int)XboxButtons.R3] = GetState(oldButtons.RightStick, newButtons.RightStick);
			buttons[(int)XboxButtons.LeftBumper] = GetState(oldButtons.LeftShoulder, newButtons.LeftShoulder);
			buttons[(int)XboxButtons.RightBumper] = GetState(oldButtons.RightShoulder, newButtons.RightShoulder);
			buttons[(int)XboxButtons.DPadUp] = GetState(oldDPad.Up, newDPad.Up);
			buttons[(int)XboxButtons.DPadDown] = GetState(oldDPad.Down, newDPad.Down);
			buttons[(int)XboxButtons.DPadLeft] = GetState(oldDPad.Left, newDPad.Left);
			buttons[(int)XboxButtons.DPadRight] = GetState(oldDPad.Right, newDPad.Right);
			buttons[(int)XboxButtons.L3] = GetState(oldButtons.LeftStick, newButtons.RightStick);
			buttons[(int)XboxButtons.R3] = GetState(oldButtons.RightStick, newButtons.RightStick);

			var leftStick = new JoystickData(new vec2(newJoysticks.Left.X, newJoysticks.Left.Y),
				new vec2(oldJoysticks.Left.X, oldJoysticks.Left.Y));
			var rightStick = new JoystickData(new vec2(newJoysticks.Right.X, newJoysticks.Right.Y),
				new vec2(oldJoysticks.Right.X, oldJoysticks.Right.Y));

			oldState = newState;

			return new GamepadData(InputTypes.Xbone, buttons, leftStick, rightStick);
		}

		private InputStates GetState(ButtonState oldState, ButtonState newState)
		{
			if (oldState == newState)
			{
				return newState == ButtonState.Pressed ? InputStates.Held : InputStates.Released;
			}

			return newState == ButtonState.Pressed ? InputStates.PressedThisFrame : InputStates.ReleasedThisFrame;
		}
	}
}
