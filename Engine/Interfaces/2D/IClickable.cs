﻿using GlmSharp;

namespace Engine.Interfaces._2D
{
	public interface IClickable : IPositionContainer2D
	{
		bool IsEnabled { get; set; }

		// TODO: For high-DPI mouse settings, might have to make these regular vec2s.
		void OnHover(ivec2 mouseLocation);
		void OnUnhover();

		// TODO: Could consider returning a boolean from this function (to indicate whether any action was taken), but that's probably not needed (since if it's enabled, some action will be taken on click).
		void OnClick(ivec2 mouseLocation);

		bool Contains(ivec2 mouseLocation);
	}
}
