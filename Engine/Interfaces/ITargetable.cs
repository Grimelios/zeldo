﻿using GlmSharp;

namespace Engine.Interfaces
{
	public interface ITargetable
	{
		// TODO: Should damage source (an object) be replaced with an interface?
		void OnDamage(int damage, object source = null);
		void OnHit(int damage, int knockback, vec3 p, vec3 direction, object source = null);
	}
}
