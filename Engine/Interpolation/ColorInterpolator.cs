﻿using System.Diagnostics;
using Engine.Core;
using Engine.Interfaces;

namespace Engine.Interpolation
{
	public class ColorInterpolator : Interpolator<Color>
	{
		private IColorable target;
		private IRenderColorable renderTarget;
		private IColorContainer containerTarget;

		public ColorInterpolator(IColorable target, EaseTypes easeType) :
			this(target, Color.White, Color.White, easeType)
		{
		}

		public ColorInterpolator(IRenderColorable target, EaseTypes easeType) :
			this(target, Color.White, Color.White, easeType)
		{
		}

		public ColorInterpolator(IColorContainer target, EaseTypes easeType) :
			this(target, Color.White, Color.White, easeType)
		{
		}

		public ColorInterpolator(IColorable target, Color start, Color end, EaseTypes easeType, float duration = 0) :
			base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			this.target = target;
		}

		public ColorInterpolator(IRenderColorable target, Color start, Color end, EaseTypes easeType, float duration = 0) :
			base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			renderTarget = target;
		}

		public ColorInterpolator(IColorContainer target, Color start, Color end, EaseTypes easeType, float duration = 0) :
			base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			containerTarget = target;
		}

		protected override void Lerp(float t)
		{
			Color c = Color.Lerp(Start, End, t);

			if (target != null)
			{
				target.Color = c;
			}
			else if (renderTarget != null)
			{
				renderTarget.Color.SetValue(c, true);
			}
			else
			{
				containerTarget.SetColor(c, true);
			}
		}
	}
}
