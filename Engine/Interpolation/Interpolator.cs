﻿using System.Diagnostics;
using Engine.Interfaces;

namespace Engine.Interpolation
{
	// Interpolators are similar to timers, but I think it still makes sense to have them separate (primarily to more
	// easily modify start and end values mid-tick).
	public abstract class Interpolator<T> : IComponent
	{
		protected const string NullMessage = "Can't interpolate on a null target.";

		private float elapsed;
		private float duration;

		private EaseTypes easeType;

		protected Interpolator(T start, T end, float duration, EaseTypes easeType)
		{
			Debug.Assert(duration >= 0, "Duration can't be negative.");

			this.duration = duration;
			this.easeType = easeType;

			Start = start;
			End = end;
		}

		// In some cases, the start and end values need to be modified mid-interpolation.
		public T Start { get; set; }
		public T End { get; set; }

		public bool IsComplete { get; private set; }

		public void Reset()
		{
			elapsed = 0;
			IsComplete = false;
		}

		protected abstract void Lerp(float t);

		// This can be called directly in some cases (such as for certain attacks).
		public void Interpolate(float t)
		{
			Lerp(Ease.Compute(t, easeType));
		}

		public void Update()
		{
			if (IsComplete)
			{
				return;
			}

			elapsed += Game.DeltaTime;

			float t;

			if (elapsed >= duration)
			{
				// This ignores any leftover time (such that the end state is set exactly).
				t = 1;
				IsComplete = true;
			}
			else
			{
				t = elapsed / duration;
			}

			Interpolate(t);
		}
	}
}
