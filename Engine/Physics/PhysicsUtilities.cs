﻿using Engine.Utility;
using GlmSharp;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;

namespace Engine.Physics
{
	// TODO: Consider adding raycast variations that take JVectors directly.
	// TODO: Add optional lambdas to filter results.
	public static class PhysicsUtilities
	{
		// TODO: Consider swapping JVectors for regular vec3.
		public delegate bool RaycastFilter(RigidBody body, JVector[] triangle, vec3 p, vec3 n);

		public static bool Raycast(World world, vec3 start, vec3 end, out RaycastResults results)
		{
			return RaycastInternal(world, null, start, end - start, null, out results);
		}

		public static bool Raycast(World world, vec3 start, vec3 direction, float range, out RaycastResults results,
			RaycastFilter filter = null)
		{
			return RaycastInternal(world, null, start, direction * range, filter, out results);
		}

		public static bool Raycast(World world, RigidBody body, vec3 start, vec3 end, out RaycastResults results)
		{
			return RaycastInternal(world, body, start, end - start, null, out results);
		}

		public static bool Raycast(World world, RigidBody body, vec3 start, vec3 direction, float range,
			out RaycastResults results)
		{
			return RaycastInternal(world, body, start, direction * range, null, out results);
		}

		private static bool RaycastInternal(World world, RigidBody body, vec3 start, vec3 ray, RaycastFilter filter,
			out RaycastResults results)
		{
			JVector jStart = start.ToJVector();

			// Note that Jitter's Raycast signature below calls one of its parameters "rayDirection", but that vector
			// isn't meant to be normalized (meaning that it's actually just a ray from start to end).
			JVector jDirection = ray.ToJVector();
			JVector normal;
			JVector[] triangle;

			var system = world.CollisionSystem;

			float fraction;

			bool success;

			if (body != null)
			{
				success = system.Raycast(body, jStart, jDirection, out normal, out fraction, out triangle);
			}
			else
			{
				success = system.Raycast(jStart, jDirection,
					(b, t, p, n) => filter?.Invoke(b, t, p.ToVec3(), n.ToVec3()) ?? true, out body, out normal,
					out fraction, out triangle);
			}

			if (!success || fraction > 1)
			{
				results = null;

				return false;
			}

			vec3[] tVectors = null;

			// Triangle will only be set if a triangle mesh was hit.
			if (triangle != null)
			{
				tVectors = new vec3[3];

				for (int i = 0; i < 3; i++)
				{
					tVectors[i] = triangle[i].ToVec3();
				}
			}

			results = new RaycastResults(body, start + jDirection.ToVec3() * fraction,
				Utilities.Normalize(normal.ToVec3()), tVectors);

			return true;
		}
	}
}
