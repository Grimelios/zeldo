﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Engine.Editing;
using Engine.Interfaces;

namespace Engine.Props
{
	public static class Properties
	{
		private static Dictionary<string, string> map = new Dictionary<string, string>();
		private static PropertyAccessor accessor;

		// TODO: Loading properties in this way feels sloppy. Might be better to explicitly call a function (and trigger events as appropriate).
		static Properties()
		{
			foreach (var file in Directory.GetFiles(Paths.Properties, "*", SearchOption.AllDirectories))
			{
				Reload(file);
			}
		}

		// TODO: Consider storing commands, then updating as properties are reloaded (or cleared).
		public static TerminalCommand[] CreateCommands()
		{
			var sortedKeys = map.Keys.ToArray();

			Array.Sort(sortedKeys);

			return new TerminalCommand[]
			{
				new EchoCommand(map, sortedKeys),
				new SetCommand(map, accessor, sortedKeys) 
			};
		}

		private static void Reload(string filename)
		{
			Debug.Assert(File.Exists(filename), $"Missing property file '{filename.StripPath()}'.");

			var lines = File.ReadAllLines(filename);

			for (int i = 0; i < lines.Length; i++)
			{
				string line = lines[i];

				// Comments start with '#'.
				if (line.Length == 0 || line[0] == '#')
				{
					continue;
				}

				Debug.Assert(!line.StartsWith("//"), $"Invalid property ('{filename}', line {i}'): Use # for comments.");
				Debug.Assert(!line.EndsWith(";"), $"Invalid property ('{filename}', line {i}'): Don't end lines with semicolons.");

				// The expected format of each line is "key = value" (although it'll work without the spaces as well).
				string[] tokens = line.Split('=');

				Debug.Assert(tokens.Length == 2, $"Invalid property ('{filename}', line {i}'): Expected format is 'key = value'.");

				string key = tokens[0].TrimEnd();
				string value = tokens[1].TrimStart();

				// This assumes that no two properties will share the same name (across all loaded property files).
				map.Add(key, value);
			}

			accessor = new PropertyAccessor(map);
		}

		public static PropertyAccessor Access(IReloadable target = null)
		{
			if (target != null && !target.Reload(accessor, out var message))
			{
				// By triggering a failure here, bad properties will be detected on object creation (rather than only
				// on reload via the terminal).
				Debug.Fail(message);
			}
			
			return accessor;
		}

		public static void Remove(IReloadable target)
		{
		}
	}
}
