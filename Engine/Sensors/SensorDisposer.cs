﻿using System;

namespace Engine.Sensors
{
	public class SensorDisposer : IDisposable
	{
		private Space space;
		private Sensor sensor;

		public SensorDisposer(Space space, Sensor sensor)
		{
			this.space = space;
			this.sensor = sensor;
		}

		public void Dispose()
		{
			space.Remove(sensor);
		}
	}
}
