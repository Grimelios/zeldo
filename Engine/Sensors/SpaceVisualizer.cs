﻿using Engine.Core;
using Engine.Graphics._3D;
using Engine.Shapes._3D;
using Engine.View;

namespace Engine.Sensors
{
	public class SpaceVisualizer
	{
		private PrimitiveRenderer3D primitives;
		private Space space;

		public SpaceVisualizer(Camera3D camera, Space space)
		{
			this.space = space;

			primitives = new PrimitiveRenderer3D(camera, 10000, 1000);
		}

		public bool IsEnabled { get; set; }

		public void Draw()
		{
			if (!IsEnabled)
			{
				return;
			}

			foreach (var sensor in space.Sensors)
			{
				if (sensor.IsCompound)
				{
					((MultiSensor)sensor).Attachments.ForEach(a => Draw(sensor, a.Shape));
				}
				else
				{
					Draw(sensor, sensor.Shape);
				}
			}

			primitives.Flush();
		}

		private void Draw(Sensor sensor, Shape3D shape)
		{
			if (shape == null)
			{
				return;
			}

			// Disabled sensors are still rendered (assuming they have a valid shape), but in grey.
			var color = sensor.IsEnabled ? Color.Cyan : new Color(50);

			switch (shape.ShapeType)
			{
				case ShapeTypes3D.Box: primitives.Draw((Box)shape, color);
					break;

				case ShapeTypes3D.Capsule: primitives.Draw((Capsule)shape, color);
					break;

				case ShapeTypes3D.Cylinder: primitives.Draw((Cylinder)shape, color);
					break;

				case ShapeTypes3D.Line: primitives.Draw((Line3D)shape, color);
					break;
			}
		}
	}
}
