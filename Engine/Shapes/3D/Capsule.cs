﻿using System;
using System.Diagnostics;
using Engine.Utility;
using GlmSharp;

namespace Engine.Shapes._3D
{
	public class Capsule : Shape3D
	{
		private float radius;
		private float height;

		public Capsule() : this(0, 0)
		{
		}

		public Capsule(float radius, float height, bool isOrientable = true) : base(ShapeTypes3D.Capsule, isOrientable)
		{
			Radius = radius;
			Height = height;
		}

		public float Radius
		{
			get => radius;
			set
			{
				Debug.Assert(value >= 0, "Radius can't be negative.");

				radius = value;
			}
		}

		public float Height
		{
			get => height;
			set
			{
				Debug.Assert(value >= 0, "Height can't be negative.");

				height = value;
			}
		}

		public override bool Contains(vec3 p)
		{
			p -= Position;

			if (IsOrientable)
			{
				p = InverseOrientation * p;
			}

			// Check hemispheres.
			var halfVector = new vec3(0, Height / 2, 0);
			var d1 = Utilities.DistanceSquared(p, Position + halfVector);
			var d2 = Utilities.DistanceSquared(p, Position - halfVector);
			var rSquared = Radius * Radius;

			if (d1 <= rSquared || d2 <= rSquared)
			{
				return true;
			}

			// Check the cylinder.
			if (Math.Abs(p.y - Position.y) > Height / 2)
			{
				return false;
			}

			return Utilities.DistanceSquared(p.swizzle.xz, Position.swizzle.xz) <= rSquared;
		}
	}
}
