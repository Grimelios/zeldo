﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Engine.Core._2D;
using Engine.Utility;

namespace Engine.UI
{
	public class Atlas
	{
		private Dictionary<string, Bounds2D> map;

		public Atlas(string filename)
		{
			var lines = File.ReadAllLines(Paths.Content + filename);

			map = new Dictionary<string, Bounds2D>();

			for (int i = 0; i < lines.Length; i++)
			{
				var line = lines[i];

				// Atlas comments start with # (just like properties).
				if (line.Length == 0 || line[0] == '#')
				{
					continue;
				}

				// The expected format of each atlas line is key = bounds, where bounds is a pipe-separated list of
				// values (X|Y|Width|Height).
				var tokens = line.Split('=');

				Debug.Assert(tokens.Length == 2, $"Incorrect atlas format in file {filename}, line {i + 1} " +
					"(expected key = X|Y|Width|Height).");

				var key = tokens[0].TrimEnd();
				var bounds = Utilities.ParseBounds(tokens[1].TrimStart());

				map.Add(key, bounds);
			}
		}

		public Bounds2D this[string key]
		{
			get
			{
				Debug.Assert(map.ContainsKey(key), $"Missing atlas key '{key}'.");

				return map[key];
			}
		}
	}
}
