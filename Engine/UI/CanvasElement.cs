﻿using System.Collections.Generic;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Input;
using Engine.Interfaces;
using Engine.Interfaces._2D;
using Engine.Messaging;
using Engine.Props;
using Engine.Shapes._2D;
using GlmSharp;

namespace Engine.UI
{
	public abstract class CanvasElement : IPositionContainer2D, IReloadable, IDynamic, IRenderable2D
	{
		protected List<(Component2D Target, ivec2 Location)> attachments;

		protected Rectangle bounds;

		protected CanvasElement()
		{
			attachments = new List<(Component2D target, ivec2 location)>();
			Components = new ComponentCollection();
			bounds = new Rectangle();

			// Elements are visible by default.
			IsVisible = true;
		}

		protected ComponentCollection Components { get; }

		public vec2 Position => IsCentered ? bounds.Position : new vec2(bounds.Left, bounds.Top);

		public bool IsVisible { get; set; }
		public bool IsCentered { get; protected set; }
		public bool IsMarkedForDestruction { get; protected set; }

		public Alignments Anchor { get; set; }

		public ivec2 Offset { get; set; }
		public Rectangle Bounds => bounds;
		public Canvas Canvas { get; set; }

		public int Width
		{
			get => (int)bounds.Width;
			set => bounds.Width = value;
		}

		public int Height
		{
			get => (int)bounds.Height;
			set => bounds.Height = value;
		}

		protected T Attach<T>(T component, ivec2? location = null) where T : Component2D
		{
			var l = location ?? ivec2.Zero;

			attachments.Add((component, l));
			component.Position.SetValue(Position + l, false);

			return component;
		}

		protected void Unattach(int index)
		{
			attachments[index].Target.Dispose();
			attachments.RemoveAt(index);
		}

		// This is useful for repositioning attachments based on property reload.
		protected void Relocate(int attachment, ivec2 location)
		{
			var target = attachments[attachment].Target;
			attachments[attachment] = (target, location);
			target.Position.SetValue(Position + location, false);
		}

		public virtual void SetPosition(vec2 position, bool shouldInterpolate)
		{
			if (IsCentered)
			{
				bounds.Position = position;
			}
			else
			{
				bounds.Left = position.x;
				bounds.Top = position.y;
			}

			attachments.ForEach(a => a.Target.Position.SetValue(position + a.Location, shouldInterpolate));
		}

		public virtual bool Reload(PropertyAccessor accessor, out string message)
		{
			message = null;

			return true;
		}

		// This can be used to initialize components once other values are set.
		public virtual void Initialize()
		{
			Properties.Access(this);
		}

		public virtual void Dispose()
		{
			if (this is IReceiver receiver)
			{
				MessageSystem.Unsubscribe(receiver);
			}

			if (this is IControllable target)
			{
				InputProcessor.Remove(target);
			}
		}

		public virtual void Update()
		{
			Components.Update();
		}

		public virtual void Draw(SpriteBatch sb, float t)
		{
			attachments.ForEach(a => a.Target.Draw(sb, t));
		}
	}
}
