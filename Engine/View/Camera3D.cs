﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Engine.Core;
using Engine.Core._3D;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.Messaging;
using Engine.Props;
using Engine.Utility;
using GlmSharp;

namespace Engine.View
{
	// TODO: Does FOV need to be interpolated? (maybe for some visual effects?)
	public class Camera3D : IReceiver, IReloadable, IRenderPositionable3D, IRenderOrientable, IDynamic
	{
		private RenderPosition3DField positionField;
		private RenderOrientationField orientationField;
		private mat4 projection;
		private List<CameraView3D> controllers;
		private CameraView3D activeController;

		private float orthoHalfWidth;
		private float orthoHalfHeight;
		private float nearPlane;
		private float farPlane;
		private float fov;

		private bool isOrthographic;

		public Camera3D()
		{
			controllers = new List<CameraView3D>();

			var flags = new Flags<TransformFlags>(TransformFlags.None);

			positionField = new RenderPosition3DField(flags);
			orientationField = new RenderOrientationField(flags);

			Properties.Access(this);

			MessageSystem.Subscribe(this, CoreMessageTypes.ResizeRender, data =>
			{
				RecomputeProjection();
			});
		}

		public List<MessageHandle> MessageHandles { get; set; }

		// TODO: Are ortho settings required? (since the game is full 3D now)
		public float OrthoWidth
		{
			get => orthoHalfWidth * 2;
			set
			{
				Debug.Assert(value > 0, "Orthographic width must be positive.");

				orthoHalfWidth = value / 2;
			}
		}

		public float OrthoHeight
		{
			get => orthoHalfHeight * 2;
			set
			{
				Debug.Assert(value > 0, "Orthographic height must be positive.");

				orthoHalfHeight = value / 2;
			}
		}

		public float NearPlane
		{
			get => nearPlane;
			set
			{
				Debug.Assert(value > 0, "Near plane must be positive.");

				nearPlane = value;
			}
		}

		public float FarPlane
		{
			get => farPlane;
			set
			{
				Debug.Assert(value > 0, "Far plane must be positive.");

				farPlane = value;
			}
		}

		// This assumes that FOV will be given in degrees (not radians).
		public float Fov
		{
			get => fov;
			set
			{
				Debug.Assert(value > 0, "Field of view must be positive.");

				fov = Utilities.ToRadians(value);
				RecomputeProjection();
			}
		}

		public bool IsOrthographic
		{
			get => isOrthographic;
			set
			{
				isOrthographic = value;
				RecomputeProjection();
			}
		}

		public RenderPosition3DField Position => positionField;
		public RenderOrientationField Orientation => orientationField;
		public mat4 ViewProjection { get; private set; }
		public mat4 ViewProjectionInverse { get; private set; }

		public CameraView3D ActiveController => activeController;

		private void RecomputeProjection()
		{
			var dimensions = Resolution.RenderDimensions;

			// TODO: Probably add functionality for different kinds of perspectives (like a fish-eye lense) as needed.
			projection = isOrthographic
				? mat4.Ortho(-orthoHalfWidth, orthoHalfWidth, -orthoHalfHeight, orthoHalfHeight, NearPlane, FarPlane)
				: mat4.PerspectiveFov(fov, dimensions.x, dimensions.y, NearPlane, FarPlane);
		}

		public bool Reload(PropertyAccessor accessor, out string message)
		{
			// TODO: Restore properties.
			OrthoWidth = accessor.GetFloat("camera.ortho.width");
			OrthoHeight = accessor.GetFloat("camera.ortho.height");
			NearPlane = 0.1f;//accessor.GetFloat("camera.near.plane", this);
			FarPlane = 100;//accessor.GetFloat("camera.far.plane", this);
			Fov = 90;//accessor.GetFloat("camera.fov", this);

			RecomputeProjection();
			message = null;

			return true;
		}

		public void Recompute(float t)
		{
			var p = positionField.Evaluate(t);
			var q = orientationField.Evaluate(t);
			var view = new mat4(q) * mat4.Translate(-p.x, -p.y, -p.z);

			ViewProjection = projection * view;
			ViewProjectionInverse = ViewProjection.Inverse;
		}

		public vec3 ToScreen(vec3 p)
		{
			// TODO: Is casting to a quaternion needed? Feels like it shouldn't be.
			return ViewProjectionInverse.ToQuaternion * p;
		}

		public T GetController<T>() where T : CameraView3D
		{
			// This requires that a controller with the given type is attached.
			return controllers.OfType<T>().First();
		}

		public T Attach<T>(T view, bool shouldActivate = true) where T : CameraView3D
		{
			Debug.Assert(!controllers.OfType<T>().Any(), "Can't attach multiple camera controllers of the same type.");
			Debug.Assert(!controllers.Contains(view), "Duplicate camera view attached.");

			controllers.Add(view);
			view.Camera = this;

			if (shouldActivate)
			{
				Activate(view);
			}

			return view;
		}

		public void Remove(CameraView3D view, bool shouldDispose = false)
		{
			Debug.Assert(controllers.Contains(view), "Given controller isn't attached to the camera.");
			Debug.Assert(view != activeController, "Can't remove an active controller.");

			// TODO: Does the view need to disposed here? (could maybe pass a flag?)
			controllers.Remove(view);

			if (shouldDispose)
			{
				view.Dispose();
			}
		}

		public T Activate<T>() where T : CameraView3D
		{
			var controller = GetController<T>();

			// This means that a controller with the given type is already active (meaning that nothing else has to be
			// done). Without returning early, you'd call OnActivate twice on the controller (which is wrong).
			if (controller == activeController)
			{
				return controller;
			}

			Activate(controller);

			return controller;
		}

		public T Activate<T>(T controller) where T : CameraView3D
		{
			// It's valid to activate the same controller (even if it's already active). Probably shouldn't happen much
			// in practice, but it's not an error state if it does.
			if (controller == activeController)
			{
				return controller;
			}

			// As a useful shorthand, if the given controller isn't attached, it's attached here instead (before being
			// activated).
			if (controller != null && !controllers.Contains(controller))
			{
				controllers.Add(controller);
				controller.Camera = this;
			}

			// Passing a null controller effectively disables all controllers.
			activeController?.OnDeactivate();
			activeController = controller;
			activeController?.OnActivate();

			return controller;
		}

		// This version of the function can be useful when temporarily taking control away from an existing
		// controller (such as enabling the freecam via terminal, which also has to restore the old controller once
		// disabled again).
		public T Activate<T>(T controller, out CameraView3D oldView) where T : CameraView3D
		{
			oldView = activeController;

			return Activate(controller);
		}

		// TODO: Might want to rename this function? It's probably fine.
		// TODO: Rather than deactivating (and nullifying) the active controller, probably better to temporarily lock using a boolean, then unlock when appropriate.
		// This is useful when the camera needs to temporarily clear all controllers (e.g. Ocarina of Time-style door
		// opening, where the camera freezes in place until Link walks through).
		public void Freeze()
		{
			activeController?.OnDeactivate();
			activeController = null;
		}

		public void Dispose()
		{
			controllers.ForEach(c => c.Dispose());

			MessageSystem.Unsubscribe(this);
		}

		public void Update()
		{
			activeController?.Update();
		}
	}
}
