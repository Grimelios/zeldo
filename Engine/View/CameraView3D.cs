﻿using System;
using Engine.Interfaces;
using Engine.Props;

namespace Engine.View
{
	// TODO: Consider renaming back to CameraController3D.
	public abstract class CameraView3D : IDynamic, IReloadable, IDisposable
	{
		protected CameraView3D()
		{
			Properties.Access(this);
		}

		public Camera3D Camera { get; set;  }

		public virtual void OnActivate()
		{
		}

		public virtual void OnDeactivate()
		{
		}

		public virtual bool Reload(PropertyAccessor accessor, out string message)
		{
			message = null;

			return true;
		}

		public virtual void Dispose()
		{
		}

		public abstract void Update();
	}
}
