﻿using Engine.Editing;
using Engine.Input;
using Engine.Input.Data;

namespace Engine.View
{
	public class FreecamCommand : TerminalCommand, IControllable
	{
		private Camera3D camera;
		private CameraView3D oldController;
		private FreecamView freecam;

		public FreecamCommand(Camera3D camera) : base("freecam")
		{
			this.camera = camera;

			freecam = new FreecamView();
		}

		public override bool Process(string[] args, out string result)
		{
			// The freecam command acts as a toggle (i.e. entering the command again disables the freecam).
			if (camera.ActiveController == freecam)
			{
				camera.Activate(oldController);
				oldController = null;

				InputProcessor.Remove(this);
			}
			else
			{
				camera.Activate(freecam, out oldController);

				// TODO: Account for transfers from controllers other than an orbit view.
				// TODO: Consider transferring sensitivity and max pitch as well.
				var orbit = (OrbitView)oldController;
				freecam.Yaw = orbit.Yaw + Constants.Pi;
				freecam.Pitch = orbit.Pitch;

				InputProcessor.Add(this, 28);
			}

			result = null;

			return true;
		}

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			freecam.ProcessInput(data);

			return InputFlowTypes.BlockingUnpaused;
		}

		public void Dispose()
		{
			freecam.Dispose();

			InputProcessor.Remove(this);
		}
	}
}
