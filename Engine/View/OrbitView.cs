﻿using System.Diagnostics;
using Engine.Input.Data;
using Engine.Utility;
using GlmSharp;

namespace Engine.View
{
	// TODO: Update the camera while processing input directly (rather than waiting until Update).
	// This class was originally designed for orbiting around a central point (most frequently the player). It later
	// became a more general container for views that involve pitch and yaw changes based on user input (including the
	// editor freecam).
	public abstract class OrbitView : CameraView3D
	{
		private float pitch;
		private float yaw;

		protected float maxPitch;
		protected float sensitivity;

		protected bool invertX;
		protected bool invertY;

		public float Pitch
		{
			get => pitch;
			set => pitch = value;
		}

		public float Yaw
		{
			get => yaw;
			set => yaw = value;
		}

		public float Sensitivity
		{
			get => sensitivity;
			set
			{
				Debug.Assert(value > 0, "Orbit sensitivity must be positive.");

				sensitivity = value;
			}
		}

		public float MaxPitch
		{
			get => maxPitch;
			set
			{
				Debug.Assert(value > 0 && value < Constants.PiOverTwo, "Orbit maximum pitch must be between zero " +
					"and π/2 (exclusive).");

				maxPitch = value;
			}
		}

		public virtual void ProcessInput(FullInputData data)
		{
			Debug.Assert(sensitivity > 0, "Orbit sensitivity must be positive (and set before input is processed).");

			// TODO: Process movement from other input devices as well (like controllers, or even keyboard-only).
			if (!data.TryGetData(out MouseData mouse))
			{
				return;
			}

			vec2 delta = mouse.Delta;

			if (invertX)
			{
				delta.x *= -1;
			}

			if (!invertY)
			{
				delta.y *= -1;
			}

			delta *= sensitivity / 10000f;
			pitch += delta.y;
			pitch = Utilities.Clamp(pitch, -maxPitch, maxPitch);
			yaw = Utilities.RestrictAngle(yaw + delta.x);
		}
	}
}
