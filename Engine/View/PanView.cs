﻿using System.Diagnostics;
using Engine.Structures;
using Engine.Timing;
using GlmSharp;

namespace Engine.View
{
	public class PanView : CameraView3D
	{
		private SingleTimer timer;

		public PanView()
		{
			timer = new SingleTimer();
		}

		// If target is set, the camera orients itself to face the target each frame.
		public vec3? Target { private get; set; }

		// Using this view, the camera can pan either 1) between two points (using this function), or 2) along a curved
		// path (using the overloaded version below).
		public void Refresh(vec3 p1, vec3 p2, float duration)
		{
			// TODO: Assign timer trigger function.
			timer.Duration = duration;
			timer.Tick = t =>
			{
				Recompute(vec3.Lerp(p1, p2, t), true);
			};

			// When a pan begins, the camera is immediately snapped to the starting transform (same with using a curved
			// path below).
			Recompute(p1, false);
		}

		public void Refresh(Curve3D path, float duration)
		{
			Debug.Assert(path != null, "Given path was null.");

			// TODO: Assign timer trigger function.
			timer.Duration = duration;
			timer.Tick = t =>
			{
				Recompute(path.Evaluate(t), true);
			};

			Recompute(path.Evaluate(0), false);
		}

		private void Recompute(vec3 p, bool shouldInterpolate)
		{
			Camera.Position.SetValue(p, true);

			if (Target.HasValue)
			{
				Camera.Orientation.SetValue(mat4.LookAt(p, Target.Value, vec3.UnitY).ToQuaternion, shouldInterpolate);
			}
		}

		public override void Update()
		{
			Debug.Assert(timer.Tick != null, "Pan view must be refreshed before activation.");

			timer.Update();
		}
	}
}
