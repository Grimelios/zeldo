﻿using Engine.Entities;
using Engine.Utility;
using GlmSharp;
using Zeldo.Entities.Core;

namespace Zeldo.Cinematics
{
	public enum CameraEventTypes
	{
		Fixed,
		Pan
	}

	public class CameraEvent : CutsceneEvent
	{
		private CameraEventTypes type;

		// TODO: Need a reference to the camera as well.
		public CameraEvent(CameraEventTypes type) : base(CutsceneEventTypes.Camera)
		{
			this.type = type;
		}

		public vec3 P1 { get; }
		public vec3 P2 { get; }

		public override void Process(Scene scene, float t)
		{
			var camera = scene.Camera;

			switch (type)
			{
				case CameraEventTypes.Fixed:
					camera.Position.SetValue(P1, false);
					camera.Orientation.SetValue(Utilities.LookAt(P1, P2), false);

					break;

				case CameraEventTypes.Pan:
					break;
			}
		}
	}
}
