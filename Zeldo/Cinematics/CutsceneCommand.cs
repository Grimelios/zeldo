﻿using System.IO;
using System.Linq;
using Engine;
using Engine.Editing;

namespace Zeldo.Cinematics
{
	public class CutsceneCommand : TerminalCommand
	{
		private string[] options;

		public CutsceneCommand() : base("play")
		{
			options = Directory.GetFiles(Cutscene.Path).Select(p => p.StripPath()).ToArray();
		}

		public override TerminalArgument[] Usage => new []
		{
			new TerminalArgument("cutscene", ArgumentTypes.Required), 
		};

		public override string[] GetOptions(string[] args)
		{
			return options;
		}
	}
}
