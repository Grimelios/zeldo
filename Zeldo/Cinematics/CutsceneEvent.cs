﻿using Engine.Entities;
using Zeldo.Entities.Core;

namespace Zeldo.Cinematics
{
	public abstract class CutsceneEvent
	{
		protected CutsceneEvent(CutsceneEventTypes type)
		{
			Type = type;
		}

		public CutsceneEventTypes Type { get; }

		public float Time { get; }

		// The t value here represents elapsed time since the event's timestamp.
		public abstract void Process(Scene scene, float t);
	}
}
