﻿using System.Collections.Generic;
using Engine;
using Engine.Entities;
using Engine.Interfaces;
using Zeldo.Entities.Core;

namespace Zeldo.Cinematics
{
	public class CutscenePlayer : IDynamic
	{
		private float elapsed;
		private float duration;

		private Scene scene;
		private Queue<CutsceneEvent> events;

		public CutscenePlayer(Scene scene, Queue<CutsceneEvent> events, float duration)
		{
			this.scene = scene;
			this.events = events;
			this.duration = duration;
		}

		public void Update()
		{
			elapsed += Game.DeltaTime;

			CutsceneEvent e;

			while (events.Count > 0 && (e = events.Peek()).Time <= elapsed)
			{
				events.Dequeue();
				e.Process(scene, elapsed - e.Time);
			}

			// TODO: End the cutscene.
			if (elapsed >= duration)
			{
			}
		}
	}
}
