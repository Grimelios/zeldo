﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Engine.Core;
using Engine.Entities;
using Engine.Interfaces;
using Engine.Props;
using Engine.Sensors;
using Engine.Shapes._3D;
using Engine.Timing;
using Zeldo.Entities.Weapons;

namespace Zeldo.Combat
{
	public abstract class Attack : IComponent, IReloadable, IDisposable
	{
		private Weapon weapon;
		private AttackData data;
		private LivingEntity owner;
		private SingleTimer phaseTimer;
		private List<Sensor> sensors;
		private AttackPhases phase;
		
		private bool isStalled;

		protected Attack(AttackData data)
		{
			this.data = data;
			
			phaseTimer = new SingleTimer(t =>
			{
				if (ShouldAdvance(phase))
				{
					AdvancePhase();
				}
				else
				{
					OnStall(phase);
					isStalled = true;
				}
			});

			phaseTimer.Tick = t => WhilePhase(phase, t);
			phase = AttackPhases.Idle;

			Components = new ComponentCollection();
			Components.Add(phaseTimer);

			// All attacks are enabled by default. Some player attacks will likely be locked down (until unlocked).
			// Bosses could also lock certain attacks until a more powerful rematch.
			IsEnabled = true;
		}

		// This can be useful for accessing timing values.
		protected AttackData Data => data;
		protected ComponentCollection Components { get; }

		public Weapon Weapon
		{
			get => weapon;
			set
			{
				Debug.Assert(weapon == null || weapon == value, "Can't change an attack's weapon after being set.");

				weapon = value;
			}
		}

		// Attacks need to store owner as well (for cases when a weapon isn't used). Also note that this implementation
		// intentionally avoids a debug assertion (like the one above for weapons) in order to accommodate the
		// possibility of swapping weapon owners on the fly (e.g. a system similar to Breath of the Widl where enemies
		// can pick up new weapons). At time of writing, this likely won't actually happen in the game, but it's better
		// to account for it anyway.
		public virtual LivingEntity Owner
		{
			get => owner;
			set => owner = value;
		}

		public bool IsEnabled { get; set; }
		public bool IsComplete => phase == AttackPhases.Complete;
		public bool IsCoolingDown => phase == AttackPhases.Cooldown;

		public virtual void Initialize(Scene scene)
		{
			Properties.Access(this);
		}

		protected Sensor CreateMeleeSensor(Scene scene, Shape3D shape = null)
		{
			var meleeWeapon = Weapon as MeleeWeapon;

			Debug.Assert(meleeWeapon != null, "Can't create a melee sensor for a non-melee attack (i.e. an attack " +
				"without a melee weapon referenced).");

			// Sensor creation for attacks is simpler than entities.
			var sensor = meleeWeapon.CreateMeleeSensor(scene, shape);
			sensors = sensors ?? new List<Sensor>();
			sensors.Add(sensor);

			return sensor;
		}

		public void Start()
		{
			Debug.Assert(owner != null, "Attacks must have an owner before triggering.");

			phase = AttackPhases.Idle;

			// All attacks are guaranteed to have at least one phase enabled (the execution phase).
			AdvancePhase();
		}

		private void Reset()
		{
			sensors?.ForEach(s => s.IsEnabled = false);
			phaseTimer.Reset();

			// TODO: Is a more general completion function needed?
			if (Weapon is MeleeWeapon meleeWeapon)
			{
				meleeWeapon.ResetTargets();
			}
		}

		public virtual void Cancel()
		{
			Reset();
			phase = AttackPhases.Idle;
		}

		private void AdvancePhase()
		{
			int phaseIndex;

			do
			{
				phase = (AttackPhases)(((int)phase + 1) % 6);
				phaseIndex = (int)phase - 1;
			}
			// A phase is considered disabled if its duration is zero.
			while (phase != AttackPhases.Complete && data.Durations[phaseIndex] == 0);

			if (phase == AttackPhases.Complete)
			{
				// Phase is left complete here (rather than being set back to idle) so that the attack is properly
				// removed from any component collections (if used in that way).
				Reset();

				return;
			}

			phaseTimer.Duration = data.Durations[phaseIndex];
			phaseTimer.IsPaused = false;

			OnPhase(phase);
		}

		protected virtual void OnPhase(AttackPhases phase)
		{
		}

		protected virtual void OnStall(AttackPhases phase)
		{
		}

		protected virtual void WhilePhase(AttackPhases phase, float t)
		{
		}

		protected virtual void WhileStalled(AttackPhases phase)
		{
		}

		// This is useful when certain phases need to be stalled until a specific event occurs. For example, once the
		// bow is drawn, an attack isn't executed until the button is released.
		protected virtual bool ShouldAdvance(AttackPhases phase)
		{
			return true;
		}

		public virtual bool Reload(PropertyAccessor accessor, out string message)
		{
			message = null;

			return true;
		}

		public virtual void Dispose()
		{
			var scene = Owner.Scene;

			sensors?.ForEach(scene.Space.Remove);
		}

		public virtual void Update()
		{
			Debug.Assert(phase != AttackPhases.Idle, "Attacks shouldn't be updated when idle (this likely means " +
				"that the attack was either never started or canceled without being removed from a component " +
				"collection).");
			Debug.Assert(phase != AttackPhases.Complete, "Attacks shouldn't be updated once complete (this likely " +
				"indicates that a manually-updated attack wasn't nullified correctly).");

			if (isStalled)
			{
				if (ShouldAdvance(phase))
				{
					AdvancePhase();
					isStalled = false;
				}
				else
				{
					WhileStalled(phase);
				}
				
				return;
			}

			if (phase != AttackPhases.Idle)
			{
				Components.Update();
			}
		}
	}
}
