﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Engine.Entities;
using Engine.Utility;
using Zeldo.Entities.Core;
using Zeldo.Entities.Weapons;

namespace Zeldo.Combat
{
	public class AttackCollection
	{
		private Dictionary<int, Attack> attacks;
		private Func<string> selectAttack;
		private Weapon weapon;

		private bool isInitialized;

		// In some cases, attack collections can be created without a weapon (at which point passing in the owner
		// directly is convenient).
		public AttackCollection(string filename, Weapon weapon, LivingEntity owner)
		{
			Debug.Assert(weapon != null || owner != null, "Attack collections must be given either a valid weapon " +
				"or owner on creation (or both).");

			this.weapon = weapon;

			var map = JsonUtilities.Deserialize<Dictionary<string, AttackData>>("Combat/" + filename);

			attacks = new Dictionary<int, Attack>();

			// Attack names are internal, so they're stored using hash codes rather than the raw string.
			foreach (var pair in map)
			{
				var attack = pair.Value.CreateAttack();
				attack.Weapon = weapon;
				attack.Owner = owner;
				attacks.Add(pair.Key.GetHashCode(), attack);
			}
		}
		
		public LivingEntity Owner
		{
			set
			{
				foreach (var attack in attacks.Values)
				{
					attack.Owner = value;
				}
			}
		}

		// This is useful for attack collections not bound to a weapon.
		public Func<string> SelectAttack
		{
			set
			{
				Debug.Assert(weapon == null, "Can't override the attack selection function if a weapon is set (the " +
					"weapon's selection function is used instead).");
				Debug.Assert(value != null, "Attack selection function can't be set to null.");

				selectAttack = value;
			}
		}

		public Attack this[string key] => attacks[key.GetHashCode()];

		public void Initialize(Scene scene)
		{
			Debug.Assert(!isInitialized, "Can't initialize an attack collection more than once.");

			foreach (var pair in attacks)
			{
				pair.Value.Initialize(scene);
			}

			isInitialized = true;
		}

		public T GetAttack<T>() where T : Attack
		{
			var result = attacks.FirstOrDefault(a => a.Value is T).Value;

			Debug.Assert(result != null, $"No attack of type '{typeof(T).FullName}' is present on the collection.");

			return (T)result;
		}

		public Attack Execute()
		{
			string attackName;

			if (weapon != null)
			{
				var function = weapon.SelectAttack;

				Debug.Assert(function != null, "If an attack collection uses a weapon, that weapon must have a " +
					"non-null attack selection function.");

				attackName = weapon.SelectAttack();
			}
			else
			{
				Debug.Assert(selectAttack != null, "Can't select an attack to execute (requires " +
					"either a weapon or non-null selection function).");

				attackName = selectAttack();
			}

			// Returning null is considered valid (it means that no attack can be executed under the current
			// conditions). Also helpful during development when many attacks are missing.
			return attackName == null ? null : this[attackName];
		}
	}
}
