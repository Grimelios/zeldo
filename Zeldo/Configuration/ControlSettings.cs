﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Configuration
{
	public class ControlSettings
	{
		// TODO: Add similar apply functions to other configuration classes.
		public delegate void ApplyHandler(ControlSettings settings);

		public int MouseSensitivity { get; set; } = 50;

		public bool InvertX { get; set; }
		public bool InvertY { get; set; }

		// For accessibility (and preference), some skills (like grab and ascend) can be used as either a toggle or a
		// hold.
		public bool UseToggleAscend { get; set; }
		public bool UseToggleBlock { get; set; }
		public bool UseToggleGrab { get; set; }

		public event ApplyHandler ApplyEvent;

		public void Apply()
		{
			ApplyEvent?.Invoke(this);
		}
	}
}
