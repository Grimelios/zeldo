﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Configuration
{
	public class GameplaySettings
	{
		// TODO: Should this also apply to auto-powering bow shots to always hit their target exactly? (i.e. compute the trajectory in advance)
		public bool UseAimAssist { get; set; }
	}
}
