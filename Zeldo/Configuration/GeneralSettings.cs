﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Configuration
{
	// TODO: Consider adding an option to disable enemy health bars (likely in gameplay options instead?).
	public class GeneralSettings
	{
		public bool ShouldKeepRunningWhileUnfocused { get; }
		public bool ShouldPauseOnUnfocus { get; }
	}
}
