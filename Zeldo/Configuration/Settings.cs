﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Configuration
{
	public class Settings
	{
		public Settings()
		{
			// TODO: Load from configuration files.
			Audio = new AudioSettings();
			Control = new ControlSettings();
			General = new GeneralSettings();
			Video = new VideoSettings();
		}

		public AudioSettings Audio { get; }
		public ControlSettings Control { get; }
		public GeneralSettings General { get; }
		public VideoSettings Video { get; }
	}
}
