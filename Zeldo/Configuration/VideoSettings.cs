﻿
namespace Zeldo.Configuration
{
	public class VideoSettings
	{
		public int ShadowQuality { get; set; }
		public int ColorblindMode { get; set; }

		public bool UseAmbientOcclusion { get; set; }
		public bool UseDepthOfField { get; set; }
		public bool UsePuddleReflections { get; set; }
	}
}
