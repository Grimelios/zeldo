﻿#version 440 core

in vec4 fColor;
in vec3 fNormal;

out vec4 fragColor;

uniform vec3 lightColor;
uniform vec3 lightDirection;
uniform float ambientIntensity;

void main()
{
	float d = dot(-lightDirection, fNormal);
	float diffuse = clamp(d, 0, 1);
	float combined = clamp(ambientIntensity + diffuse, 0, 1);
	
	fragColor = fColor * vec4(lightColor * combined, 1);
}
