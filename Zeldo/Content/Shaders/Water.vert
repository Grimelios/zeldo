﻿#version 440 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec4 vColor;
layout (location = 2) in vec3 vNormal;

out vec4 fColor;
out vec3 fNormal;

uniform mat4 mvp;

void main()
{
	gl_Position = mvp * vec4(vPosition, 1);

	fColor = vColor;
	fNormal = vNormal;
}
