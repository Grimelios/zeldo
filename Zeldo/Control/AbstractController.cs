﻿using Zeldo.Entities.Core;

namespace Zeldo.Control
{
	public abstract class AbstractController
	{
		protected AbstractController(Actor parent)
		{
			Parent = parent;
		}

		protected Actor Parent { get; }

		public virtual void PreStep()
		{
		}

		public virtual void MidStep()
		{
		}

		public virtual void PostStep()
		{
		}
	}
}
