﻿using Engine.Entities;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;

namespace Zeldo.Critters
{
	public class Chipmunk : Critter
	{
		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "Chipmunk.obj");

			base.Initialize(scene, data);
		}
	}
}
