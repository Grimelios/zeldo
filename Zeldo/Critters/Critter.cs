﻿using Engine.Entities;
using Zeldo.Entities.Core;

namespace Zeldo.Critters
{
	public abstract class Critter : Entity
	{
		protected Critter() : base(EntityGroups.Critter)
		{
		}
	}
}
