﻿using Zeldo.Entities.Bosses;

namespace Zeldo.Entities.Bosses
{
	public class Automaton : Boss
	{
		public Automaton() : base("automaton")
		{
		}
	}
}
