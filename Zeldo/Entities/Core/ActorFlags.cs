﻿using System;

namespace Zeldo.Entities.Core
{
	// TODO: Consider adding a flag for wall usage as well.
	[Flags]
	public enum ActorFlags
	{
		None = 0,
		UsesAir = 1<<0,
		UsesGround = 1<<1,
		UsesPlatforms = 1<<2,
		UsesWater = 1<<3
	}
}
