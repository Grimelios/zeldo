﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Entities.Core
{
	public static class EntityGroups
	{
		// TODO: Consider whether entity groups are needed at all (likely used to more efficiently query entities).
		public const int Boss = 0;
		public const int Character = 1;
		public const int Critter = 2;
		public const int Enemy = 3;
		public const int Environment = 4;

		// TODO: Consider removing this group if a separate entity class is created for UI-based elements.
		public const int Interface = 5;
		public const int Item = 6;
		public const int Mechanism = 7;
		public const int Object = 8;
		public const int Player = 9;
		public const int Platform = 10;
		public const int Projectile = 11;
		public const int Structure = 12;
		public const int Weapon = 13;
	}
}
