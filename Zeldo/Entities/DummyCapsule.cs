﻿using Engine.Entities;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Physics;

namespace Zeldo.Entities
{
	public class DummyCapsule : Entity
	{
		private RigidBodyTypes bodyType;

		private bool isAffectedByGravity;

		public DummyCapsule(RigidBodyTypes bodyType, bool isAffectedByGravity) : base(EntityGroups.Object)
		{
			this.bodyType = bodyType;
			this.isAffectedByGravity = isAffectedByGravity;
		}

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "Capsule.obj");
			CreateBody(scene, new CapsuleShape(1, 0.5f), PhysicsGroups.Object, bodyType, isAffectedByGravity ?
				RigidBodyFlags.IsAffectedByGravity : RigidBodyFlags.None);

			base.Initialize(scene, data);
		}
	}
}
