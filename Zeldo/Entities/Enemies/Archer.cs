﻿using Engine.Entities;
using Engine.Props;
using Engine.Shapes._3D;
using Engine.Timing;
using GlmSharp;
using Jitter.Collision.Shapes;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Entities.Objects;
using Zeldo.Physics;

namespace Zeldo.Entities.Enemies
{
	public class Archer : Enemy
	{
		// TODO: Archers can probably use platforms or air as well (if they move around).
		public Archer() : base(EnemyTypes.Archer, ActorFlags.UsesGround)
		{
			// TODO: This is identical to the player. Should probably be abstracted down to the Actor level.
			var accessor = Properties.Access();

			// TODO: These propery names are wrong as well (should be specific to the archer).
			// This is the height of the cylinder (excluding the two rounded caps).
			capsuleHeight = accessor.GetFloat("player.capsule.height");
			capsuleRadius = accessor.GetFloat("player.capsule.radius");
			FullHeight = capsuleHeight + capsuleRadius * 2;

			// TODO: Use a different attack file.
			//Equip(new Bow("PlayerBowAttacks.json", this));
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			if (!accessor.GetFloat("archer.fire.rate", PropertyConstraints.Positive, out var fireRate, out message,
				this))
			{
				return false;
			}

			// TODO: Need to remove the old timer first.
			Components.Add(new RepeatingTimer(t =>
			{
				//Components.Add(Weapon.TriggerPrimary());

				return true;
			}, fireRate, TimerFlags.None));

			return base.Reload(accessor, out message);
		}

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "Capsule.obj");
			CreateMasterBody(scene, new CapsuleShape(capsuleHeight, capsuleRadius), PhysicsGroups.Enemy, true);
			CreateSensor(scene, new Capsule(capsuleHeight, capsuleRadius), SensorGroups.Enemy | SensorGroups.Target);

			base.Initialize(scene, data);
		}

		protected override void OnDeath(vec3 p, vec3 force)
		{
			entityFlags |= EntityFlags.PreserveBody;

			// TODO: Verify that when an enemy dies, it doesn't accidentally update for one additional frame.
			// TODO: Verify that swapping out an actor for a ragdoll doesn't introduce weird physics bugs (i.e. off-by-one frame error).
			Scene.Add(new Ragdoll(this, p, force));
			Scene.Remove(this);
		}

		public override void Update()
		{
			// TODO: This feels a bit wasteful (weapon aim logic should maybe be passed to attacks more directly, probably using OnExecute).
			//Weapon.Aim = Utilities.Normalize(player.Position - position);

			base.Update();
		}
	}
}
