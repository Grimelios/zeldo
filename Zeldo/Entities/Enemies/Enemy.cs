﻿using Engine;
using Engine.Entities;
using Engine.Props;
using GlmSharp;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Entities.Player;
using Zeldo.UI;

namespace Zeldo.Entities.Enemies
{
	public abstract class Enemy : Actor
	{
		// Since this game is built to be singleplayer-only, a single, static reference to the player can be used by
		// all enemies.
		protected static PlayerCharacter player;

		// This function should be called once when the gameplay loop is loaded. Using a static scene event doesn't
		// really work since the player needs to be added first.
		public static void AcquirePlayer(Scene scene)
		{
			player = scene.GetEntities<PlayerCharacter>(EntityGroups.Player)[0];
		}

		// Similar to the function above, this function should be called once when the gameplay loop is unloaded.
		// Leaving the player reference intact (even when the player is unloaded) could cause problems with garbage
		// collection.
		public static void InvalidatePlayer()
		{
			player = null;
		}

		private HealthBar healthBar;

		protected Enemy(EnemyTypes type, ActorFlags flags) : base(EntityGroups.Enemy, flags)
		{
			EnemyType = type;
		}

		public EnemyTypes EnemyType { get; }

		public override void Initialize(Scene scene, JToken data)
		{
			// Every enemy can optionally be spawned with a custom health value.
			if (data.TryParse("Health", out int health))
			{
				Health = health;
			}

			base.Initialize(scene, data);
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			// TODO: If the enemy name contains multiple words, should add periods in between.
			var name = EnemyType.ToString().Uncapitalize();

			if (!accessor.GetInt($"{name}.health", PropertyConstraints.Positive, out var health, out message, this))
			{
				return false;
			}

			// By default, every enemy spawns at maximum health. Reloading also fully refreshes health for all enemies
			// of a particular type.
			MaxHealth = health;
			Health = health;

			return true;
		}

		protected override void OnDeath(vec3 p, vec3 force)
		{
			//Scene.Canvas.Remove(healthBar);
		}
	}
}
