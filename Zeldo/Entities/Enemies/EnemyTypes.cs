﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Entities.Enemies
{
	public enum EnemyTypes
	{
		Archer,
		Groot,
		Kitterket,
		LivingLava,
		Sunflower
	}
}
