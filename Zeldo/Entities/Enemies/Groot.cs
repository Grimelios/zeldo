﻿using Engine.Entities;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;

namespace Zeldo.Entities.Enemies
{
	public class Groot : Enemy
	{
		public Groot() : base(EnemyTypes.Groot, ActorFlags.UsesGround)
		{
		}

		public bool IsPoweredBySunlight { get; set; }

		public override void Initialize(Scene scene, JToken data)
		{
		}

		public override void Update()
		{
		}
	}
}
