﻿using Zeldo.Combat;
using Zeldo.Entities.Core;

namespace Zeldo.Entities.Enemies
{
	public class LivingLava : Enemy
	{
		// TODO: This collection should probably be moved to the base class (to be optionally instantiated for enemies without weapons)
		private AttackCollection attackCollection;

		public LivingLava() : base(EnemyTypes.LivingLava, ActorFlags.UsesGround)
		{
			attackCollection = new AttackCollection("LivingLavaAttacks.json", null, this);
		}
	}
}
