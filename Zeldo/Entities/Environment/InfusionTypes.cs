﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Entities.Environment
{
	// These represents elements that can be infused into weapons (once the upgrade is unlocked).
	public enum InfusionTypes
	{
		Fire,
		Ice,

		// Nature infusion (alternately "growth") extends the reach of melee weapons (with a stretchy tree, vine-like
		// effect).
		Nature,
		Toxic,

		// Wind infusion would probably make the most sense as increased knockback (for enemies and physics objects).
		// Could also potentially be used to gain additional height while jumping (e.g. during an upward swipe, a
		// pillar of air is spawned that pushes you a bit higher than usual).
		Wind
	}
}
