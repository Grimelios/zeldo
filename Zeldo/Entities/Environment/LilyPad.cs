﻿using System.Collections.Generic;
using System.Linq;
using Engine;
using Engine.Core;
using Engine.Entities;
using Engine.Utility;
using GlmSharp;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;

namespace Zeldo.Entities.Environment
{
	public class LilyPad : Entity
	{
		private vec3[] points;
		private ushort[] indices;

		public LilyPad() : base(EntityGroups.Environment)
		{
		}

		public override void Initialize(Scene scene, JToken data)
		{
			const float Radius = 0.75f;
			const int Rings = 7;

			var pointList = new List<vec2>();
			pointList.Add(vec2.Zero);

			// This loop has some duplicated code with the one below, but it's simpler to repeat some logic.
			for (int i = 0; i < 6; i++)
			{
				var angle = Constants.TwoPi / 6 * i;
				var d = Utilities.Direction(angle) * Radius;

				pointList.Add(d);
			}

			// To render the inner hexagon, three triangle strips are used (with two triangles each). Feels maybe a bit
			// wasteful, but this approach is much simpler than trying to render regular triangles + triangle strips.
			var indexList = new List<ushort>();
			indexList.AddRange(new ushort[]
			{
				1, 0, 2, 3, Constants.RestartIndex,
				3, 0, 4, 5, Constants.RestartIndex,
				5, 0, 6, 1, Constants.RestartIndex
			});

			int max = 6;
			int oldRingStart = 1;

			for (int i = 2; i <= Rings; i++)
			{
				var oldMax = max;

				for (int j = 0; j < 6; j++)
				{
					var angle = Constants.TwoPi / 6 * j;
					var r = Radius * i;
					var d = Utilities.Direction(angle) * r;

					if (j > 0)
					{
						var p = pointList.Last();

						indexList.Add((ushort)(oldRingStart + (j - 1) * (i - 1)));

						for (int k = 1; k < i; k++)
						{
							var f = 1f / i * k;
							var interpolated = vec2.Lerp(p, d, f);

							pointList.Add(interpolated);
							indexList.Add((ushort)(max + 1));
							indexList.Add((ushort)(oldRingStart + (j - 1) * (i - 1) + k));
							max++;
						}
					}

					pointList.Add(d);
					indexList.Add((ushort)(max + 1));
					max++;

					if (j > 0)
					{
						indexList.Add(Constants.RestartIndex);
						indexList.Add((ushort)max);
					}
				}

				// Some duplicated code here (and in the code below), but it's difficult to merge into a single
				// code block.
				var p0 = pointList.Last();
				var p1 = pointList[oldMax + 1];

				for (int k = 1; k < i; k++)
				{
					var f = 1f / i * k;
					var interpolated = vec2.Lerp(p0, p1, f);

					pointList.Add(interpolated);
					indexList.Add((ushort)(oldMax + k - i + 1));
					indexList.Add((ushort)(max + 1));
					max++;
				}

				indexList.Add((ushort)oldRingStart);
				indexList.Add((ushort)(oldMax + 1));
				indexList.Add(Constants.RestartIndex);
				oldRingStart = oldMax + 1;
			}

			points = new vec3[pointList.Count];

			for (int i = 0; i < points.Length; i++)
			{
				var p = pointList[i];
				points[i] = position + new vec3(p.x, 0, p.y);
			}

			indices = indexList.ToArray();

			base.Initialize(scene, data);
		}

		public override void Draw()
		{
			for (int i = 0; i < indices.Length;)
			{
				if (indices[i + 2] == ushort.MaxValue)
				{
					i += 3;

					continue;
				}

				var p0 = points[indices[i]];
				var p1 = points[indices[i + 1]];
				var p2 = points[indices[i + 2]];

				var primitives = Scene.Primitives;
				primitives.DrawLine(p0, p1, Color.White);
				primitives.DrawLine(p1, p2, Color.White);
				primitives.DrawLine(p2, p0, Color.White);

				i++;
			}

			foreach (var p in points)
			{
				//Scene.Primitives.Draw(p, Color.Red);
			}
		}
	}
}
