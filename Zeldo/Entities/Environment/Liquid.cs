﻿using System;
using Engine;
using Engine.Core;
using Engine.Entities;
using Engine.Input;
using Engine.Input.Data;
using Engine.Physics;
using Engine.Props;
using Engine.Shaders;
using Engine.Utility;
using GlmSharp;
using Zeldo.Entities.Core;
using static Engine.GL;

namespace Zeldo.Entities.Environment
{
	// TODO: Consider giving liquids a density as well (to work with a potential buoyance system with submersible points).
	public class Liquid : Entity
	{
		private const int Size = 70;
		private const int Resolution = 100;

		private const float Segment = (float)Size / (Resolution - 1);

		// The w coordinate represents vertical speed (like a spring).
		private vec4[,] grid;

		private float[,] advection;
		private float[] kernel;
		private float maxSpeed;
		private float density;

		private Shader shader;

		public Liquid() : base(EntityGroups.Environment)
		{
			// Sentinel nodes are used along the outer rim of each grid.
			grid = new vec4[Resolution, Resolution];
			advection = new float[Resolution, Resolution];

			// TODO: Modify density by liquid type (water, lava, etc.).
			density = 25;

			for (int i = 0; i < Resolution; i++)
			{
				for (int j = 0; j < Resolution; j++)
				{
					grid[j, i] = new vec4(Segment * j - Size / 2f, 0, Segment * i - Size / 2f, 0);
				}
			}

			// These values are based on the proportion of the radius of a circle to the half-size of a square fit
			// inside that circle (which corresponds to how the kernel is applied to the grid).
			kernel = new []
			{
				0.5f,
				0.646f,
				0.5f,
				0.646f,
				0,
				0.646f,
				0.5f,
				0.646f,
				0.5f
			};

			shader = new Shader();
			shader.Attach(ShaderTypes.Vertex, "Primitives3D.vert");
			shader.Attach(ShaderTypes.Fragment, "Primitives.frag");
			shader.AddAttribute<float>(3, GL_FLOAT);
			shader.AddAttribute<byte>(4, GL_UNSIGNED_BYTE, ShaderAttributeFlags.IsNormalized);
			shader.Initialize();

			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW.GLFW_KEY_L, InputStates.PressedThisFrame))
				{
					const int N = 2;
					const int Power = 1000;

					var random = new Random();
					var x = random.Next(N + 1, Resolution - N - 1);
					var y = random.Next(N + 1, Resolution - N - 1);

					for (int i = y - N; i <= y + N; i++)
					{
						for (int j = x - N; j <= x + N; j++)
						{
							var d = Utilities.Distance(grid[j, i].swizzle.xz, grid[x, y].swizzle.xz);
							var f = 1 - d / ((float)Size / (Resolution - 1) * (N + 1));
							var p = grid[j, i];
							p.w = -Power * f * ((float)random.NextDouble() * 0.5f + 0.75f);
							grid[j, i] = p;
						}
					}
				}
			});
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			return accessor.GetFloat("water.max.surface.speed", PropertyConstraints.Positive, out message,
				ref maxSpeed, this);
		}

		public override void SetPosition(vec3 position, bool shouldInterpolate)
		{
			for (int i = 0; i < Resolution; i++)
			{
				for (int j = 0; j < Resolution; j++)
				{
					// TODO: If water moves, interpolation may need to be considered.
					var p = grid[j, i];
					p.y = position.y;
					grid[j, i] = p;
				}
			}

			base.SetPosition(position, shouldInterpolate);
		}

		public bool QueryPoint(SubmersiblePoint point, out vec3 impulse)
		{
			impulse = vec3.Zero;

			var p = point.Position;
			var flat = p.swizzle.xz;
			var dX = flat.x - (position.x - Size / 2f);
			var dZ = flat.y - (position.z - Size / 2f);

			if (dX < 0 || dX > Size || dZ < 0 || dZ > Size)
			{
				return false;
			}

			var x = (int)Math.Floor(dX / Segment);
			var z = (int)Math.Floor(dZ / Segment);
			var remainderX = dX - x * Segment;
			var remainderZ = dZ - z * Segment;

			// Although only three points are used to check for a surface collision (i.e. a triangle), if a surface
			// break *does* occur, the entire square of points is affected (rather than only the triangle). Through
			// testing, deforming only the triangle resulted in ripples that looked unnatural.
			ivec2 grid0 = new ivec2(x, z);
			ivec2 grid1;
			ivec2 grid2;
			ivec2 grid3;

			// TODO: Verify the indexing order (compared to how triangles are rendered).
			if (remainderX > remainderZ)
			{
				grid1 = new ivec2(x + 1, z);
				grid2 = new ivec2(x + 1, z + 1);
				grid3 = new ivec2(x, z + 1);
			}
			else
			{
				grid1 = new ivec2(x + 1, z + 1);
				grid2 = new ivec2(x, z + 1);
				grid3 = new ivec2(x + 1, z);
			}

			var p0 = grid[grid0.x, grid0.y];
			var p1 = grid[grid1.x, grid1.y];
			var p2 = grid[grid2.x, grid2.y];
			var triangle = new []
			{
				p0.swizzle.xyz,
				p1.swizzle.xyz,
				p2.swizzle.xyz
			};
			
			// TODO: Is gravity needed here? (it was previously computed using gravity * delta time)
			var v = (point.Position - point.OldPosition) / Game.DeltaTime;
			var n = Utilities.ComputeNormal(triangle[0], triangle[1], triangle[2], WindingTypes.CounterClockwise,
				false);
			var dot = Utilities.Dot(n, p - triangle[0]);

			// The denser an object (relative to the liquid), the more it deforms the water (and the less the
			// object slows down). This ratio also affects damping while underwater.
			var mass = point.Mass;
			var f = density / (density + mass);

			// TODO: Water can move (individual triangles while waving or the body of water as a whole). Needs to be accounted for as far as surface interactions with objects.
			// TODO: Likely need a way for large objects to affect many surface points at once (might be fine without, though).
			// This means that the point is underwater.
			if (dot < 0)
			{
				// Water deformation is only applied on the first frame the point crosses the surface boundary.
				if (!point.IsUnderwater)
				{
					// The effective collision velocity is computed by averaging the triangle's surface velocity.
					float effectiveY = v.y + (p0.w + p1.w + p2.w) / 3;

					// TODO: Consider weighting deformation based on distance (or distance squared) to surface points.
					// During any liquid impact, a portion of the force goes towards deformation and the rest goes
					// towards slowing down the object. The portion that goes towards deformation is artificially
					// lowered a bit to prevent the point from rapidly ping ponging back and forth (below and above the
					// surface). Note that this is still pretty likely to happen sometimes (especially if multiple
					// objects land in liquid around the same time near each other), but the reduction should help keep
					// things a little more stable.
					var w = effectiveY * (1 - f) * 2.95f;
					var p3 = grid[grid3.x, grid3.y];
					
					// TODO: Consider applying deformation for a few frames (possibly along multiple triangles) to better account for lateral movement (when striking the water).
					p0.w = w;
					p1.w = w;
					p2.w = w;
					p3.w = w;

					grid[grid0.x, grid0.y] = p0;
					grid[grid1.x, grid1.y] = p1;
					grid[grid2.x, grid2.y] = p2;
					grid[grid3.x, grid3.y] = p3;

					// TODO: Consider putting a timer on submersible points so they don't affect the liquid surface several times over a few frames (especially if multiple objects plunge into the water near each other).
					point.IsUnderwater = true;
					impulse = -v * f * 0.25f;
				}
				// If the point is already underwater, its velocity is modified (i.e. an impulse is applied) based on
				// density delta. Further, the liquid's density (which for all intents and purposes functions exactly
				// like viscosity) affects target speed moving both up *and* down. For example, a beach ball
				// interacting with thick lava (hypothetically) would not only slow down quickly upon hitting the
				// surface, but also rise slowly if released within the liquid.
				else
				{
					// Damping is applied while underwater.
					impulse = -v * f * Game.DeltaTime;

					var target = -(mass - density);

					// If the point's mass is greater than the liquid's, it slows down to match that delta and then
					// continues falling. If the densities are exactly the same, the same logic applies, but the object
					// will eventually (in theory) come to a complete stop instead.
					if (mass >= density)
					{
						// If the object is currently moving down (faster than the target speed), gravity is negated in
						// order to effectively slow down the point (based on liquid density). The magnitude of the
						// impulse is further scaled based on closeness to the target speed (such that the rate of
						// deceleration is rapid at first, then slows down and becomes asymptotic).
						if (v.y < target)
						{
							var ratio = (v.y /*+ g*/ - target) / v.y;
							//impulse.y += density * ratio * Game.DeltaTime + g;

							// This helps ensure that (outside of external forces), the object will eventually slow down to
							// exactly the desired speed.
							//if (v.y + impulse.y > target)
							{
								//impulse.y += target - v.y;
							}
						}
						else
						{
							//impulse.y += target * Game.DeltaTime;
						}
					}
					else if (v.y < target)
					{
						// If a non-dense point (i.e. less dense than the liquid) isn't rising fast enough, the point
						// is simply accelerated up until that speed is reached.
						var increment = target * Game.DeltaTime;// + g;
						
						if (v.y + increment <= target)
						{
							impulse.y += increment;
						}
						else
						{
							impulse.y += target - v.y;
						}
					}
					else
					{
						// TODO: Apply resistance while moving up as well.
						//impulse.y += target * Game.DeltaTime;
					}
				}
				
				return true;
			}
			
			if (point.IsUnderwater)
			{
				// TODO: Apply upward speed to the water surface.
				point.IsUnderwater = false;

				// This effectively acts as damping when the object breaks from the liquid's surface (similar to how
				// the object is slowed when it hits the water).
				//impulse.y -= v.y * 0.5f;
				impulse = -v * 0.25f;

				return true;
			}

			return false;
		}

		public override void Update()
		{
			const float Damping = 0.98f;
			const float Limit = 0.15f;
			
			// Compute advection values.
			for (int i = 1; i < Resolution - 1; i++)
			{
				for (int j = 1; j < Resolution - 1; j++)
				{
					var y = grid[j, i].y;

					for (int u = -1; u <= 1; u++)
					{
						for (int v = -1; v <= 1; v++)
						{
							var otherY = grid[j + v, i + u].y;
							var delta = (y - otherY) * kernel[(u + 1) * 3 + v + 1];

							advection[j + v, i + u] += delta;
						}
					}
				}
			}

			// Apply velocities.
			for (int i = 1; i < Resolution - 1; i++)
			{
				for (int j = 1; j < Resolution - 1; j++)
				{
					var p = grid[j, i];
					
					p.w += advection[j, i];

					if (Math.Abs(p.w) < Limit)
					{
						p.y = position.y;
						p.w = 0;
					}
					else
					{
						// Enforcing a maximum speed for surface points prevents water going crazy from collisions
						// with large, fast, or dense objects (which could cause too much deformation if unchecked).
						p.w = Utilities.Clamp(p.w, maxSpeed);
						p.y += p.w * Game.DeltaTime;
						p.w -= p.y - position.y;
						p.w *= Damping;
					}

					grid[j, i] = p;
					advection[j, i] = 0;
				}
			}

			base.Update();
		}

		public override void Draw()
		{
			var primitives = Scene.Primitives;

			for (int i = 1; i < Resolution - 1; i++)
			{
				for (int j = 1; j < Resolution - 1; j++)
				{
					primitives.Draw(grid[j, i].swizzle.xyz, Color.White);
				}
			}

			// Render the water.
			/*
			var data = new vec3[Resolution * Resolution];

			for (int i = 0; i < Resolution; i++)
			{
				for (int j = 0; j < Resolution; j++)
				{
					data[i * Resolution + j] = grid[j, i].swizzle.xyz;
				}
			}

			var primitives = Scene.Primitives;
			primitives.Apply(shader);
			primitives.Fill(data, Resolution, Color.Blue);
			*/
		}
	}
}
