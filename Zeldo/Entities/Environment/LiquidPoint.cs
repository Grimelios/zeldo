﻿using GlmSharp;

namespace Zeldo.Entities.Environment
{
	public class LiquidPoint
	{
		// The Y velocity is used to actually move the point up and down. XZ represents current (used to move objects
		// in something like a river).
		public vec3 Position { get; set; }
		public vec3 Velocity { get; set; }
	}
}
