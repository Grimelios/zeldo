﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Engine;
using Engine.Entities;
using Engine.Interfaces;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;
using Jitter.Dynamics;
using Zeldo.Entities.Core;
using Zeldo.Physics;

namespace Zeldo.Entities.Environment
{
	public class SubmersibleCollection : IDynamic
	{
		// TODO: Once ragdolls exist, might need to update to accommodate multiple bodies (i.e. track body by point rather than by collection).
		private (SubmersiblePoint Point, vec3 Attachment)[] points;
		private RigidBody body;
		private Scene scene;

		// TODO: Support differing masses by point.
		public SubmersibleCollection(Scene scene, vec3[] points, float mass, RigidBody body)
		{
			this.scene = scene;

			Debug.Assert(points != null && points.Length > 0, "Submersible collections must contain at least one " +
				"point.");
			Debug.Assert(body != null, "Submersible collection body must not be null.");
			Debug.Assert(body.BodyType != RigidBodyTypes.Static, "Submersible collections can't be created for " +
				"static bodies.");

			this.body = body;
			this.points = new (SubmersiblePoint Point, vec3 Attachment)[points.Length];

			for (int i = 0; i < points.Length; i++)
			{
				this.points[i] = (new SubmersiblePoint(mass), points[i]);
			}
		}

		public vec3[] Points => points.Select(p => p.Point.Position).ToArray();

		public void Update()
		{
			// TODO: Account for multiple sources of liquid.
			var liquid = scene.GetEntities<Liquid>(EntityGroups.Environment)[0];
			var center = body.Position.ToVec3();
			var q = body.Orientation.ToQuat();
			var impulses = new List<vec3>();
			//var impulsePositions = new List<vec3>();

			foreach (var p in points)
			{
				var point = p.Point;
				var attachment = q * p.Attachment;

				point.Position = center + attachment;

				if (liquid.QueryPoint(point, out var impulse))
				{
					impulses.Add(impulse);
					//impulsePositions.Add(attachment);
					
					//torques.Add(body.InverseInertiaWorld.ToQuat() * Utilities.Cross(p.Attachment, result));
					//body.AddTorque(impulse.ToJVector(), attachment.ToJVector(), ForceTypes.Relative);
				}
				// TODO: Should gravity torque be applied in this block too?
				else
				{
					impulses.Add(new vec3(0, -PhysicsConstants.Gravity * Game.DeltaTime, 0));
					//impulsePositions.Add(attachment);
				}
			}

			// This means that no points are currently within liquid.
			/*
			if (impulses.Count == 0)
			{
				return;
			}

			var averageImpulse = vec3.Zero;
			var impulseLengths = impulses.Select(Utilities.Length).ToArray();
			var sumLength = impulseLengths.Sum();
			var impulsePosition = vec3.Zero;

			for (int i = 0; i < impulses.Count; i++)
			{
				averageImpulse += impulses[i];

				// The impulse position is a weighted average of impulses from each point currently underwater.
				impulsePosition += impulsePositions[i] * (impulseLengths[i] / sumLength);
			}

			averageImpulse /= impulses.Count;
			*/

			var v = vec3.Zero;

			for (int i = 0; i < impulses.Count; i++)
			{
				var i1 = impulses[i];

				if (Utilities.Length(i1) < 0.001f)
				{
					continue;
				}

				v += i1;

				for (int j = i + 1; j < impulses.Count; j++)
				{
					var i2 = impulses[j];

					if (Utilities.Length(i2) > 0.001f)// && Utilities.Dot(i1, i2) > 0)
					{
						impulses[j] -= Utilities.Project(i1, i2);
					}
				}
			}

			//body.LinearVelocity += v.ToJVector();
			//body.LinearVelocity += averageImpulse.ToJVector();
			//body.AddTorque(averageImpulse.ToJVector(), impulsePosition.ToJVector(), ForceTypes.Relative);
			//body.ApplyImpulse(averageImpulse.ToJVector(), impulsePosition.ToJVector(), ForceTypes.Relative, true);
		}
	}
}
