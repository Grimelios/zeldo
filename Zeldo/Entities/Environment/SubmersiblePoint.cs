﻿using System.Diagnostics;
using GlmSharp;

namespace Zeldo.Entities.Environment
{
	// This class acts as a container to have water (or other liquids) properly react when key portions of objects
	// cross the water plane.
	public class SubmersiblePoint
	{
		// Submersible points use verlet physics, meaning that their velocity is implied via old vs. new position
		// (rather than storing velocity directly). This ends up being way simpler (and likely more efficient) when
		// sets of submersible points are attached to entities (especially big ones).
		private vec3 position;
		private vec3 oldPosition;

		private bool isPositionSet;

		public SubmersiblePoint(float mass)
		{
			Debug.Assert(mass > 0, "Submersible point mass must be positive.");

			Mass = mass;
		}

		public vec3 Position
		{
			get => position;
			set
			{
				if (!isPositionSet)
				{
					oldPosition = value;
					isPositionSet = true;
				}
				else
				{
					oldPosition = position;
				}

				position = value;
			}
		}

		public vec3 OldPosition => oldPosition;

		// In real terms, an object's bouyancy is determined by the weight of the fluid displaced (i.e. fluid density
		// times submerged volume). Since liquid interactions are determined by points in this game, the volume is
		// assumed one (for simplicity), making the fluid weight equal to the liquid's density. The point's mass, then,
		// is used to compute point weight (i.e. mass times gravity) in order to compare against buoyancy.
		public float Mass { get; }

		public bool IsUnderwater { get; set; }
	}
}
