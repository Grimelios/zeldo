﻿using Engine.Entities;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Physics;

namespace Zeldo.Entities.Objects
{
	public class ArcheryTarget : Entity
	{
		public ArcheryTarget() : base(EntityGroups.Object)
		{
		}

		public override void Initialize(Scene scene, JToken data)
		{
			var bounds = CreateModel(scene, "ArcheryTarget.obj").Mesh.Bounds;
			var shape = new CylinderShape(bounds.y, bounds.x / 2);

			CreateBody(scene, shape, PhysicsGroups.Object, RigidBodyTypes.PseudoStatic);

			base.Initialize(scene, data);
		}
	}
}
