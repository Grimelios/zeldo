﻿using Engine.Entities;
using Engine.Physics;
using GlmSharp;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;

namespace Zeldo.Entities.Objects
{
	public class Ragdoll : Entity
	{
		public Ragdoll(Actor actor, vec3 p, vec3 force) : base(EntityGroups.Object)
		{
			controllingBody = actor.ControllingBody;
			controllingBody.MakeDynamic();
			controllingBody.PreStep = null;
			controllingBody.MidStep = null;
			controllingBody.PostStep = null;
			controllingBody.ShouldGenerateContact = null;
			controllingBody.Tag = this;
			controllingBody.AddForce(force.ToJVector(), p.ToJVector(), ForceTypes.Absolute);

			// TODO: These are default values taken from the physics engine. Should be polished as necessary (should also use properties).
			// TODO: Verify that existing contacts have their friction values updated correctly.
			var material = controllingBody.Material;
			material.KineticFriction = 0.3f;
			material.StaticFriction = 0.6f;

			// Toggling self-update avoids setting the controlling body's position again (it's already preserved from
			// the parent actor).
			selfUpdate = true;
			SetPosition(actor.Position, false);
			selfUpdate = false;

			actor.TransferEntities(this);
		}

		public override void Initialize(Scene scene, JToken data)
		{
			var p = controllingBody.Position.ToVec3();
			var q = controllingBody.Orientation.ToQuat();

			CreateModel(scene, "Capsule.obj", true, p, q);

			base.Initialize(scene, data);
		}
	}
}
