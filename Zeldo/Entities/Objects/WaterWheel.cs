﻿using System.Collections.Generic;
using Engine;
using Engine.Core._3D;
using Engine.Entities;
using Engine.Props;
using Engine.Utility;
using GlmSharp;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Entities.Environment;
using Zeldo.Physics;

namespace Zeldo.Entities.Objects
{
	public class WaterWheel : Entity
	{
		private Motor motor;

		// TODO: Move this to the base class (likely as a component).
		private SubmersibleCollection collection;

		public WaterWheel() : base(EntityGroups.Object)
		{
		}

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "WaterWheel.obj", out var bounds);

			// TODO: Create fine bodies as well.
			CreateBody(scene, new CylinderShape(bounds.y, bounds.x / 2), PhysicsGroups.EnvironmentCoarse,
				RigidBodyTypes.PseudoStatic);

			motor = Components.Add(new Motor(this));

			var points = new List<vec3>();

			for (int i = 0; i < 12; i++)
			{
				var angle = Constants.TwoPi / 12 * i;
				var d = Utilities.Direction(angle) * bounds.x / 2;

				points.Add(new vec3(d.x, -bounds.y / 2, d.y));
				points.Add(new vec3(d.x, 0, d.y));
				points.Add(new vec3(d.x, bounds.y / 2, d.y));
			}

			collection = new SubmersibleCollection(scene, points.ToArray(), 20, controllingBody);

			base.Initialize(scene, data);
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			if (!accessor.GetFloat("water.wheel.speed", PropertyConstraints.None, out var result, out message,
				this))
			{
				return false;
			}

			motor.AngularVelocity = result;

			return true;
		}

		public override void Update()
		{
			collection.Update();

			base.Update();
		}
	}
}
