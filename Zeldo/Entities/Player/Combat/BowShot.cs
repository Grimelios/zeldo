﻿using Engine;
using Engine.Entities;
using Engine.Props;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Zeldo.Combat;
using Zeldo.Entities.Core;
using Zeldo.Entities.Projectiles;
using Zeldo.Entities.Weapons;
using Zeldo.View;

namespace Zeldo.Entities.Player.Combat
{
	// TODO: Add an option to toggle hold/release of the bow shot.
	// TODO: Most of this logic will have to change for enemy bows.
	// TODO: Consider renaming specifically to PlayerBowShot (especially considering the current namespace).
	public class BowShot : Attack
	{
		private Camera3D camera;
		private PanView panView;
		private FollowView followView;
		private ShoulderView shoulderView;

		// TODO: Consider setting shot speed externally (since enemies wielding bows might want a slower arrow arc).
		private float shotSpeed;
		private float shotCorrection;

		public BowShot(AttackData data) : base(data)
		{
		}

		public override LivingEntity Owner
		{
			get => base.Owner;
			set
			{
				if (Weapon.IsPlayerOwned)
				{
					camera = value.Scene.Camera;
					panView = camera.GetController<PanView>();
					followView = camera.GetController<FollowView>();
					shoulderView = camera.GetController<ShoulderView>();
				}

				base.Owner = value;
			}
		}

		// The bow can be drawn and held prior to release.
		public bool ShouldRelease { get; set; }

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"bow.shot.speed",
				"bow.shot.correction"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			shotSpeed = results["bow.shot.speed"];
			shotCorrection = results["bow.shot.correction"];

			return true;
		}

		protected override bool ShouldAdvance(AttackPhases phase)
		{
			return !Weapon.IsPlayerOwned || phase != AttackPhases.Prepare || ShouldRelease;
		}

		protected override void WhilePhase(AttackPhases phase, float t)
		{
			if (phase == AttackPhases.Prepare)
			{
				OrientOwner();
			}
		}

		protected override void WhileStalled(AttackPhases phase)
		{
			if (phase == AttackPhases.Prepare)
			{
				OrientOwner();
			}
		}

		private void OrientOwner()
		{
			var view = (OrbitView)Weapon.Scene.Camera.ActiveController;

			Owner.SetOrientation(quat.FromAxisAngle(-view.Yaw - Constants.PiOverTwo, vec3.UnitY), false);
		}

		protected override void OnPhase(AttackPhases phase)
		{
			if (phase != AttackPhases.Execute)
			{
				return;
			}

			// TODO: Feels like aim should be given by the bow (rather than the attack object itself querying the camera).
			var view = (OrbitView)Weapon.Scene.Camera.ActiveController;
			var pitch = view.Pitch;
			var max = view.MaxPitch;

			// When firing the bow, the shot angle is artificially raised (to give a more satisfying arc). However, the
			// magnitude of that angle change depends on pitch. Aiming straight up (i.e. maximum pitch) results in no
			// change, while aiming straight down (i.e. negative maximum pitch) results in the full correction being
			// applied. In between, the correction scales exponentially.
			var correction = (pitch + max) / (max * 2);
			correction = (1 - correction * correction) * shotCorrection;

			var aim = vec3.UnitZ * (quat.FromAxisAngle(pitch + correction, vec3.UnitX) *
				quat.FromAxisAngle(view.Yaw, vec3.UnitY));
			var velocity = aim * shotSpeed;

			// TODO: This is wrong. Should be fixed.
			// TODO: Once animations are in place, the arrow will already exist and be visible while aiming. Might not need to set orientation on release.
			var q = Utilities.LookAt(vec3.Zero, velocity);

			// TODO: Need to account for firing an arrow extremely close to a target (to make sure it doesn't tunnel through).
			var arrow = new Arrow();
			arrow.SetPosition(Weapon.Position, false);
			arrow.SetOrientation(q, false);
			arrow.Velocity = velocity;

			Weapon.Scene.Add(arrow);
			ShouldRelease = false;
		}
	}
}
