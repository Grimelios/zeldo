﻿using Engine.Props;
using Zeldo.Combat;

namespace Zeldo.Entities.Player.Combat
{
	public class PlayerAerialSwordLift : Attack
	{
		private float boost;

		public PlayerAerialSwordLift(AttackData data) : base(data)
		{
		}

		// The sword lift, apart from dealing damage, also stops downward movement and gives a small upward boost (i.e.
		// it can be used as a movement tool). As such, the attack behaves similarly to jump vs. double jump (limited
		// usages in the air). There's an upgrade that ups this number from one to two (although that upgrade may be
		// removed later if the double lift feels too powerful).
		public int UsagesRemaining { get; set; }

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			return accessor.GetFloat("player.aerial.sword.lift", PropertyConstraints.Positive, out message, ref boost,
				this);
		}

		// TODO: Consider adding two boost values (small and large), one for lifting while already moving up and the other for cancelling falling speed.
		protected override void OnPhase(AttackPhases phase)
		{
			if (phase != AttackPhases.Execute)
			{
				return;
			}

			// TODO: If double lift is unlocked, play a slightly different animation and sound effect on the second one.
			UsagesRemaining--;

			var body = Weapon.Owner.ControllingBody;
			var v = body.LinearVelocity;

			if (v.Y < boost)
			{
				v.Y = boost;
				body.LinearVelocity = v;
			}
		}
	}
}
