﻿using Engine.Props;
using Zeldo.Combat;

namespace Zeldo.Entities.Player.Combat
{
	// TODO: Attacking and then jumping within a very narrow window should probably transition into a swipe (i.e. a type of buffering).
	public class PlayerAerialSwordSwipe : Attack
	{
		private float boost;
		private float limit;

		public PlayerAerialSwordSwipe(AttackData data) : base(data)
		{
		}

		public float Limit => limit;

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"player.aerial.sword.swipe.boost",
				"player.aerial.sword.swipe.limit"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			boost = results["player.aerial.sword.swipe.boost"];
			limit = results["player.aerial.sword.swipe.limit"];

			return true;
		}

		protected override void OnPhase(AttackPhases phase)
		{
			if (phase != AttackPhases.Execute)
			{
				return;
			}

			// TODO: Might work better by negating gravity for a very short time (i.e. rush upward at a consistent speed).
			// TODO: Verify that the player reaches the same maximum height no matter when the swipe is triggered (within its short activation window).
			var body = Weapon.Owner.ControllingBody;
			var v = body.LinearVelocity;		
			v.Y += boost;
			body.LinearVelocity = v;
		}
	}
}
