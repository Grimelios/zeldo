﻿using System.Collections.Generic;
using Engine;
using Engine.Core;
using Engine.Entities;
using Engine.Props;
using Engine.Sensors;
using Engine.Shapes._3D;
using Engine.Timing;
using Engine.Utility;
using GlmSharp;
using Zeldo.Combat;
using Zeldo.Entities.Core;

namespace Zeldo.Entities.Player.Combat
{
	public class PlayerGroundedSwordSlash : Attack
	{
		private float spread;
		private float range;
		private float spin;

		private SingleTimer slashTimer;

		// The first sensor is a line from the sword's base to the tip. The second sensor is a line from the current
		// tip position to the old one.
		private Sensor sensor1;
		private Sensor sensor2;
		private Line3D line1;
		private Line3D line2;

		private vec3? oldTip;

		public PlayerGroundedSwordSlash(AttackData data) : base(data)
		{
			slashTimer = new SingleTimer();
			slashTimer.Tick = Tick;
			
			Components.Add(slashTimer);
		}

		public override void Initialize(Scene scene)
		{
			line1 = new Line3D();
			line2 = new Line3D();
			sensor1 = CreateMeleeSensor(scene, line1);
			sensor2 = CreateMeleeSensor(scene, line2);

			base.Initialize(scene);
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"player.sword.slash.spread",
				"player.sword.slash.range",
				"player.sword.slash.spin"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			spread = results["player.sword.slash.spread"];
			range = results["player.sword.slash.range"];
			spin = results["player.sword.slash.spin"];

			return true;
		}

		protected override void OnPhase(AttackPhases phase)
		{
			if (phase != AttackPhases.Execute)
			{
				return;
			}

			slashTimer.Duration = Data.ExecutionTime;
			slashTimer.IsPaused = false;

			// TODO: Consider enabling/disabling sensors automatically in the base class.
			// On the first frame of the attack, only the first sensor is active (the one representing the length of
			// the sword itself).
			sensor1.IsEnabled = true;

			Tick(0);
		}

		protected override void WhilePhase(AttackPhases phase, float t)
		{
			if (phase == AttackPhases.Execute)
			{
				sensor2.IsEnabled = true;
			}
		}

		private void Tick(float t)
		{
			var angle = -spread / 2 + spread * t;
			var d = Utilities.Direction(angle);
			var p = Weapon.Position;
			var facing = ((Actor)Owner).Facing;
			var q = quat.FromAxisAngle(-Utilities.Angle(facing), vec3.UnitY) * quat.FromAxisAngle(-spin, vec3.UnitX);
			var tip = p + q * new vec3(d.x, 0, d.y) * range;

			line1.P1 = p;
			line1.P2 = tip;

			if (sensor2.IsEnabled)
			{
				line2.P1 = tip;
				line2.P2 = oldTip.Value;
			}

			oldTip = tip;
		}
	}
}
