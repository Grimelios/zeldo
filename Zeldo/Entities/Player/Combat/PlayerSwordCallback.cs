﻿using Zeldo.Entities.Weapons;

namespace Zeldo.Entities.Player.Combat
{
	public class PlayerSwordCallback
	{
		private PlayerCharacter player;
		private PlayerAerialSwordLift aerialLift;
		private PlayerAerialSwordSwipe aerialSwipe;

		public PlayerSwordCallback(PlayerCharacter player, Sword sword)
		{
			this.player = player;

			var attacks = sword.Attacks;
			aerialLift = attacks.GetAttack<PlayerAerialSwordLift>();
			aerialSwipe = attacks.GetAttack<PlayerAerialSwordSwipe>();
		}

		public string SelectAttack()
		{
			if ((player.State & PlayerStates.Airborne) == 0)
			{
				return "GroundedSlash";
			}

			var v = player.ControllingBody.LinearVelocity;

			// TODO: Does the jump button need to be held to trigger a swipe? Otherwise, the rapidly-decreasing velocity might look weird with the boost (and swipe animation).
			// Upward swipes must be performed very quickly after starting a jump. As such, the limit here represents
			// the minimum vertical speed required to trigger a swipe (rather than a lift).
			if (v.Y >= aerialSwipe.Limit)
			{
				return "AerialSwipe";
			}

			// Aerial lifts can provide an upward boost (if the player's Y velocity is below a certain threshold).
			// However, the lift attack can still trigger if that boost condition isn't true (the boost just won't be
			// applied). This most frequently happens when attacking shortly after a jump, but not fast enough to
			// trigger a swipe.
			return aerialLift.UsagesRemaining > 0 ? "AerialLift" : null;
		}
	}
}
