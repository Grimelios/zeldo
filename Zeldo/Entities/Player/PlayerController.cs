﻿using Engine.Input;
using Engine.Input.Data;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Zeldo.Combat;
using Zeldo.Control;

namespace Zeldo.Entities.Player
{
	public class PlayerController
	{
		// This is useful for diagonal movement using the keyboard (or maybe d-pad too).
		private const float SqrtTwo = 1.41421356237f;

		// When moving using the keyboard, diagonal directions can be normalized by pre-computing this value (avoiding
		// an actual square root call at runtime).
		private PlayerCharacter player;
		private PlayerData playerData;
		private PlayerControls controls;

		private GroundController groundController;
		private AerialController aerialController;
		private PlatformController platformController;
		private LadderController ladderController;
		private WallController wallController;

		// Unlike other combat entities, player attacks are advanced during input processing (rather than the regular
		// update step).
		private Attack activeAttack;

		// It's possible for actions to have multiple binds. In cases where releasing a bind does something (e.g.
		// limiting a player's jump or releasing a hold), that action should only take place if the *same* bind was
		// released (rather than releasing a *different* button bound to the same action). In practice, then, that
		// means that while one bind is held in this scenario, other binds for that same action are ignored.
		private InputBind attackBindUsed;
		private InputBind grabBindUsed;
		private InputBind blockBindUsed;

		public PlayerController(PlayerCharacter player, PlayerData playerData, PlayerControls controls,
			AbstractController[] controllers)
		{
			this.player = player;
			this.playerData = playerData;
			this.controls = controls;

			aerialController = (AerialController)controllers[PlayerCharacter.ControllerIndexes.Air];
			groundController = (GroundController)controllers[PlayerCharacter.ControllerIndexes.Ground];
			platformController = (PlatformController)controllers[PlayerCharacter.ControllerIndexes.Platform];
			wallController = (WallController)controllers[PlayerCharacter.ControllerIndexes.Wall];
			ladderController = (LadderController)controllers[PlayerCharacter.ControllerIndexes.Ladder];
		}

		public vec2 FlatDirection { get; private set; }

		public Attack ActiveAttack => activeAttack;

		// These binds can be set externally (by the player) due to input buffers (triggered when the relevant event
		// occurs).
		public InputBind JumpBindUsed { get; set; }

		// This helps ensure that active attacks are still advanced if input is blocked, but the game remains
		// unpaused (e.g. opening the terminal).
		public bool WasInputProcessedThisFrame { get; set; }

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			WasInputProcessedThisFrame = true;

			if (activeAttack != null)
			{
				activeAttack.Update();

				if (activeAttack.IsComplete)
				{
					activeAttack = null;
				}
			}

			/*
			if (freecam == null)
			{
				freecam = player.Scene.Camera.Add(new FreecamView(), true);
				freecam.Acceleration = 50;
				freecam.Deceleration = 50;
				freecam.MaxSpeed = 10;
				freecam.Sensitivity = 50;
				freecam.MaxPitch = 1.55f;
			}
			*/
			
			// TODO: Probably store relevant views once (rather than retrieving every frame).
			// TODO: Make sure the call order is correct among all of these actions.
			var view = player.Scene.Camera.ActiveController as OrbitView;
			view?.ProcessInput(data);

			FlatDirection = ComputeFlatDirection(data, view);

			// TODO: This is just a quick solution. The actual facing direction will need to be based on animation and movement.
			if (Utilities.LengthSquared(FlatDirection) > 0)
			{
				player.Facing = FlatDirection;
			}
			
			// TODO: Probably remove these (since updates aren't split from physics steps anymore).
			// Flat direction is on multiple controllers regardless of player state (so that if the player changes
			// state mid-step, movement still continues correctly).
			aerialController.FlatDirection = FlatDirection;
			groundController.FlatDirection = FlatDirection;
			platformController.FlatDirection = FlatDirection;
			wallController.FlatDirection = FlatDirection;

			// The player can only interact while grounded. Interaction also takes priority over other actions on the
			// current frame.
			if ((player.State & PlayerStates.OnGround) > 0 && ProcessInteraction(data))
			{
				return InputFlowTypes.BlockingUnpaused;
			}

			ProcessLadder(data);

			// The jump button is used for multiple skills (including regular jumps, wall jump, and ascend). The order
			// in which these functions are called enforces the priority of those skills (ascend first, then wall jump,
			// then regular jumps).
			if (!(ProcessAscend(data) || ProcessWallJump(data)))
			{
				ProcessJump(data, FlatDirection);
			}

			ProcessAttack(data);

			return InputFlowTypes.BlockingUnpaused;
		}

		private vec2 ComputeFlatDirection(FullInputData data, OrbitView orbitView)
		{
			bool forward = data.Query(controls.RunForward, InputStates.Held);
			bool back = data.Query(controls.RunBack, InputStates.Held);
			bool left = data.Query(controls.RunLeft, InputStates.Held);
			bool right = data.Query(controls.RunRight, InputStates.Held);

			// "Flat" direction means the direction the player would run on flat ground. The actual movement direction
			// depends on the current surface.
			vec2 flatDirection = vec2.Zero;

			if (forward ^ back)
			{
				flatDirection.y = forward ? 1 : -1;
			}
			
			if (left ^ right)
			{
				flatDirection.x = left ? 1 : -1;
			}

			// This normalizes the velocity when moving diagonally using a keyboard.
			if ((forward ^ back) && (left ^ right))
			{
				flatDirection *= SqrtTwo;
			}

			// TODO: Account for non-orbit views as well (such as panning between follow and shoulder views).
			if (orbitView != null)
			{
				flatDirection = Utilities.Rotate(flatDirection, orbitView.Yaw);
			}

			return flatDirection;
		}

		private bool ProcessInteraction(FullInputData data)
		{
			return data.Query(controls.Interact, InputStates.PressedThisFrame) && player.TryInteract();
		}

		private void ProcessLadder(FullInputData data)
		{
			// Ladder climbing uses the same controls as running forward and back. Also note that directions remain the
			// same even if the camera is tilted down.
			bool up = data.Query(controls.RunForward, InputStates.Held);
			bool down = data.Query(controls.RunBack, InputStates.Held);

			ladderController.Direction = up ^ down ? (up ? 1 : -1) : 0;
		}

		private bool ProcessAscend(FullInputData data)
		{
			// There are two ascend-based actions the player can take: 1) starting an ascend (by holding the relevant
			// button and pressing jump), or 2) breaking out of an ongoing ascend (by pressing jump mid-ascend).
			if (!player.IsUnlocked(PlayerSkills.Ascend) || !data.Query(controls.Ascend, InputStates.PressedThisFrame))
			{
				return false;
			}

			if ((player.State & PlayerStates.Ascending) == 0)
			{
				return player.TryAscend();
			}

			player.BreakAscend();

			return true;
		}

		/*
		private void ProcessGrab(FullInputData data, float dt)
		{
			// TODO: Player actions (in relation to state) will likely need to be refined. In this case, could other states prevent grabbing?
			if (player.State != PlayerStates.Grabbing)
			{
				if (grabBuffer.Refresh(data, dt, out grabBindUsed))
				{
					player.TryGrab();
				}

				return;
			}

			// Ladders are special among grabbable objects in that a toggle is always used to attach or detach from the
			// ladder (regardless of control settings). I've never played a game where you have to hold a button to
			// remain on a ladder.
			bool shouldRelease = settings.UseToggleGrab || player.IsOnLadder
				? data.Query(controls.Grab, InputStates.ReleasedThisFrame)
				: data.Query(grabBindUsed, InputStates.ReleasedThisFrame);

			if (shouldRelease)
			{
				player.ReleaseGrab();
			}
		}
		*/

		private bool ProcessWallJump(FullInputData data)
		{
			if (player.IsWallJumpAvailable && data.Query(controls.Jump, InputStates.PressedThisFrame, out var bind))
			{
				player.WallJump();
				JumpBindUsed = bind;

				return true;
			}

			return false;
		}

		private void ProcessJump(FullInputData data, vec2 flatDirection)
		{
			// If this is true, it's assumed that the jump bind must have been populated. Note that this behavior (jump
			// limiting) applies to multiple kinds of jumps (including regular jumps, breaking ascends, wall jumps, and
			// maybe more).
			if ((player.State & PlayerStates.Jumping) > 0)
			{
				if (data.Query(JumpBindUsed, InputStates.ReleasedThisFrame) &&
				    player.ControllingBody.LinearVelocity.Y > playerData.JumpLimit)
				{
					player.LimitJump();
					JumpBindUsed = null;
				}

				return;
			}

			// This accounts for jump being unlocked and available.
			if (player.JumpsRemaining == 0)
			{
				return;
			}

			if (data.Query(controls.Jump, InputStates.PressedThisFrame, out var bind))
			{
				player.Jump(flatDirection);
				JumpBindUsed = bind;
			}
		}

		private void ProcessAttack(FullInputData data)
		{
			// TODO: Aim shouldn't be set this way. If the player whips the camera around really fast, could result in firing an arrow backwards or something.
			var weapon = player.Weapon;
			weapon.Aim = player.Scene.Camera.Orientation.ResultValue.Inverse * -vec3.UnitZ;

			if (attackBindUsed != null)
			{
				if (data.Query(attackBindUsed, InputStates.ReleasedThisFrame))
				{
					weapon.ReleasePrimary();
					attackBindUsed = null;
				}

				return;
			}

			// TODO: Process weapon cooldown as needed.
			if (data.Query(controls.Attack, InputStates.PressedThisFrame, out var bind))
			{
				attackBindUsed = bind;
				activeAttack = weapon.TriggerPrimary();
			}
		}

		/*
		private void ProcessBlock(FullInputData data)
		{
			var binds = controls.Block;

			if (settings.UseToggleBlock)
			{
				if (data.Query(binds, InputStates.PressedThisFrame))
				{
					if (player.IsBlocking)
					{
						player.Block();
					}
					else
					{
						player.Unblock();
					}
				}

				return;
			}

			if (!player.IsBlocking && data.Query(binds, InputStates.PressedThisFrame, out blockBindUsed))
			{
				player.Block();
			}
			else if (player.IsBlocking && data.Query(blockBindUsed, InputStates.ReleasedThisFrame))
			{
				player.Unblock();
			}
		}

		private void ProcessParry(FullInputData data)
		{
			// Parry will never be enabled (or unlocked) without the ability to block. A parrying tool (like a shield)
			// must be equipped as well. Once those conditions are satisfied, parrying works similarly to ascend, where
			// an existing bind (in this case, block) executes differently if chorded with the parry bind.
			if (!(player.SkillsEnabled[ParryIndex] && data.Query(controls.Parry, InputStates.Held)))
			{
				return;
			}

			// TODO: This means that the player needs to release block for at least a frame before parrying. Could instead have a dedicated parry button (but at time of writing, I prefer the chorded approach).
			if (data.Query(controls.Block, InputStates.PressedThisFrame))
			{
				player.Parry();
			}
		}
		*/
	}
}
