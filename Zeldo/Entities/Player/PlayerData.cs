﻿using Engine.Interfaces;
using Engine.Props;
using Engine.Utility;

namespace Zeldo.Entities.Player
{
	public class PlayerData : IReloadable
	{
		public PlayerData()
		{
			Properties.Access(this);
		}

		// Jumps
		public float JumpSpeed { get; private set; }
		public float JumpLimit { get; private set; }
		public float JumpDeceleration { get; private set; }
		public float DoubleJumpSpeed { get; private set; }

		// In practice, the double jump limit will likely always be the same as the regular jump limit, but it's more
		// future-proof to keep them separate. Jump deceleration, in constrast, is assumed the same between single and
		// double jumps.
		public float DoubleJumpLimit { get; private set; }

		// TODO: Consider adding both small and big fall damage (with separate thresholds).
		// Fall damage
		public int FallDamage { get; private set; }
		public float FallDamageThreshold { get; private set; }

		// Walls
		public float WallJumpFlatSpeed { get; private set; }
		public float WallJumpYSpeed { get; private set; }
		public float WallJumpMaxAngle { get; private set; }
		public float WallPressThreshold { get; private set; }

		// Platforms
		public float PlatformJumpSpeed { get; private set; }
		public float PlatformJumpThreshold { get; private set; }

		// Dash
		public float DashDistance { get; private set; }
		public float DashDuration { get; private set; }

		// Ascend
		public float AscendAcceleration { get; private set; }
		public float AscendTargetSpeed { get; private set; }

		// Vaults
		public float GroundedVaultThreshold { get; private set; }
		public float AerialVaultThreshold { get; private set; }

		// Other
		public float SlideThreshold { get; private set; }
		public float IdleTime { get; private set; }

		public int KillPlane { get; private set; }

		// TODO: Track properties as reloadable.
		// TODO: Validate fields.
		public bool Reload(PropertyAccessor accessor, out string message)
		{
			// Positive floats
			var keys = new []
			{
				// Jumps
				"player.jump.speed",
				"player.jump.limit",
				"player.jump.deceleration",
				"player.double.jump.speed",
				"player.double.jump.limit",

				// Walls
				"player.wall.jump.speed",
				"player.wall.jump.angle",
				"player.wall.jump.max.angle",
				"player.wall.press.threshold",

				// Dash
				"player.dash.distance",
				"player.dash.duration",

				// Ascend
				"player.ascend.acceleration",
				"player.ascend.target.speed",

				// Vaults
				"player.grounded.vault.threshold",
				"player.aerial.vault.threshold",

				// Other
				"player.slide.threshold",
				"player.idle.time"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var floats, out message, this))
			{
				return false;
			}

			// Jumps
			JumpSpeed = floats["player.jump.speed"];
			JumpLimit = floats["player.jump.limit"];
			JumpDeceleration = floats["player.jump.deceleration"];
			DoubleJumpSpeed = floats["player.double.jump.speed"];
			DoubleJumpLimit = floats["player.double.jump.limit"];

			// Walls
			var wallJumpSpeed = floats["player.wall.jump.speed"];
			var wallJumpAngle = floats["player.wall.jump.angle"];
			var d = Utilities.Direction(wallJumpAngle);

			// Wall jump values are specified as speed + angle, but stored more simply as flat speed and Y speed.
			WallJumpFlatSpeed = d.x * wallJumpSpeed;
			WallJumpYSpeed = d.y * wallJumpSpeed;
			WallJumpMaxAngle = floats["player.wall.jump.max.angle"];
			WallPressThreshold = floats["player.wall.press.threshold"];

			// Dash
			DashDistance = floats["player.dash.distance"];
			DashDuration = floats["player.dash.duration"];

			// Ascend
			AscendAcceleration = floats["player.ascend.acceleration"];
			AscendTargetSpeed = floats["player.ascend.target.speed"];

			// Vaults
			GroundedVaultThreshold = floats["player.grounded.vault.threshold"];
			AerialVaultThreshold = floats["player.aerial.vault.threshold"];

			// Other
			SlideThreshold = floats["player.slide.threshold"];
			IdleTime = floats["player.idle.time"];

			// Non-negative floats
			keys = new []
			{
				"player.fall.damage.threshold",

				// Platforms
				"player.platform.jump.speed",
				"player.platform.jump.threshold"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.NonNegative, out floats, out message, this))
			{
				return false;
			}

			// Platforms
			PlatformJumpSpeed = floats["player.platform.jump.speed"];
			PlatformJumpThreshold = floats["player.platform.jump.threshold"];

			// Fall damage
			if (!accessor.GetInt("player.fall.damage", PropertyConstraints.NonNegative, out var fallDamage,
				out message, this))
			{
				return false;
			}

			FallDamage = fallDamage;
			FallDamageThreshold = floats["player.fall.damage.threshold"];
			KillPlane = accessor.GetInt("kill.plane", PropertyConstraints.None);

			return true;
		}
	}
}
