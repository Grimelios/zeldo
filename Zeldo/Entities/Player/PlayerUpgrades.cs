﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Entities.Player
{
	public enum PlayerUpgrades
	{
		BufferedReflect,
		DoubleAerialLift,
		HomingArrows,
		OmnidirectionalReflect,
		StrongSwim,
		UnderwaterBreathing,
		WaterfallAscend,
		WaterJumping,
		WeaponInfusion
	}
}
