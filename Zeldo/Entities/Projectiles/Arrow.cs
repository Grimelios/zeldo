﻿using Engine;
using Engine.Entities;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Enemies;

namespace Zeldo.Entities.Projectiles
{
	public class Arrow : Projectile
	{
		private vec3 oldTip;

		private bool isEmbedded;
		private bool isFirstRaycast = true;

		private float radius;

		public Arrow() : base(true)
		{
		}

		public override void Initialize(Scene scene, JToken data)
		{
			var model = CreateModel(scene, "Arrow.obj");
			radius = model.Mesh.Bounds.x / 2;

			base.Initialize(scene, data);
		}

		public override void Update()
		{
			if (isEmbedded)
			{
				return;
			}

			base.Update();

			// TODO: Possible edge case if velocity is zero.
			// Orientation is set first (based on current trajectory) to give more accurate raycast endpoints.
			SetOrientation(Utilities.LookAt(vec3.Zero, Velocity) *
				quat.FromAxisAngle(-Constants.PiOverTwo, vec3.UnitY), true);

			Raycast();
		}

		private void Raycast()
		{
			var v = orientation * new vec3(-radius, 0, 0);
			var newTip = position + v;

			// Without this check, the first raycast would likely be way too large.
			if (!isFirstRaycast && PhysicsUtilities.Raycast(Scene.World, oldTip, newTip, out var results))
			{
				var triangle = results.Triangle;

				if (triangle != null)
				{
					// Arrows can only hit front-facing triangles.
					if (Utilities.Dot(Velocity, Utilities.ComputeNormal(triangle, WindingTypes.CounterClockwise,
						false)) <= 0)
					{
						SetPosition(results.Position, false);
					}
				}
				else
				{
					var p = results.Position;
					var entity = (Entity)results.Body.Tag;

					entity.Attach(this, p);

					// TODO: Should be abstracted to damage any non-owner target (since enemies can also wield bows and fire at the player).
					if (entity is Enemy enemy)
					{
						// TODO: Don't hardcode hit values.
						// The arrow is intentionally attached before applying damage (so that the arrow attachment can
						// be transferred to the swapped ragdoll if necessary).
						enemy.OnHit(1, 250, p, Utilities.Normalize(Velocity));
					}
				}

				Embed();

				return;
			}

			oldTip = newTip;
			isFirstRaycast = false;
		}

		private void Embed()
		{
			isEmbedded = true;

			// TODO: Consider loading the extra embedded model on startup (rather than on first embed).
			// While flying through the air, arrows are positioned by their center. Once embedded, arrows are
			// positioned by their tip. To make this render correctly, the simplest approach is to simply load a new
			// model whose vertices are shifted relative to the tip.
			RemoveModel();
			CreateModel(Scene, "ArrowEmbedded.obj");
		}
	}
}
