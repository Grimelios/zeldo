﻿using Engine;
using Engine.Entities;
using Engine.Physics;
using GlmSharp;
using Zeldo.Entities.Core;
using Zeldo.Physics;

namespace Zeldo.Entities.Projectiles
{
	public abstract class Projectile : Entity
	{
		private vec3 velocity;

		private bool isAffectedByGravity;

		protected Projectile(bool isAffectedByGravity) : base(EntityGroups.Projectile)
		{
			this.isAffectedByGravity = isAffectedByGravity;
		}

		// Some fast-moving projectiles don't use physics bodies, so they need to store velocity separately.
		public vec3 Velocity
		{
			get => controllingBody?.LinearVelocity.ToVec3() ?? velocity;
			set
			{
				if (controllingBody != null)
				{
					controllingBody.LinearVelocity = value.ToJVector();
				}
				else
				{
					velocity = value;
				}
			}
		}

		public override void Update()
		{
			if (controllingBody == null)
			{
				if (isAffectedByGravity)
				{
					velocity.y -= PhysicsConstants.Gravity * Game.DeltaTime;
				}

				SetPosition(position + velocity * Game.DeltaTime, true);
			}

			base.Update();
		}
	}
}
