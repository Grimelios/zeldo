﻿using System.Collections.Generic;
using Engine.Core;
using Engine.Entities;
using Engine.Physics;
using GlmSharp;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Entities.Environment;
using Zeldo.Physics;

namespace Zeldo.Entities
{
	public class SubmersibleBox : Entity
	{
		private ivec3 scale;
		private List<vec3> points;
		private SubmersibleCollection collection;

		private float density;
		private bool useSinglePoint;

		public SubmersibleBox(ivec3 scale, float density, bool useSinglePoint) : base(EntityGroups.Object)
		{
			this.scale = scale;
			this.density = density;
			this.useSinglePoint = useSinglePoint;
		}

		public override void Initialize(Scene scene, JToken data)
		{
			/*
			var model = CreateModel(scene, "Poop.obj");
			model.Scale.SetValue(scale, false);

			var increment = Constants.TwoPi / 12;
			var s = model.Mesh.Bounds;

			for (int i = 0; i < 3; i++)
			{
				var y = -s.y / 2f + i * (s.y / 2f);
				var r = s.x * (1 - i / 6f);

				for (int j = 0; j < 12; j++)
				{
					var d = Utilities.Direction(increment * j) * r;

					points.Add(new vec3(d.x, y, d.y));
				}
			}

			for (int i = 0; i < 12; i++)
			{
				var d = Utilities.Direction(increment * i) * scale.x * 0.8f;

				points.Add(new vec3(d.x, -s.y / 1.2f, d.y));
			}

			points.Add(new vec3(0, s.y, 0));
			points.Add(new vec3(0, -s.y / 1.2f, 0));
			*/

			points = new List<vec3>();

			var halfScale = (vec3)scale / 2;
			
			for (int i = 0; i <= scale.x; i++)
			{
				for (int j = 0; j <= scale.z; j++)
				{
					var x = -halfScale.x + i;
					var z = -halfScale.z + j;

					points.Add(new vec3(x, -halfScale.y, z));
					points.Add(new vec3(x, halfScale.y, z));
				}
			}

			for (int i = 1; i < scale.y; i++)
			{
				var y = -halfScale.y + i;

				for (int j = 0; j < scale.x; j++)
				{
					var x = -halfScale.x + j;

					points.Add(new vec3(x, y, -halfScale.z));
					points.Add(new vec3(x, y, halfScale.z));
				}

				for (int j = 0; j < scale.z; j++)
				{
					var z = -halfScale.z + j;

					points.Add(new vec3(-halfScale.x, y, z));
					points.Add(new vec3(halfScale.x, y, z));
				}
			}

			//points.Add(new vec3(-halfScale.x, -halfScale.y, -halfScale.z));
			//points.Add(new vec3(-halfScale.x, -halfScale.y, halfScale.z));
			//points.Add(new vec3(halfScale.x, -halfScale.y, -halfScale.z));
			//points.Add(new vec3(halfScale.x, -halfScale.y, halfScale.z));
			//points.Add(new vec3(0, -halfScale.y, 0));

			if (useSinglePoint)
			{
				points.Clear();
				points.Add(new vec3(0, -halfScale.y, -halfScale.z));
				//points.Add(new vec3(0, -halfScale.y, halfScale.z));
				//points.Add(new vec3(-halfScale.x, -halfScale.y, 0));
			}

			var model = CreateModel(scene, "Cube.obj");
			model.Scale.SetValue(scale, false);

			var body = CreateBody(scene, new BoxShape(scale.ToJVector()), PhysicsGroups.Object, RigidBodyTypes.Dynamic,
				RigidBodyFlags.IsAffectedByGravity);

			collection = new SubmersibleCollection(scene, points.ToArray(), density, body);

			base.Initialize(scene, data);
		}

		public override void Update()
		{
			base.Update();

			if (position.y < -4000)
			{
				//Scene.Remove(this);
			}

			collection.Update();
		}
	}
}
