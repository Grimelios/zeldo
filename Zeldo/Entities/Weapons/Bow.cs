﻿using Engine.Entities;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Player.Combat;

namespace Zeldo.Entities.Weapons
{
	public class Bow : RangedWeapon
	{
		public Bow(string attackFile, LivingEntity owner = null) : base(attackFile, owner)
		{
		}

		public override void Initialize(Scene scene, JToken data)
		{
			// TODO: Should probably load models and such in the base class.
			CreateModel(scene, "Weapons/Bow.obj");

			base.Initialize(scene, data);
		}

		public override void ReleasePrimary()
		{
			// TODO: This will have to change when additional bow attacks are added (since not all of them can be held and released).
			if (IsPlayerOwned)
			{
				((BowShot)ActiveAttack).ShouldRelease = true;
			}
		}
	}
}
