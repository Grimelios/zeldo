﻿using Engine.Entities;

namespace Zeldo.Entities.Weapons
{
	public class HarpoonGun : RangedWeapon
	{
		public HarpoonGun(LivingEntity owner = null) : base("PlayerHarpoonAttacks.json", owner)
		{
		}
	}
}
