﻿using System.Collections.Generic;
using Engine.Entities;
using Engine.Interfaces;
using Engine.Sensors;
using Engine.Shapes._3D;

namespace Zeldo.Entities.Weapons
{
	// TODO: Consider adding a "main" sensor to melee weapons (whose shape is modified per-attack). Might not work cleanly (since some melee attacks use multiple sensors).
	public abstract class MeleeWeapon : Weapon
	{
		// Melee weapons only hit targets once per swing (even if sensors overlap for multiple frames).
		private List<ITargetable> targetsHit;

		protected MeleeWeapon(string attackFile, LivingEntity owner) : base(attackFile, owner)
		{
			targetsHit = new List<ITargetable>();
		}

		// This function is meant to be called by melee attacks (which require sensors to function).
		public Sensor CreateMeleeSensor(Scene scene, Shape3D shape)
		{
			// Sensor creation for melee attacks is simpler than entities.
			var sensor = new Sensor(SensorTypes.Zone, this, (int)SensorGroups.DamageSource, (int)SensorGroups.Target,
				shape);
			sensor.IsEnabled = false;
			sensor.OnSense = (sensorType, owner) =>
			{
				ApplyDamage((ITargetable)owner);
			};

			scene.Space.Add(sensor);

			return sensor;
		}

		private void ApplyDamage(ITargetable target)
		{
			if (targetsHit.Contains(target))
			{
				return;
			}

			// TODO: Don't hardcode damage.
			target.OnDamage(1, this);
			targetsHit.Add(target);
		}

		// TODO: Is a more general function needed to react to attacks ending? Probably not?
		public void ResetTargets()
		{
			targetsHit.Clear();
		}
	}
}
