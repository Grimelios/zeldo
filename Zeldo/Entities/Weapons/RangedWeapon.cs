﻿using Engine.Entities;

namespace Zeldo.Entities.Weapons
{
	public abstract class RangedWeapon : Weapon
	{
		protected RangedWeapon(string attackFile, LivingEntity owner = null) : base(attackFile, owner)
		{
		}
	}
}
