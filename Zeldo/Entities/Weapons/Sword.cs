﻿using Engine.Entities;
using Zeldo.Entities.Core;
using Zeldo.Entities.Player.Combat;

namespace Zeldo.Entities.Weapons
{
	public class Sword : MeleeWeapon
	{
		public Sword(LivingEntity owner = null) : base("PlayerSwordAttacks.json", owner)
		{
			AerialLift = Attacks.GetAttack<PlayerAerialSwordLift>();
		}

		public PlayerAerialSwordLift AerialLift { get; }
	}
}
