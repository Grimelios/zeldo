﻿using Engine.Entities;

namespace Zeldo.Entities.Weapons
{
	public class Twinblade : MeleeWeapon
	{
		public Twinblade(LivingEntity owner = null) : base("PlayerTwinbladeAttacks.json", owner)
		{
		}
	}
}
