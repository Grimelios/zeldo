﻿using System;
using System.Diagnostics;
using Engine.Core;
using Engine.Entities;
using Engine.Utility;
using GlmSharp;
using Newtonsoft.Json.Linq;
using Zeldo.Combat;
using Zeldo.Entities.Core;
using Zeldo.Entities.Player;

namespace Zeldo.Entities.Weapons
{
	// TODO: Consider moving the attack map down here (since all weapons will need attacks attached).
	// TODO: Weapons should probably be disposable (e.g. when an enemy is killed mid-attack).
	public abstract class Weapon : Entity
	{
		// The active attack is stored so that it can be canceled.
		private Attack activeAttack;
		private LivingEntity owner;
		private vec3 aim;
		private Func<string> selectAttack;
		private AttackCollection attacks;

		// TODO: Should weapons be swappable? (would require that owner be changeable)
		protected Weapon(string attackFile, LivingEntity owner = null) : base((int)EntityGroups.Weapon)
		{
			attacks = new AttackCollection(attackFile, this, owner);
			IsDrawEnabled = false;

			if (owner != null)
			{
				Owner = owner;
			}
		}

		protected Attack ActiveAttack => activeAttack;

		// Some weapons (like the bow) behave differently if player-owned (for example, holding back the bowstring
		// zooms in the camera, but only if player-owned).
		public bool IsPlayerOwned { get; private set; }

		// This is useful for retrieving specific attack objects in external classes (primarily to determine trigger
		// logic).
		public AttackCollection Attacks => attacks;

		public LivingEntity Owner
		{
			get => owner;
			set
			{
				Debug.Assert(value != null, "Can't set a null owner on weapons.");

				owner = value;

				// This boolean is intentionally computed first, so that attacks (if needed) can simply check if their
				// parent weapon is player-owned.
				IsPlayerOwned = value is PlayerCharacter;
				attacks.Owner = value;
			}
		}

		public vec3 Aim
		{
			get => aim;
			set
			{
				Debug.Assert(value != vec3.Zero, "Weapon aim must have positive length.");

				aim = value;
			}
		}

		public Func<string> SelectAttack
		{
			get => selectAttack;
			set
			{
				Debug.Assert(value != null, "Weapon's attack selection function can't be null.");

				selectAttack = value;
			}
		}

		// TODO: Revisit how weapon cooldown works (primarily for the player, but might be applicable to other actors as well).
		// If a weapon is on cooldown, input may still be buffered for a short time to trigger the next attack (in the
		// case of the player, anyway).
		public bool IsCoolingDown => activeAttack != null && activeAttack.IsCoolingDown;

		public override void Initialize(Scene scene, JToken data)
		{
			attacks.Initialize(scene);

			base.Initialize(scene, data);
		}

		protected virtual void OnCooldownExpired()
		{
		}

		// This function is used to signal the player controller to trigger another attack immediately if input was
		// buffered.
		/*
		public bool HasCooldownExpired(float dt)
		{
			bool previouslyOnCooldown = IsCoolingDown;
			cooldownFlag.Update(dt);

			return previouslyOnCooldown && !cooldownFlag.Value;
		}
		*/

		// Player attacks use a short input buffering window in order to make chaining a series of attacks a bit easier
		// and more fluid. The specific length of that window is configurable per attack animation, though. To that
		// end, the return value of this function is used directly for input buffering.
		// TODO: Implement buffer time for player attacks (configurable per-attack).
		public Attack TriggerPrimary()
		{
			activeAttack?.Cancel();
			activeAttack = attacks.Execute();
			activeAttack?.Start();

			return activeAttack;
		}

		// This is primarily (maybe only) used by the player (for example, for releasing a taut bowstring).
		public virtual void ReleasePrimary()
		{
		}
	}
}
