﻿using System.Diagnostics;
using Engine;
using Engine.Interfaces;

namespace Zeldo.Entities.Windmill
{
	public class MotorTree : IComponent
	{
		private MotorNode root;

		private float rotation;

		public MotorTree(MotorNode root)
		{
			Debug.Assert(root != null, "Motor tree root can't be null.");

			this.root = root;
		}

		public bool IsComplete => false;

		public float AngularVelocity { get; set; }

		public void Update()
		{
			rotation += AngularVelocity * Game.DeltaTime;

			// This always keeps the net rotation between zero and two pi.
			if (rotation > Constants.TwoPi)
			{
				rotation -= Constants.TwoPi;
			}
			else if (rotation < 0)
			{
				rotation += Constants.TwoPi;
			}

			root.Apply(rotation, Game.DeltaTime);
		}
	}
}
