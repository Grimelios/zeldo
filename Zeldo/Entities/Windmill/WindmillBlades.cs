﻿using System.Collections.Generic;
using Engine;
using Engine.Entities;
using Engine.Physics;
using GlmSharp;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Newtonsoft.Json.Linq;
using Zeldo.Entities.Core;
using Zeldo.Physics;

namespace Zeldo.Entities.Windmill
{
	public class WindmillBlades : Entity
	{
		// TODO: Start the motor tree when the blades are unstuck.
		private MotorTree motorTree;
		private MotorNode root;

		public WindmillBlades() : base(EntityGroups.Structure)
		{
			// TODO: Apply radius if needed.
			root = new MotorNode(this, 0);
			motorTree = Components.Add(new MotorTree(root));
			motorTree.AngularVelocity = 1;
		}

		public override void Initialize(Scene scene, JToken data)
		{
			int blades = data["Blades"].Value<int>();
			
			// This is the inner radius (of the object to which the blades are attached).
			var radius = data["Radius"].Value<float>();
			var mesh = ContentCache.GetMesh(data["Mesh"].Value<string>());

			// TODO: Create a polygon shape for the blade.
			var shape = new BoxShape(mesh.Bounds.ToJVector());
			var offset = new vec3(radius, 0, 0);

			// TODO: Fine vs. coarse bodies might be needed here.
			FanBodies(scene, shape, PhysicsGroups.Environment, blades, vec3.UnitY, offset);
			FanModels(scene, mesh, blades, vec3.UnitY, offset);
			CreateBody(scene, new CylinderShape(0.25f, 2), PhysicsGroups.Environment, RigidBodyTypes.PseudoStatic);

			base.Initialize(scene, data);
		}

		protected override void ResolveHandles(Scene scene, List<EntityHandle> handles)
		{
		}

		public override void Update()
		{
			/*
			SetOrientation(orientation * quat.FromAxisAngle(Game.DeltaTime, vec3.UnitY), true);

			var box = new Box(1, 1, 2);
			box.Position = position;
			box.Orientation = orientation;

			Scene.Primitives.Draw(box, Color.Purple);
			*/

			JMatrix m1 = controllingBody.Orientation;
			JMatrix m2 = JMatrix.CreateFromAxisAngle(JVector.Up, 0.01f);
			JMatrix.Multiply(ref m1, ref m2, out var test);

			quat q1 = m1.ToQuat();
			quat q2 = m2.ToQuat();
			quat q3 = test.ToQuat();

			controllingBody.SetOrientation(test, Game.DeltaTime);

			base.Update();
		}
	}
}
