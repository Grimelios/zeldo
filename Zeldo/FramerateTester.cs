﻿using System.Collections.Generic;
using System.Diagnostics;
using Engine;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;
using Engine.Interfaces._2D;
using Engine.Messaging;
using GlmSharp;
using static Engine.GLFW;

namespace Zeldo
{
	public class FramerateTester : IReceiver, IDynamic, IRenderable2D
	{
		private Sprite sprite1;
		private Sprite sprite2;
		private SpriteText text1;
		private SpriteText text2;
		private SpriteText text3;
		private SpriteText text4;

		private vec2 start1;
		private vec2 start2;

		private bool isEnabled;

		public FramerateTester(Game game)
		{
			var texture = ContentCache.GetTexture("Crash.png");
			var font = ContentCache.GetFont("Debug");

			start1 = new vec2(155, 130);
			start2 = new vec2(155, 260);

			sprite1 = new Sprite(texture);
			sprite1.Position.SetValue(start1, false);
			sprite2 = new Sprite(texture);
			sprite2.Position.SetValue(start2, false);

			text1 = new SpriteText(font, "Update framerate: " + 150); //game.UpdateFramerate);
			text1.Position.SetValue(new vec2(15, 15), false);
			text2 = new SpriteText(font, "Render framerate: " + game.RenderFramerate);
			text2.Position.SetValue(new vec2(15, 34), false);
			text3 = new SpriteText(font, "Interpolated", Alignments.Left);
			text3.Position.SetValue(new vec2(15, start1.y), false);
			text4 = new SpriteText(font, "Fixed", Alignments.Left);
			text4.Position.SetValue(new vec2(15, start2.y), false);
			
			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW_KEY_R, InputStates.PressedThisFrame))
				{
					sprite1.Position.SetValue(start1, false);
					sprite1.Rotation.SetValue(0, false);
					sprite2.Position.SetValue(start2, false);
					sprite2.Rotation.SetValue(0, false);

					isEnabled = false;
				}
				else if (keyboard.Query(GLFW_KEY_S, InputStates.PressedThisFrame))
				{
					isEnabled = true;
				}
			});
		}

		public List<MessageHandle> MessageHandles { get; set; }

		public void Dispose()
		{
			MessageSystem.Unsubscribe(this);
		}

		public void Update()
		{
			if (!isEnabled)
			{
				return;
			}
			
			var x = sprite1.Position.Value.x + Game.DeltaTime * 100;
			var r = sprite1.Rotation.Value + Game.DeltaTime; 

			sprite1.Position.SetX(x, true);
			sprite1.Rotation.SetValue(r, true);
			sprite2.Position.SetX(x, false);
			sprite2.Rotation.SetValue(r, false);
		}

		public void Draw(SpriteBatch sb, float t)
		{
			text1.Draw(sb, t);
			text2.Draw(sb, t);
			text3.Draw(sb, t);
			text4.Draw(sb, t);
			sprite1.Draw(sb, t);
			sprite2.Draw(sb, t);
		}
	}
}
