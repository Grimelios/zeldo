﻿using GlmSharp;
using Zeldo.Entities.Core;

namespace Zeldo.Interfaces
{
	public interface IInteractive
	{
		vec3 Position { get; }

		bool IsInteractionEnabled { get; }
		bool RequiresFacing { get; }

		void OnInteract(Actor actor);
	}
}
