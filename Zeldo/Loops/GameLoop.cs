﻿using System.Collections.Generic;
using Engine.Graphics._2D;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.UI;
using Engine.View;
using Zeldo.Configuration;

namespace Zeldo.Loops
{
	public abstract class GameLoop : IDynamic, IRenderTargetUser3D
	{
		protected Camera3D camera;
		protected Canvas canvas;

		// TODO: Consider moving this to the scene instead.
		protected Settings settings;
		protected SpriteBatch sb;
		protected List<IRenderTargetUser3D> renderTargetUsers3D;

		protected GameLoop(LoopTypes type)
		{
			LoopType = type;
			renderTargetUsers3D = new List<IRenderTargetUser3D>();
		}

		public LoopTypes LoopType { get; }
		
		public Camera3D Camera { set => camera = value; }
		public Canvas Canvas { set => canvas = value; }
		public Settings Settings { set => settings = value; }
		public SpriteBatch SpriteBatch { set => sb = value; }

		public virtual void Dispose()
		{
		}

		public abstract void Initialize();

		public virtual void Update()
		{
		}

		public virtual void Draw(float t)
		{
		}

		public void DrawTargets(float t)
		{
			renderTargetUsers3D.ForEach(r => r.DrawTargets(t));
		}
	}
}
