﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Editing;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;
using Engine.Messaging;
using Engine.Physics;
using Engine.Sensors;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Jitter;
using Jitter.Collision;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Zeldo.Entities;
using Zeldo.Entities.Core;
using Zeldo.Entities.Enemies;
using Zeldo.Entities.Player;
using Zeldo.Entities.Weapons;
using Zeldo.Physics;
using Zeldo.UI;
using Zeldo.UI.Hud;
using Zeldo.View;
using Zeldo.Weather;
using static Engine.GLFW;

namespace Zeldo.Loops
{
	// TODO: When the loop is unloaded, call Enemy.InvalidatePlayer().
	public class GameplayLoop : GameLoop, IReceiver
	{
		private World world;
		private Scene scene;
		private Space space;
		private SpawnCommand spawnHelper;
		private WeatherManager weather;

		// Visualizers.
		private SpaceVisualizer spaceVisualizer;
		private JitterVisualizer jitterVisualizer;

		public GameplayLoop() : base(LoopTypes.Gameplay)
		{
			InputProcessor.Add(data =>
			{
				const int MaxSize = 4;

				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW_KEY_B, InputStates.PressedThisFrame))
				{
					var random = new Random();
					var scaleX = random.Next(MaxSize) + 1;
					var scaleY = random.Next(MaxSize) + 1;
					var scaleZ = random.Next(MaxSize) + 1;
					var angleX = (float)random.NextDouble() * Constants.TwoPi;
					var angleY = (float)random.NextDouble() * Constants.TwoPi;
					var angleZ = (float)random.NextDouble() * Constants.TwoPi;
					var q = quat.FromAxisAngle(angleX, vec3.UnitX) *
						quat.FromAxisAngle(angleY, vec3.UnitY) *
						quat.FromAxisAngle(angleZ, vec3.UnitZ);

					var box = new SubmersibleBox(new ivec3(scaleX, scaleY, scaleZ), 15, false);
					box.SetPosition(new vec3(-13, 16, 2), false);
					box.SetOrientation(q, false);

					scene.Add(box);

					var angularX = (float)random.NextDouble() * Constants.TwoPi;
					var angularY = (float)random.NextDouble() * Constants.TwoPi;
					var angularZ = (float)random.NextDouble() * Constants.TwoPi;

					box.ControllingBody.AngularVelocity = new JVector(angularX, angularY, angularZ);
				}
			});
		}

		public List<MessageHandle> MessageHandles { get; set; }

		public override void Initialize()
		{
			var system = new CollisionSystemSAP();
			system.UseTriangleMeshNormal = true;

			// TODO: Should damping factors be left in their default states? (they were changed while adding kinematic bodies)
			world = new World(system);
			world.Gravity = new JVector(0, -PhysicsConstants.Gravity, 0);
			world.SetDampingFactors(1, 1);
			world.Events.ContactCreated += OnContact;

			weather = new WeatherManager();
			//weather.Spawn<Rain>();

			space = new Space();
			scene = new Scene
			{
				Camera = camera,
				Canvas = canvas,
				Space = space,
				World = world
			};

			var stats = new StatisticsDisplay();
			stats.Anchor = Alignments.Left | Alignments.Top;
			stats.Offset = new ivec2(10);
			stats.IsVisible = false;
			
			var healthBar = new HealthBar(4);
			healthBar.SetPosition(new vec2(Resolution.WindowWidth / 2f, 100), false);

			// TODO: Do excess canvas elements need to be cleaned up from the previous state first?
			canvas.Load("Canvas.json");
			canvas.Add(stats);
			canvas.Add(new Crosshairs());
			//canvas.Add(healthBar);
			//canvas.Add(new RopeTester());

			spawnHelper = new SpawnCommand(scene);

			var controlSettings = settings.Control;

			// TODO: Set player position (and state in general) from a save slot.
			var player = new PlayerCharacter(controlSettings);
			player.Unlock(PlayerSkills.Grab);
			player.Unlock(PlayerSkills.Dash);
			player.Unlock(PlayerSkills.Jump);
			player.Unlock(PlayerSkills.DoubleJump);
			player.Unlock(PlayerSkills.WallJump);
			//player.Unlock(PlayerSkills.Ascend);
			player.Unlock(PlayerSkills.Block);
			player.Unlock(PlayerSkills.Parry);
			player.Unlock(PlayerUpgrades.WaterJumping);

			// TODO: Load fragments from a save slot.
			scene.Add(player);

			// TODO: Should enemies simply query the scene each frame instead?
			Enemy.AcquirePlayer(scene);

			var fragment = scene.LoadFragment("Windmill.json");
			player.SetPosition(fragment.Origin + fragment.Spawn, false);

			CreateDebugCubes();

			// Creating these views also binds them to the player.
			// TODO: Restore views.
			camera.Attach(new PanView());
			camera.Attach(new FollowView(controlSettings, player));
			camera.Attach(new ShoulderView(controlSettings, player));
			camera.Activate<FollowView>();

			canvas.GetElement<Terminal>().Add(new FreecamCommand(camera));

			//player.Equip(new Bow("PlayerBowAttacks.json"));
			player.Equip(new Sword());

			// TODO: Initialize renderer settings from a configuration file (based on user settings).
			// TODO: Set light color and direction based on time of day and weather.
			var renderer = scene.Renderer;
			renderer.Light.Direction = Utilities.Normalize(new vec3(2, -2, -2));

			renderTargetUsers3D.Add(renderer);

			// Create visualizers.
			spaceVisualizer = new SpaceVisualizer(camera, space);
			jitterVisualizer = new JitterVisualizer(camera, world);

			InputProcessor.Add(data =>
			{
				if (data.TryGetData(out KeyboardData keyboard))
				{
					ProcessKeyboard(keyboard);
				}
			});
		}

		public override void Dispose()
		{
			MessageSystem.Unsubscribe(this);
		}

		private void ProcessKeyboard(KeyboardData data)
		{
			bool altHeld = data.Query(InputStates.Held, GLFW_KEY_LEFT_ALT, GLFW_KEY_RIGHT_ALT);

			if (!altHeld)
			{
				return;
			}

			// Toggle physics visualization.
			if (data.Query(GLFW_KEY_J, InputStates.PressedThisFrame))
			{
				jitterVisualizer.IsEnabled = !jitterVisualizer.IsEnabled;
			}

			// Toggle space visualization.
			if (data.Query(GLFW_KEY_S, InputStates.PressedThisFrame))
			{
				spaceVisualizer.IsEnabled = !spaceVisualizer.IsEnabled;
			}

			// Toggle the scene's master renderer (meshes, skeletons, and 3D sprites).
			if (data.Query(GLFW_KEY_M, InputStates.PressedThisFrame))
			{
				var renderer = scene.Renderer;
				renderer.IsEnabled = !renderer.IsEnabled;
			}

			// Toggle UI.
			if (data.Query(GLFW_KEY_U, InputStates.PressedThisFrame))
			{
				canvas.IsVisible = !canvas.IsVisible;
			}
		}

		private void CreateDebugCubes()
		{
			/*
			var cube = new DummyCube(RigidBodyTypes.Dynamic, true, new vec3(3, 1, 3));
			cube.Position = new vec3(5, 8, 5);

			scene.Add(cube);
			*/

			/*
			var random = new Random();

			for (int i = 0; i < 10; i++)
			{
				float x = (float)random.NextDouble() * 10 - 5;
				float y = (float)random.NextDouble() * 5 + 8;
				float z = (float)random.NextDouble() * 10 - 5;

				var cube = new DummyCube(RigidBodyTypes.Dynamic, true, new vec3(3, 0.5f, 3));
				cube.Position = new vec3(x, y, z);

				scene.Add(cube);
			}
			*/
		}

		private bool OnContact(RigidBody body1, RigidBody body2, JVector point1, JVector point2, JVector normal,
			JVector[] triangle, float penetration)
		{
			// By design, all physics objects have entities attached, with the exception of static parts of the map. In
			// the case of map collisions, it's unknown which body comes first as an argument (as of writing this
			// comment, anyway), which is why both entities are checked for null.
			Entity entity1 = body1.Tag as Entity;
			Entity entity2 = body2.Tag as Entity;

			vec3 p1 = point1.ToVec3();
			vec3 p2 = point2.ToVec3();

			// The normal needs to be flipped based on how Jitter handles triangle winding.
			//vec3 n = Utilities.Normalize(-normal.ToVec3());
			vec3 n = Utilities.Normalize(normal.ToVec3());

			// A triangle will only be given in the case of collisions with a triangle mesh (or terrain).
			if (triangle != null)
			{
				var entity = entity1 ?? entity2;
				var point = entity1 != null ? p2 : p1;
				var tArray = triangle.Select(t => t.ToVec3()).ToArray();

				return entity.OnContact(point, n, tArray);
			}

			bool b1 = entity1?.OnContact(entity2, body2, p1, -n) ?? true;
			bool b2 = entity2?.OnContact(entity1, body1, p2, n) ?? true;

			// Either entity can negate the contact.
			return b1 && b2;
		}

		public override void Update()
		{
			world.Step(Game.DeltaTime, false);
			scene.Update();
			space.Update();
			camera.Update();
			weather.Update();
		}
		
		public override void Draw(float t)
		{
			/*
			var renderer = scene.Renderer;
			var list = canvas.GetElement<DebugView>().GetGroup("Scene");
			list.Add("Entities: " + scene.Size);
			list.Add("Bodies: " + scene.World.RigidBodies.Count);
			list.Add("Models: " + renderer.Models.Size);
			list.Add("Skeletons: " + renderer.Skeletons.Size);
			list.Add("Sprites: " + renderer.Sprites.Size);
			*/

			scene.Draw(camera, t);
			weather.Draw(camera, t);
			//RenderRainbow(scene.Primitives);
			jitterVisualizer.Draw(camera);
			spaceVisualizer.Draw();
		}

		private void RenderRainbow(PrimitiveRenderer3D primitives)
		{
			const float Spacing = -2.5f;

			var quad = new []
			{
				new vec3(4, 6, 20),
				new vec3(4, 1, 20),
				new vec3(6, 6, 20),
				new vec3(6, 1, 20)
			};

			var colors = new []
			{
				Color.Red,
				Color.Orange,
				Color.Yellow,
				Color.Green,
				Color.Blue,
				Color.Indigo,
				Color.Violet, 
			};

			foreach (var color in colors)
			{
				primitives.Fill(quad, color);

				for (int i = 0; i < quad.Length; i++)
				{
					quad[i] += new vec3(Spacing, 0, 0);
				}
			}
		}
	}
}
