﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo
{
	public enum Materials
	{
		Cobblestone,
		Dirt,
		Grass,
		Stone,
		Wood
	}
}
