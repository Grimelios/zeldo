﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeldo.Physics
{
	public static class PhysicsGroups
	{
		public const uint Boss = 1<<0;
		public const uint Enemy = 1<<1;

		// For environmental meshes (like the map), some detailed geometry (like fences and railings) is wrapped with
		// bodies from two different groups. "Fine" bodies is more detailed and accurate to the actual geometry
		// (generally used for loose physics objects and ragdolls). In contrast, "coarse" bodies are larger and less
		// accurate (generally used for collisions with the player character and other actors).
		public const uint Environment = EnvironmentFine | EnvironmentCoarse;
		public const uint EnvironmentCoarse = 1<<2;
		public const uint EnvironmentFine = 1<<3;
		public const uint None = 0;
		public const uint Object = 1<<4;
		public const uint Player = 1<<5;
	}
}
