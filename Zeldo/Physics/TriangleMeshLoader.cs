﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Engine;
using Engine.Physics;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.LinearMath;

namespace Zeldo.Physics
{
	public static class TriangleMeshLoader
	{
		public static TriangleMeshShape Load(string filename)
		{
			// This function is very similar to the main mesh loading function, but physics meshes generally contain
			// less data (fewer vertices, no texture coordinates, and maybe no normals). Physics meshes also aren't
			// cached in the same way as regular models (since triangle meshes tend to be loaded in large chunks
			// infrequently as the player moves around the world).
			var mesh = ContentCache.GetMesh(filename, null, false);
			var points = mesh.Points.Select(p => p.ToJVector()).ToList();
			var vertices = mesh.Vertices;
			var indices = mesh.Indices;
			var tris = new List<TriangleVertexIndices>();

			for (int i = 0; i < indices.Length; i += 3)
			{
				// Note that assigning indices in the opposite order (2-1-0 rather than 0-1-2) is required to have
				// Jitter process triangles with the correct winding (without having to flip normals in Blender).
				int i0 = vertices[indices[i + 2]].x;
				int i1 = vertices[indices[i + 1]].x;
				int i2 = vertices[indices[i]].x;

				tris.Add(new TriangleVertexIndices(i0, i1, i2));
			}

			var octree = new Octree(points, tris);
			octree.BuildOctree();

			// TODO: Lists are attached to the triangle mesh for debug rendering. They should be removed later.
			var shape = new TriangleMeshShape(octree);
			shape.Tag = new Tuple<List<JVector>, List<TriangleVertexIndices>>(points, tris);

			return shape;
		}
		
		public static JVector ParseJVector(string line)
		{
			var tokens = line.Split(' ');
			var x = float.Parse(tokens[1]);
			var y = float.Parse(tokens[2]);
			var z = float.Parse(tokens[3]);

			return new JVector(x, y, z);
		}
	}
}
