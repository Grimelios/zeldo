﻿using System;

namespace Zeldo
{
	public static class SensorGroups
	{
		public const int None = 0;
		public const int DamageSource = 1<<1;
		public const int DamageBlock = 1<<2;
		public const int Enemy = 1<<3;
		public const int Hitbox = 1<<4;
		public const int Interaction = 1<<5;
		public const int Player = 1<<6;
		public const int Target = 1<<7;
	}
}
