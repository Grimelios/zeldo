﻿using Engine;
using Engine.Core;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Interfaces;
using Engine.Sensors;
using Engine.Shapes._3D;
using Engine.Utility;
using GlmSharp;
using Zeldo.Entities.Core;
using Zeldo.Entities.Player;

namespace Zeldo
{
	public class ShapeTester : IDynamic
	{
		private Sensor sensor;
		private PlayerCharacter player;
		private PrimitiveRenderer3D primitives;

		public ShapeTester(Scene scene)
		{
			sensor = new Sensor(SensorTypes.Entity, this, 0, 0, new Capsule(0.5f, 1.6f));
			sensor.Position = new vec3(0, 0.65f, 3);
			scene.Space.Add(sensor);
			player = scene.GetEntities<PlayerCharacter>(EntityGroups.Player)[0];
			primitives = scene.Primitives;
		}

		public void Dispose()
		{
		}

		public void Update()
		{
			var shape = sensor.Shape;
			shape.Orientation *= quat.FromAxisAngle(Game.DeltaTime, vec3.UnitZ);

			var p1 = new vec3(0, 2, 0);
			var p2 = player.Position;
			var overlaps = shape.Overlaps(new Line3D(p1, p2));
			var color = overlaps ? Color.Green : Color.White;

			primitives.DrawLine(p1, p2, color);
		}
	}
}
