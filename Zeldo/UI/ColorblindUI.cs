﻿using System;
using System.Collections.Generic;
using Engine;
using Engine.Accessibility;
using Engine.Core;
using Engine.Core._2D;
using Engine.Input.Data;
using Engine.Interfaces;
using Engine.Interpolation;
using Engine.Messaging;
using Engine.UI;
using GlmSharp;
using static Engine.GLFW;

namespace Zeldo.UI
{
	public class ColorblindUI : CanvasElement, IReceiver
	{
		public ColorblindUI()
		{
			var font = ContentCache.GetFont("Debug");
			var values = new []
			{
				"1. Deuteranopia",
				"2. Protanopia",
				"3. Tritanopia",
				"4. Grayscale",
				"5. None"
			};

			var textArray = new SpriteText[values.Length];

			for (int i = 0; i < textArray.Length; i++)
			{
				var text = new SpriteText(font, values[i]);
				textArray[i] = text;
				Attach(text, new ivec2(0, -(textArray.Length - i - 1) * font.Size));
			}

			/*
			MessageSystem.Subscribe(this, CoreMessageTypes.Keyboard, (messageType, data, dt) =>
			{
				var keyboard = (KeyboardData)data;

				if (keyboard.Query(InputStates.PressedThisFrame, out var result, GLFW_KEY_KP_1, GLFW_KEY_KP_2,
					GLFW_KEY_KP_3, GLFW_KEY_KP_4, GLFW_KEY_KP_5))
				{
					var type = (ColorblindTypes)(result - GLFW_KEY_KP_1);
					var text = textArray[(int)type];
					var smoother = new ColorInterpolator(text, Color.Green, Color.White, EaseTypes.QuadraticIn, 0.5f);

					Command.Refresh(type);
					Components.Add(smoother);
				}
			});
			*/
		}

		public ColorblindCommand Command { get; set; }
		public List<MessageHandle> MessageHandles { get; set; }
	}
}
