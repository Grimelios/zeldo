﻿using System.Diagnostics;
using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Props;
using Engine.Shapes._2D;
using Engine.UI;
using GlmSharp;

namespace Zeldo.UI
{
	public class HealthBar : CanvasElement
	{
		private int capWidth;
		private int maxHealth;

		private Color gradient1;
		private Color gradient2;

		public HealthBar(int maxHealth)
		{
			Debug.Assert(maxHealth > 0, "Maximum health (on health bars) must be positive.");

			this.maxHealth = maxHealth;

			var texture = ContentCache.GetTexture("UI.png");
			var atlas = ContentCache.GetAtlas("Atlas.txt");
			var cap = atlas["health.bar.cap"];
			var mid = atlas["health.bar.mid"];
			var arrows = atlas["health.bar.arrows"];

			capWidth = cap.Width;
			bounds = new Rectangle(0, 0, 0, arrows.Height);
			IsCentered = true;

			Attach(new Sprite(texture, cap));
			Attach(new Sprite(texture, mid));
			Attach(new Sprite(texture, cap, Alignments.Center, SpriteModifiers.FlipHorizontal));

			for (int i = 0; i < maxHealth - 1; i++)
			{
				Attach(new Sprite(texture, arrows));
			}
		}

		// Note that maximum health is designed to be immutable (once the health bar is created).
		public int Health
		{
			set
			{
			}
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			// Reload the fill gradient.
			if (!accessor.GetFloat("health.bar.gradient", PropertyConstraints.ZeroToOne, out var multiplier,
				out message, this))
			{
				return false;
			}

			gradient2 = Color.Red;
			gradient1 = gradient2 * multiplier;
			gradient1.A = 255;
			
			// Reload width.
			if (!accessor.GetInt("health.bar.width", PropertyConstraints.Positive, out var width, out message, this))
			{
				return false;
			}

			var innerWidth = width - capWidth;

			bounds.Width = innerWidth;
			attachments[1].Target.Scale.SetValue(new vec2(innerWidth, 1), false);

			Relocate(0, new ivec2(-width / 2, 0));
			Relocate(2, new ivec2(width / 2, 0));

			var spacing = (float)innerWidth / maxHealth;

			for (int i = 1; i < maxHealth; i++)
			{
				Relocate(i + 2, new ivec2((int)(-innerWidth / 2f + spacing * i), 0));
			}

			return true;
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			// TODO: Use a subtle gradient.
			// The canvas bounds are reused to fill the inner area of the health bar.
			sb.FillGradient(bounds, gradient1, gradient2);

			base.Draw(sb, t);
		}
	}
}
