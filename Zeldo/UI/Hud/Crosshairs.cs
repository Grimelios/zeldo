﻿using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.UI;
using GlmSharp;

namespace Zeldo.UI.Hud
{
	public class Crosshairs : CanvasElement
	{
		public Crosshairs()
		{
			Anchor = Alignments.Center;
			IsVisible = true;
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			// TODO: Use properties.
			const int Radius = 10;

			var center = Resolution.WindowDimensions / 2;
			var color = new Color(255, 255, 255, 100);

			sb.DrawLine(center - new ivec2(0, Radius), center + new ivec2(0, Radius), color);
			sb.DrawLine(center - new ivec2(Radius, 0), center + new ivec2(Radius, 0), color);
		}
	}
}
