﻿using Engine;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.UI;
using GlmSharp;
using Zeldo.Items;

namespace Zeldo.UI.Hud
{
	public class ItemShowcaseDisplay : CanvasElement
	{
		private Sprite itemSprite;
		private SpriteText itemText;

		private int textOffset;

		private float fadeTime;
		private float lifetime;

		public ItemShowcaseDisplay()
		{
			const string Prefix = "item.showcase.";

			/*
			itemText = new SpriteText("Debug");
			fadeTime = Properties.GetFloat(Prefix + "fade.time");
			lifetime = Properties.GetFloat(Prefix + "lifetime");
			textOffset = Properties.GetInt(Prefix + "text.offset");
			*/
		}

		public void Refresh(ItemData item)
		{
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			itemSprite.Draw(sb, t);
			itemText.Draw(sb, t);
		}
	}
}
