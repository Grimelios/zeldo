﻿using Engine.Graphics._2D;
using Engine.Input;
using Engine.Input.Data;
using Engine.UI;
using Engine.Utility;
using GlmSharp;

namespace Zeldo.UI.Menus
{
	public abstract class Menu : CanvasElement
	{
		// TODO: Use a menu item class instead.
		private MenuItem[] items;
		private ClickableSet<MenuItem> itemSet;

		private int selectedIndex;

		protected void Initialize(string[] values)
		{
			items = new MenuItem[values.Length];
			itemSet = new ClickableSet<MenuItem>(items);

			for (int i = 0; i < items.Length; i++)
			{
				items[i] = new MenuItem(this, values[i], i, i == 0);
			}
		}

		public override void SetPosition(vec2 position, bool shouldInterpolate)
		{
			for (int i = 0; i < items.Length; i++)
			{
				items[i].SetPosition(position + new vec2(0, 20 * i), shouldInterpolate);
			}

			base.SetPosition(position, shouldInterpolate);
		}

		public void Select(MenuItem item)
		{
			var index = item.Index;

			if (index == selectedIndex)
			{
				return;
			}

			items[selectedIndex].Deselect();
			selectedIndex = item.Index;
			items[selectedIndex].Select();
		}

		public virtual void ProcessInput(FullInputData data, MenuControls controls)
		{
			var clickFlags = ClickFlags.None;

			if (data.TryGetData(out MouseData mouse))
			{
				clickFlags = itemSet.ProcessMouse(mouse);

				// This means that the mouse is hovering over an unselected item. This can happen if the mouse hovered
				// over that item (and stopped moving), then a different input method (like the keyboard) was used to
				// cycle selection.
				if ((clickFlags & ClickFlags.WasHovered) > 0)
				{
					var hovered = itemSet.HoveredItem;

					if (selectedIndex != hovered.Index)
					{
						Select(hovered);
					}
				}
			}

			// Mouse clicks take priority over submission via other devices (like pressing enter on a keyboard).
			if ((clickFlags & ClickFlags.WasClicked) > 0)
			{
				// If submit occurs (from the mouse or otherwise), all other menu-related input on this frame is
				// ignored.
				return;
			}

			if (data.Query(controls.Submit, InputStates.PressedThisFrame))
			{
				Submit(selectedIndex);

				return;
			}

			// For selection, mouse movement takes priority over the keyboard.
			if ((clickFlags & ClickFlags.WasHovered) > 0)
			{
				return;
			}

			bool up = data.Query(controls.Up, InputStates.PressedThisFrame);
			bool down = data.Query(controls.Down, InputStates.PressedThisFrame);

			if (up ^ down)
			{
				items[selectedIndex].Deselect();

				if (up)
				{
					selectedIndex = selectedIndex == 0 ? items.Length - 1 : --selectedIndex;
				}
				else
				{
					selectedIndex = ++selectedIndex % items.Length;
				}

				items[selectedIndex].Select();
			}
		}

		public abstract void Submit(int index);

		public override void Update()
		{
			foreach (var item in items)
			{
				item.Update();
			}

			base.Update();
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			foreach (var item in items)
			{
				item.Draw(sb, t);
			}

			base.Draw(sb, t);
		}
	}
}
