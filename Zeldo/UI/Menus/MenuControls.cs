﻿using System.Collections.Generic;
using Engine.Input.Data;
using static Engine.GLFW;

namespace Zeldo.UI.Menus
{
	public class MenuControls
	{
		// TODO: Load from a file.
		public MenuControls()
		{
			Up = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_W),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_UP)
			};

			Down = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_S),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_DOWN)
			};

			Left = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_A),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_LEFT)
			};

			Right = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_D),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_RIGHT)
			};

			Submit = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_ENTER),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_KP_ENTER)
			};

			Back = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_BACKSPACE)
			};

			Pause = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_ESCAPE),
				new InputBind(InputTypes.Keyboard, GLFW_KEY_P)
			};
		}

		public List<InputBind> Up { get; }
		public List<InputBind> Down { get; }
		public List<InputBind> Left { get; }
		public List<InputBind> Right { get; }
		public List<InputBind> Submit { get; }
		public List<InputBind> Back { get; }
		public List<InputBind> Pause { get; }
	}
}
