﻿using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Interfaces;
using Engine.Interfaces._2D;
using Engine.Shapes._2D;
using GlmSharp;

namespace Zeldo.UI.Menus
{
	public class MenuItem : IClickable, IColorContainer, IDynamic, IRenderable2D
	{
		// TODO: Use properties instead.
		private static readonly Color UnselectedColor = new Color(120);
		private static readonly Color DisabledColor = new Color(50);

		private Menu parent;
		private SpriteText spriteText;
		private Rectangle bounds;

		private bool isEnabled;

		public MenuItem(Menu parent, string value, int index, bool isSelected)
		{
			this.parent = parent;

			Index = index;

			var font = ContentCache.GetFont("Debug");
			var size = font.MeasureLiteral(value, out var offset);

			bounds = new Rectangle(size.x, size.y).Expand(3);
			isEnabled = true;

			spriteText = new SpriteText("Debug", value, Alignments.Center);
			spriteText.Color.SetValue(isSelected ? Color.White : UnselectedColor, false);
		}

		public int Index { get; }

		public vec2 Position => spriteText.Position.Value;
		public Color Color => spriteText.Color.Value;

		public bool IsEnabled
		{
			get => isEnabled;
			set
			{
				// Enabling/disabling changes color, so it's simpler to return early if the enabled flag didn't
				// actually change.
				if (isEnabled != value)
				{
					isEnabled = value;

					// This assumes that a menu item can't be enabled and selected in a single atomic action (the item
					// should be enabled, *then* selected instead).
					SetColor(value ? UnselectedColor : DisabledColor, false);
				}
			}
		}

		public void SetPosition(vec2 position, bool shouldInterpolate)
		{
			spriteText.Position.SetValue(position, shouldInterpolate);

			// If interpolated, 
			if (!shouldInterpolate)
			{
				bounds.Position = position;
			}
		}

		public void SetColor(Color color, bool shouldInterpolate)
		{
			spriteText.Color.SetValue(color, shouldInterpolate);
		}

		public void Select()
		{
			SetColor(Color.White, false);
		}

		public void Deselect()
		{
			SetColor(UnselectedColor, false);
		}

		public void OnHover(ivec2 mouseLocation)
		{
			parent.Select(this);
		}

		public void OnUnhover()
		{
		}

		public void OnClick(ivec2 mouseLocation)
		{
			parent.Select(this);
			parent.Submit(Index);
		}

		public bool Contains(ivec2 mouseLocation)
		{
			return bounds.Contains(mouseLocation);
		}

		public void Update()
		{
		}

		public void Draw(SpriteBatch sb, float t)
		{
			spriteText.Draw(sb, t);

			// This is technically slightly wasteful if not interpolated (since position is already set in
			// SetPosition), but this is a negligible problem (plus, in practice, menu items will probably always be
			// interpolated once spawned).
			bounds.Position = spriteText.Position.ResultValue;
		}
	}
}
