﻿using Engine;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;
using GlmSharp;

namespace Zeldo.UI.Menus
{
	// TODO: Load and unload menus as appropriate (e.g. unload the title menu when gameplay begins).
	public class MenuManager : IControllable, IDynamic
	{
		// TODO: Abstract to manage all menus.
		private PauseMenu pauseMenu;
		private MenuControls controls;

		public MenuManager(MainGame game)
		{
			controls = new MenuControls();
			pauseMenu = new PauseMenu(game);
			pauseMenu.SetPosition(new vec2(Resolution.WindowWidth / 2f, 80), false);
		
			InputProcessor.Add(this, 20);
		}

		// TODO: This is probably temporary (to add the pause menu to the main game's list of pause elements).
		public PauseMenu PauseMenu => pauseMenu;

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			pauseMenu.ProcessInput(data, controls);

			return pauseMenu.IsVisible ? InputFlowTypes.BlockingPaused : InputFlowTypes.Passthrough;
		}

		public void Dispose()
		{
			InputProcessor.Remove(this);
		}

		public void Update()
		{
		}
	}
}
