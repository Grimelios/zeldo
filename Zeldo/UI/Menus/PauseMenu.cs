﻿using Engine;
using Engine.Input.Data;
using Engine.Messaging;
using GlmSharp;

namespace Zeldo.UI.Menus
{
	// TODO: If the player clicks on Unpause (to unpause the game), that shouldn't *also* trigger an attack.
	public class PauseMenu : Menu
	{
		private MainGame game;

		public PauseMenu(MainGame game)
		{
			this.game = game;

			Initialize(new []
			{
				"Unpause",
				"Editor",
				"Settings",
				"Exit"
			});

			// TODO: Probably don't hardcode position.
			SetPosition(new vec2(Resolution.WindowWidth / 2f, 100), false);
			IsVisible = false;
		}

		public override void ProcessInput(FullInputData data, MenuControls controls)
		{
			if (data.Query(controls.Pause, InputStates.PressedThisFrame))
			{
				IsVisible = !IsVisible;
				game.TogglePause();

				// Other input isn't processed on the frame the pause menu becomes visible.
				return;
			}

			if (IsVisible)
			{
				base.ProcessInput(data, controls);
			}
		}

		public override void Submit(int index)
		{
			switch (index)
			{
				// Unpause
				case 0:
					IsVisible = false;
					game.TogglePause();

					break;

				// Editor
				case 1:
					break;

				// Settings
				case 2:
					break;

				// Exit
				case 3:
					MessageSystem.Send(CoreMessageTypes.Exit);

					break;
			}
		}
	}
}
