﻿using Engine;
using Engine.Core._2D;
using Engine.Graphics;
using Engine.Graphics._2D;
using Engine.Interfaces._2D;
using GlmSharp;

namespace Zeldo.UI.Screens
{
	public class ItemIcon : IRenderable2D
	{
		private static Texture spritesheet;

		static ItemIcon()
		{
			spritesheet = ContentCache.GetTexture("Items.png");
		}

		private Sprite sprite;
		private SpriteText label;

		public ItemIcon(string label, Bounds2D sourceRect)
		{
			this.label = new SpriteText("Debug", label, Alignments.Center);

			sprite = new Sprite(spritesheet, sourceRect);
			Bounds = new Bounds2D(64);
		}

		public ivec2 Location
		{
			get => (ivec2)sprite.Position.Value;
			set
			{
				sprite.Position.SetValue(value, false);
				label.Position.SetValue(value + new ivec2(0, 48), false);
			}
		}

		public Bounds2D Bounds { get; }

		public void OnHover(ivec2 mouseLocation)
		{
		}

		public void OnUnhover()
		{
		}

		public void OnClick(ivec2 mouseLocation)
		{
		}

		public bool Contains(ivec2 mouseLocation)
		{
			return Bounds.Contains(mouseLocation);
		}

		public void Draw(SpriteBatch sb, float t)
		{
			sprite.Draw(sb, t);
			label.Draw(sb, t);
		}
	}
}
