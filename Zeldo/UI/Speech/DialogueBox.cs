﻿using System.Collections.Generic;
using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Interfaces._2D;
using Engine.Shaders;
using Engine.Timing;
using Engine.UI;
using Engine.Utility;
using GlmSharp;
using static Engine.GL;

namespace Zeldo.UI.Speech
{
	public class DialogueBox : CanvasElement, IRenderTargetUser2D
	{
		private const int Padding = 10;

		private SpriteFont font;
		private List<SpriteText> lines;
		private RepeatingTimer timer;

		// Speech boxes can optionally be given custom shaders for both individual characters and the full text.
		private RenderTarget renderTarget;
		private Shader fullShader;
		private Shader glyphShader;
		private Sprite fullSprite;

		public DialogueBox()
		{
			font = ContentCache.GetFont("Default");
			lines = new List<SpriteText>();
			Anchor = Alignments.Bottom;
			Offset = new ivec2(0, 100);
			IsCentered = true;
		}

		public override void Dispose()
		{
			renderTarget?.Dispose();
		}

		public void Attach(Shader fullShader, Shader glyphShader)
		{
			this.fullShader = fullShader;
			this.glyphShader = glyphShader;

			renderTarget = new RenderTarget((ivec2)Bounds.Dimensions, RenderTargetFlags.Color);
		}

		public void Refresh(DialogueToken token)
		{
			if (!IsVisible)
			{
				IsVisible = true;
			}

			var wrapped = Utilities.WrapLines(token.Value, font, (int)Bounds.Width - Padding * 2);

			lines.Clear();

			var position = Bounds.Position + new vec2(Padding);

			foreach (var line in wrapped)
			{
				SpriteText spriteText = new SpriteText(font, line);
				spriteText.Position.SetValue(position, false);

				lines.Add(spriteText);
				position.y += 20;
			}
		}

		public override void Update()
		{
			if (!timer.IsPaused)
			{
				timer.Update();
			}
		}

		public void DrawTargets(SpriteBatch sb, float t)
		{
			// If this function is called, it's assumed the render target (and related shaders) were properly set up.
			renderTarget.Apply();
			fullShader.Apply();

			DrawInternal(sb, t);
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			//if (UsesRenderTarget)
			{
				fullSprite.Draw(sb, t);

				return;
			}

			DrawInternal(sb, t);
		}

		private void DrawInternal(SpriteBatch sb, float t)
		{
			if (glyphShader != null)
			{
				sb.Apply(glyphShader, GL_TRIANGLE_STRIP);
			}

			sb.Draw(Bounds, Color.White);
			lines.ForEach(l => l.Draw(sb, t));
		}
	}
}
