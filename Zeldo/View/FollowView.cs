﻿using Engine.Props;
using Engine.View;
using GlmSharp;
using Zeldo.Configuration;
using Zeldo.Entities.Player;

namespace Zeldo.View
{
	public class FollowView : OrbitView
	{
		// TODO: Convert this to a property.
		// Shifting the camera upward by a small amount gives a better view of objects above the player without
		// negatively affecting visibility of the player. Feels a bit better than a pure centered camera.
		private const float Shift = 2;

		private PlayerCharacter player;

		private float distance;
		
		public FollowView(ControlSettings settings, PlayerCharacter player)
		{
			this.player = player;

			sensitivity = settings.MouseSensitivity;

			Properties.Access(this);
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			// TODO: Track properties.
			distance = accessor.GetFloat("follow.view.distance");
			maxPitch = accessor.GetFloat("follow.view.max.pitch");

			message = null;

			return true;
		}

		public override void Update()
		{
			// TODO: Since the camera is shifted slighty upward, experiment with subtly modifying follow distance based on pitch.
			var aim = quat.FromAxisAngle(Pitch, vec3.UnitX) * quat.FromAxisAngle(Yaw, vec3.UnitY);
			var shift = new vec3(0, Shift, 0);
			var eye = player.Position + new vec3(0, 0, -distance) * aim + shift;

			Camera.Position.SetValue(eye, true);
			Camera.Orientation.SetValue(mat4.LookAt(eye, player.Position + shift, vec3.UnitY).ToQuaternion, true);
		}
	}
}
