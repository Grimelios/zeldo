﻿using Engine.Props;
using Engine.View;
using GlmSharp;
using Zeldo.Configuration;
using Zeldo.Entities.Player;

namespace Zeldo.View
{
	// TODO; Consider merging this with the follow view in some way.
	public class ShoulderView : OrbitView
	{
		private PlayerCharacter player;

		private float offsetX;
		private float offsetY;
		private float offsetZ;
		private float distance;

		public ShoulderView(ControlSettings settings, PlayerCharacter player)
		{
			this.player = player;

			sensitivity = settings.MouseSensitivity;
		}
		
		// TODO: Validate properties.
		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			// TODO: Track properties.
			/*
			offsetX = accessor.GetFloat("shoulder.view.offset.x");
			offsetY = accessor.GetFloat("shoulder.view.offset.y");
			offsetZ = accessor.GetFloat("shoulder.view.offset.z");
			distance = accessor.GetFloat("shoulder.view.distance");
			maxPitch = accessor.GetFloat("shoulder.view.max.pitch");
			*/

			message = null;

			return true;
		}

		// The shoulder view is used primarily for aiming (e.g. the bow). While the camera is zooming in, though, the
		// player can still actively alter aim. Using this function, then, allows camera interpolators (during the
		// zoom) to be updated correctly.
		public void Evaluate(out vec3 position, out quat orientation)
		{
			// This is very similar to code from the follow view, but still simpler to just copy (for the time being,
			// anyway).
			var q = quat.FromAxisAngle(-Yaw, vec3.UnitY);
			var target = player.Position + q * new vec3(offsetX, offsetY, offsetZ);
			var aim = quat.FromAxisAngle(Pitch, vec3.UnitX) * quat.FromAxisAngle(Yaw, vec3.UnitY);
			var eye = target + new vec3(0, 0, -distance) * aim;

			position = eye;
			orientation = mat4.LookAt(eye, target, vec3.UnitY).ToQuaternion;
		}

		public override void Update()
		{
			Evaluate(out var p, out var q);

			Camera.Position.SetValue(p, true);
			Camera.Orientation.SetValue(q, true);
		}
	}
}
