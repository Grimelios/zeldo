﻿using System.Collections.Generic;
using System.Linq;
using Engine;
using Engine.Core;
using Engine.Graphics;
using Engine.Shaders;
using Engine.Utility;
using GlmSharp;
using static Engine.GL;

namespace Zeldo.Weather
{
	// TODO: This should probably use the particle system (although maybe not due to optimizations from using the same velocity).
	public class Rain : WeatherFormation
	{
		private Shader shader;
		private GLBuffer buffer;
		private List<vec3> raindrops;
		private vec3 velocity;
		private vec3 step;

		public Rain()
		{
			buffer = new GLBuffer(2000, 200);
			raindrops = new List<vec3>();

			// TODO: Modify velocity based on wind and intensity.
			velocity = new vec3(-6);

			shader = new Shader();
			shader.Attach(ShaderTypes.Vertex, "Rain.vert");
			shader.Attach(ShaderTypes.Geometry, "Rain.geom");
			shader.Attach(ShaderTypes.Fragment, "Rain.frag");
			shader.AddAttribute<float>(3, GL_FLOAT);
			shader.Initialize();
			shader.Use();
			shader.SetUniform("color", new Color(200, 200, 200, 100).ToVec4());
		}

		public override void Dispose()
		{
		}

		public override void Update()
		{
			// Like all rendered objects, raindrops need to be interpolated between update frames. Since all raindrops
			// move with the same velocity, though, old position implied by that velocity.
			step = velocity * Game.DeltaTime;
			
			for (int i = raindrops.Count - 1; i >= 0; i--)
			{
				var drop = raindrops[i];
				drop += velocity * Game.DeltaTime;

				// TODO: Use properties.
				// TODO: This is a naive solution to recycle raindrops. Should be made more robust (especially to changes in camera transform).
				if (drop.y < -5)
				{
					drop.y += 20;
				}

				raindrops[i] = drop;
			}
		}

		public override void Draw(mat4 mvp, float t)
		{
			// TODO: Either use a property or modify raindrop length based on intensity (i.e. fall speed).
			const float Length = 1.5f;

			// TODO: Raindrop velocity likely won't change frequently, so the normalized version could be pre-computed and stored.
			shader.Apply();
			shader.SetUniform("mvp", mvp);
			shader.SetUniform("halfVector", Utilities.Normalize(velocity) * Length / 2);

			var drops = raindrops.Select(d => d - step * t);
		}
	}
}
