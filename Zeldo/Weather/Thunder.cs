﻿using Engine.Core;
using GlmSharp;

namespace Zeldo.Weather
{
	public class Thunder : WeatherFormation
	{
		private bool isLightningVisible;

		public Color FilterLight(Color color)
		{
			return isLightningVisible ? Color.White : color;
		}

		public override void Dispose()
		{
		}

		public override void Update()
		{
		}

		public override void Draw(mat4 mvp, float t)
		{
		}
	}
}
