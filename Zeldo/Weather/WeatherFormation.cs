﻿using System;
using Engine.Interfaces;
using GlmSharp;

namespace Zeldo.Weather
{
	// TODO: Consider re-adding a FilterLight function.
	public abstract class WeatherFormation : IDynamic, IDisposable
	{
		// This represents different parameters for different weather systems (e.g. rainfall density or wind speed).
		public float Intensity { get; set; }

		public virtual void Dispose()
		{
		}

		public abstract void Update();

		public virtual void Draw(mat4 mvp, float t)
		{
		}
	}
}
