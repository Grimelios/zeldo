﻿using System.Collections.Generic;
using Engine.Core;
using Engine.Interfaces;
using Engine.Shaders;
using Engine.View;

namespace Zeldo.Weather
{
	public class WeatherManager : IDynamic
	{
		private Shader shader;
		private TimeOfDay dayNightCycle;
		private List<WeatherFormation> formations;

		public WeatherManager()
		{
			dayNightCycle = new TimeOfDay();
			formations = new List<WeatherFormation>();
		}

		public float TimeOfDay => dayNightCycle.Time;

		public T Spawn<T>() where T : WeatherFormation, new()
		{
			var formation = new T();
			formations.Add(formation);

			return formation;
		}

		public void Update()
		{
			dayNightCycle.Update();
			formations.ForEach(f => f.Update());
		}

		public void Draw(Camera3D camera, float t)
		{
			formations.ForEach(f => f.Draw(camera.ViewProjection, t));
		}
	}
}
